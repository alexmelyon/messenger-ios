//
//  ME_TabbarController.m
//  ME_Messme
//
//  Created by MessMe on 18/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_TabbarController.h"
#import "ME_Network.h"
#import "AppDelegate.h"
#import "ME_StartController.h"

static NSString* UNWIND_TO_START = @"unwindToStart";

@interface ME_TabbarController ()
//
@end

@implementation ME_TabbarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //
    
    NSString *path = [NSString stringWithFormat:@"/Message?id=%@&uuid=%@", [AppDelegate shared].phone, [AppDelegate shared].token];
    //
    [ME_Network connectTo:path andThen:^(SRWebSocket *webSocket) {
        
        [ME_Network setOnFirstMessage:^(NSDictionary *json) {
            if(1000 != [[json objectForKey:@"status"] intValue]) {
                //
                    [AppDelegate shared].isNewDeviceNeedRelogin = YES;
                    // TODO avoid show tabs
                    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    id ctrl = [storyboard instantiateViewControllerWithIdentifier:@"ME_StartController"];
                    [self presentViewController:ctrl animated:NO completion:nil];
                [ME_Network setReconnectRequired:NO];
//                }
            } else {
                [ME_Network setReconnectRequired:YES];
                [AppDelegate shared].isNewDeviceNeedRelogin = NO;
                [AppDelegate shared].isGoingToTabs = YES;
                //
                [[AppDelegate shared] saveUserDefaults];
                //
                BOOL clean = [AppDelegate shared].isNeededToClearFriends;
                //
                [ME_Network send:@{@"action":@"user.clearfriend"} doIt:clean andThen:^(NSDictionary *json) {
                    NSLog(@"FRIENDS CLEARED");
                    [ME_Network send:@{@"action":@"user.cleanallhistory", @"options":@{}} doIt:clean andThen:^(NSDictionary *json) {
                        NSLog(@"HISTORY CLEARED");
                        [AppDelegate shared].isNeededToClearFriends = NO;
                        [[AppDelegate shared] fetchContactsAndThen:^(NSMutableArray *arr) {
                            [ME_Network send:@{@"action":@"user.addfriend", @"options":@{@"userlist":[[AppDelegate shared] getPhoneNumbersArr]}} andThen:^(NSDictionary *json) {
                                [[AppDelegate shared] updateFriends:^(NSMutableArray *friends) {
                                    [[AppDelegate shared] updateSections];
                                }];
                            }];
                        }];
                    }];
                }];
                //
                [ME_Network send:@{@"action":@"user.setstatus", @"options":@{@"status":@(1)}} andThen:^(NSDictionary *json) {
                    if(1000 == [json[@"status"] intValue]) {
                        NSLog(@"I'M ONLINE");
                    }
                }];
            }
        }];
    }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [self performSegueWithIdentifier:SEGUE_TO_START sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
