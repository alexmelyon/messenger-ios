//
//  ME_ImageCache.m
//  ME_Messme
//
//  Created by MessMe on 11/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_ImageCache.h"
#import "ME_Network.h"
#import "UIImage+Utils.h"

@interface ME_ImageCache ()

@end

@implementation ME_ImageCache

+ (void) getAvatarForFriend:(ME_Friend*)friend andThen:(void(^)(UIImage* image))callback
{
    [[self class] getAvatarForFriend:friend size:CGSizeMake(34.0, 34.0) andThen:callback];
}

+ (void) getAvatarForFriend:(ME_Friend*)friend size:(CGSize)size andThen:(void(^)(UIImage* image))callback
{
    [[self class] getAvatar:friend.avatar login:friend.login name:[friend getPresentName] size:size andThen:^(UIImage *image) {
        friend.image = image;
        callback(image);
    }];
}

+ (void) getAvatar:(NSString*)key login:(NSString*)login name:(NSString*)name andThen:(void(^)(UIImage* image))callback
{
    [[self class] getAvatar:key login:login name:name size:CGSizeMake(34.0, 34.0) andThen:callback];
}

+ (void) getAvatar:(NSString*)key login:(NSString*)login name:(NSString*)name size:(CGSize)size andThen:(void(^)(UIImage* image))callback
{
    if(key && key.length) {
        [[self class] getImage:key subPath:@"avatar" andThen:callback];
    } else {
        UIColor* color = [[self class] getColorForLogin:login];
        UIImage* img = [[UIImage blankImageWithSize:size color:color] drawText:[name substringToIndex:1] atPoint:CGPointMake(1, 0)];
        img = [img getRounded];
        callback(img);
    }
}

+ (void) getImage:(NSString*)key subPath:(NSString*)subPath andThen:(void(^)(UIImage*image))callback
{
    NSData* savedData = [[self class] getDataWithKey:key];
//    if([_dict objectForKey:key]) {
//        callback([_dict objectForKey:key]);
    if(savedData) {
        UIImage* img = [savedData toImage];
        callback(img);
    } else {
        NSString* urlPath = [NSString stringWithFormat:@"/%@/%@", subPath, key];
        [ME_Network downloadFileFromPath:urlPath andThen:^(NSData *data) {
            UIImage* img = [data toImage];
            if(data.length) {
//                if(!_dict) {
//                    _dict = [NSMutableDictionary dictionary];
//                }
//                [_dict setObject:img forKey:key];
                [[self class] saveData:data withKey:key];
            }
            callback(img);
        }];
    }
}

+ (NSData*) getDataWithKey:(NSString*)key
{
    NSData* data = [NSData dataWithContentsOfFile:key];
    return data;
}

+ (void) saveData:(NSData*)data withKey:(NSString*)key
{
    // save to the documents folder
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsPath = paths[0];
    NSString* filename = [NSString stringWithFormat:@"%@", key];
    NSString* filePath = [documentsPath stringByAppendingPathComponent:filename];
    [data writeToFile:filePath atomically:YES];
}

+ (UIColor*) getColorForLogin:(NSString*)name
{
    // arr, ppc hindu
    name = [name lowercaseString];
    NSString* last = [name substringFromIndex:name.length - 1];
    if([@"a" isEqualToString:last]) { // 1
        return [UIColor colorWithRed:230/255.0 green:54/255.0 blue:180/255.0 alpha:1.0];
    } else if([@"b" isEqualToString:last]) {
        return [UIColor colorWithRed:250/255.0 green:55/255.0 blue:122/255.0 alpha:1.0];
    } else if([@"c" isEqualToString:last]) {
        return [UIColor colorWithRed:255/255.0 green:112/255.0 blue:102/255.0 alpha:1.0];
    } else if([@"d" isEqualToString:last]) {
        return [UIColor colorWithRed:255/255.0 green:123/255.0 blue:82/255.0 alpha:1.0];
    } else if([@"e" isEqualToString:last]) {
        return [UIColor colorWithRed:254/255.0 green:200/255.0 blue:7/255.0 alpha:1.0];
    } else if([@"f" isEqualToString:last]) {
        return [UIColor colorWithRed:96/255.0 green:219/255.0 blue:100/255.0 alpha:1.0];
    } else if([@"g" isEqualToString:last]) {
        return [UIColor colorWithRed:194/255.0 green:35/255.0 blue:137/255.0 alpha:1.0];
    } else if([@"h" isEqualToString:last]) {
        return [UIColor colorWithRed:232/255.0 green:30/255.0 blue:99/255.0 alpha:1.0];
    } else if([@"i" isEqualToString:last]) {
        return [UIColor colorWithRed:243/255.0 green:67/255.0 blue:54/255.0 alpha:1.0];
    } else if([@"j" isEqualToString:last]) { // 10
        return [UIColor colorWithRed:254/255.0 green:87/255.0 blue:34/255.0 alpha:1.0];
    } else if([@"k" isEqualToString:last]) {
        return [UIColor colorWithRed:254/255.0 green:151/255.0 blue:0/255.0 alpha:1.0];
    } else if([@"l" isEqualToString:last]) {
        return [UIColor colorWithRed:76/255.0 green:174/255.0 blue:80/255.0 alpha:1.0];
    } else if([@"m" isEqualToString:last]) {
        return [UIColor colorWithRed:124/255.0 green:21/255.0 blue:104/255.0 alpha:1.0];
    } else if([@"n" isEqualToString:last]) {
        return [UIColor colorWithRed:135/255.0 green:14/255.0 blue:76/255.0 alpha:1.0];
    } else if([@"o" isEqualToString:last]) {
        return [UIColor colorWithRed:182/255.0 green:28/255.0 blue:28/255.0 alpha:1.0];
    } else if([@"p" isEqualToString:last]) {
        return [UIColor colorWithRed:190/255.0 green:54/255.0 blue:12/255.0 alpha:1.0];
    } else if([@"q" isEqualToString:last]) {
        return [UIColor colorWithRed:229/255.0 green:81/255.0 blue:0/255.0 alpha:1.0];
    } else if([@"r" isEqualToString:last]) {
        return [UIColor colorWithRed:27/255.0 green:94/255.0 blue:32/255.0 alpha:1.0];
    } else if([@"s" isEqualToString:last]) {
        return [UIColor colorWithRed:0/255.0 green:181/255.0 blue:164/255.0 alpha:1.0];
    } else if([@"t" isEqualToString:last]) { // 20
        return [UIColor colorWithRed:0/255.0 green:197/255.0 blue:222/255.0 alpha:1.0];
    } else if([@"u" isEqualToString:last]) {
        return [UIColor colorWithRed:78/255.0 green:99/255.0 blue:219/255.0 alpha:1.0];
    } else if([@"v" isEqualToString:last]) {
        return [UIColor colorWithRed:144/255.0 green:76/255.0 blue:228/255.0 alpha:1.0];
    } else if([@"w" isEqualToString:last]) {
        return [UIColor colorWithRed:210/255.0 green:53/255.0 blue:237/255.0 alpha:1.0];
    } else if([@"x" isEqualToString:last]) {
        return [UIColor colorWithRed:121/255.0 green:157/255.0 blue:173/255.0 alpha:1.0];
    } else if([@"y" isEqualToString:last]) {
        return [UIColor colorWithRed:176/255.0 green:124/255.0 blue:105/255.0 alpha:1.0];
    } else if([@"z" isEqualToString:last]) {
        return [UIColor colorWithRed:0/255.0 green:149/255.0 blue:135/255.0 alpha:1.0];
    } else if([@"0" isEqualToString:last]) {
        return [UIColor colorWithRed:0/255.0 green:172/255.0 blue:194/255.0 alpha:1.0];
    } else if([@"1" isEqualToString:last]) {
        return [UIColor colorWithRed:63/255.0 green:81/255.0 blue:180/255.0 alpha:1.0];
    } else if([@"2" isEqualToString:last]) {
        return [UIColor colorWithRed:109/255.0 green:60/255.0 blue:178/255.0 alpha:1.0];
    } else if([@"3" isEqualToString:last]) { // 30
        return [UIColor colorWithRed:155/255.0 green:39/255.0 blue:175/255.0 alpha:1.0];
    } else if([@"4" isEqualToString:last]) {
        return [UIColor colorWithRed:96/255.0 green:125/255.0 blue:138/255.0 alpha:1.0];
    } else if([@"5" isEqualToString:last]) {
        return [UIColor colorWithRed:121/255.0 green:85/255.0 blue:72/255.0 alpha:1.0];
    } else if([@"6" isEqualToString:last]) {
        return [UIColor colorWithRed:0/255.0 green:77/255.0 blue:64/255.0 alpha:1.0];
    } else if([@"7" isEqualToString:last]) {
        return [UIColor colorWithRed:0/255.0 green:96/255.0 blue:100/255.0 alpha:1.0];
    } else if([@"8" isEqualToString:last]) {
        return [UIColor colorWithRed:21/255.0 green:31/255.0 blue:124/255.0 alpha:1.0];
    } else if([@"9" isEqualToString:last]) {
        return [UIColor colorWithRed:70/255.0 green:32/255.0 blue:127/255.0 alpha:1.0];
    } else {
        return [UIColor colorWithRed:113/255.0 green:28/255.0 blue:128/255.0 alpha:1.0];
    }
    return [UIColor redColor];
}

@end
