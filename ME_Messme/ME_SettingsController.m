//
//  ME_SettingsController.m
//  ME_Messme
//
//  Created by MessMe on 20/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_SettingsController.h"
#import "AppDelegate.h"

@interface ME_SettingsController () <UIAlertViewDelegate>
- (IBAction)exitProfileAction:(id)sender;

@end

@implementation ME_SettingsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)exitProfileAction:(id)sender {
    [[[UIAlertView alloc] initWithTitle:@"Выйти из профиля?" message:@"" delegate:self cancelButtonTitle:@"Нет" otherButtonTitles:@"Да", nil] show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(1 == buttonIndex) {
        [[AppDelegate shared] exitProfile];
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        id ctrl = [storyboard instantiateViewControllerWithIdentifier:@"ME_StartController"];
        [self presentViewController:ctrl animated:NO completion:nil];
//        [self.navigationController setViewControllers:@[] animated:YES];
    }
}

@end
