//
//  ME_CodeController.m
//  ME_Messme
//
//  Created by MessMe on 16/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_CodeController.h"
#import "ME_Network.h"
#import "AppDelegate.h"
#import "ME_ProfileController.h"

static NSString* SEGUE_TABS = @"segueTabs";
static NSString* SEGUE_PROFILE = @"segueProfile";
static NSString* SEGUE_RECOVERY = @"segueRecovery";

@interface ME_CodeController () <UITextFieldDelegate>
- (IBAction)backAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *codeField;
- (IBAction)requestCodeAgainAction:(id)sender;
- (IBAction)nextAction:(id)sender;
@end

@implementation ME_CodeController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow_left_blue"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction:)];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    //
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Отмена"
                                                            style:UIBarButtonItemStylePlain
                                                           target:self
                                                           action:@selector(cancelNumberPad:)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"ОК" style:UIBarButtonItemStyleDone
                                                           target:self
                                                           action:@selector(doneWithNumberPad:)]];
    [numberToolbar sizeToFit];
    self.codeField.inputAccessoryView = numberToolbar;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)cancelNumberPad:(id)sender
{
    [self.codeField resignFirstResponder];
}

-(void)doneWithNumberPad:(id)sender
{
    [self.codeField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([SEGUE_PROFILE isEqualToString:segue.identifier]) {
//        ME_ProfileController *ctrl = segue.destinationViewController;
//        ctrl.isInitial = YES;
    }
}


- (IBAction)requestCodeAgainAction:(id)sender {
    [ME_Network send:@{@"action":@"user.getactivationcode"} andThen:^(NSDictionary *json) {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Код доступа выслан вам в SMS" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }];
}

- (IBAction)nextAction:(id)sender
{
    if(self.codeField.text.length < 4) {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Пожалуйста, введите код доступа" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    
    [ME_Network send:@{@"action":@"user.activate", @"options":@{@"code":@([self.codeField.text intValue])}} andThen:^(NSDictionary *json) {
        if(1003 == [[json objectForKey:@"status"] intValue]) {
            [[[UIAlertView alloc] initWithTitle:@"" message:@"Пожалуйста, проверьте правильность кода" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        } else if(1000 == [[json objectForKey:@"status"] intValue]) {
            [AppDelegate shared].token = [json objectForKey:@"result"];
            NSLog(@"TOKEN = %@", [AppDelegate shared].token);
            //
            if([AppDelegate shared].isUserAlreadyExists) {
                // TODO recovery profile
                [self performSegueWithIdentifier:SEGUE_RECOVERY sender:sender];
            } else {
                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                ME_ProfileController* ctrl = [storyboard instantiateViewControllerWithIdentifier:@"profileController"];
                ctrl.isInitial = YES;
                [self.navigationController pushViewController:ctrl animated:YES];
            }
        } else {
            NSString *message = [NSString stringWithFormat:@"Код ошибки: %d", [[json objectForKey:@"status"] intValue]];
            [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
