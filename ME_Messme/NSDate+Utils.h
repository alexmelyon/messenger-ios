//
//  NSDate+Utils.h
//  ME_Messme
//
//  Created by MessMe on 10/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DateUtils)

- (NSDate*) toDateFromUtc;

@end

@interface NSDate (Util)

- (NSString*) fromNow;
- (NSString*) getHHmm;
- (NSString*) toUtc;

@end