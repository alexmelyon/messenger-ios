//
//  UIView+Utils.h
//  ME_Messme
//
//  Created by MessMe on 26/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIView (Utils)

- (UIView*) getSuperviewClass:(Class*)class;

@end
