//
//  ME_Country.h
//  ME_Messme
//
//  Created by MessMe on 02/03/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ME_Country : NSObject

@property int continentId;
@property (nonatomic, copy) NSString* continentName;
@property int countryId;
@property (nonatomic, copy) NSString* countryName;
@property int iso;
@property int phoneCode;

- (instancetype) initWithDict:(NSDictionary*)dict;

@end
