//
//  ME_ContactCellModel.h
//  ME_Messme
//
//  Created by MessMe on 12/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ME_SearchItem : NSObject

@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* surname;
@property long long ID;
@property (nonatomic, copy) NSString* login;
@property (nonatomic, copy) NSString* avatar;

- (ME_SearchItem*) initWithName:(NSString*)name surname:(NSString*)surname ID:(long long)ID login:(NSString*)login avatar:(NSString*)avatar;

@end
