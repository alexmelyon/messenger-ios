//
//  ME_CountryController.m
//  ME_Messme
//
//  Created by MessMe on 25/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_CountryController.h"
#import "ME_Network.h"

@interface ME_CountryController () <UISearchDisplayDelegate>
@property NSArray* countriesArr;
- (IBAction)backAction:(id)sender;
@property NSArray* filteredArr;
@end

@implementation ME_CountryController

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"geo.country.list" ofType:@"txt"];
    NSError* error;
    NSString* content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    if(error) {
        NSLog(@"ERROR %@", error);
    }
    self.countriesArr = [[content JSONValue] objectForKey:@"result"];
    self.filteredArr = self.countriesArr;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == self.searchDisplayController.searchResultsTableView) {
        return self.filteredArr.count;
    } else {
        return self.countriesArr.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"countryCell" forIndexPath:indexPath];
    //
    NSDictionary* obj = [self.countriesArr objectAtIndex:indexPath.row];
    
    if(tableView == self.searchDisplayController.searchResultsTableView) {
        obj = [self.filteredArr objectAtIndex:indexPath.row];
    }
    NSString* iso = [[obj objectForKey:@"iso"] stringValue];
    cell.imageView.image = [UIImage imageNamed:iso];
    //
    CGSize itemSize = CGSizeMake(34, 34);
    UIGraphicsBeginImageContextWithOptions(itemSize, NO, UIScreen.mainScreen.scale);
    CGRect imageRect = CGRectMake(0, 0, itemSize.width, itemSize.height);
    [cell.imageView.image drawInRect:imageRect];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    cell.imageView.layer.cornerRadius = 17;
    //
    cell.textLabel.text = [obj objectForKey:@"countryname"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"+%@", [obj objectForKey:@"phonecode"]];
    //
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* country = [self.countriesArr objectAtIndex:indexPath.row];
    if(tableView == self.searchDisplayController.searchResultsTableView) {
        country = [self.filteredArr objectAtIndex:indexPath.row];
    }
    [self.delegate selectCountry:country from:self];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    if(searchString == nil || [[searchString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        self.filteredArr = self.countriesArr;
    } else {
        NSPredicate* predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            NSDictionary* obj = evaluatedObject;
            NSString* countryname = [[obj objectForKey:@"countryname"] lowercaseString];
            NSString* query = [searchString lowercaseString];
            BOOL res = [countryname containsString:query];
            return res;
        }];
        self.filteredArr = [self.countriesArr filteredArrayUsingPredicate:predicate];
    }
    return YES;
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
