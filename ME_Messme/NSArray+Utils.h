//
//  NSArray+Utils.h
//  ME_Messme
//
//  Created by MessMe on 08/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Utils)

- (BOOL) containsByPredicate:(NSPredicate*)predicate;
- (NSArray*) mapWithBlock:(id(^)(id obj))block;

@end
