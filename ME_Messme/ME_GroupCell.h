//
//  ME_GroupCell.h
//  ME_Messme
//
//  Created by MessMe on 17/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ME_Dialog.h"

@protocol ME_GroupCellDelegate

- (void) iconClickedFromGroupCell:(UITableViewCell*)cell;

@end

@interface ME_GroupCell : UITableViewCell

@property (nonatomic, weak) id<ME_GroupCellDelegate> delegate;
- (ME_GroupCell*) configureWithDialog:(ME_Dialog*)dialog;

@end
