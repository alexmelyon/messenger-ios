//
//  ME_ContactsForGroupController.h
//  ME_Messme
//
//  Created by MessMe on 18/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ME_GroupModel.h"

@protocol ME_ContactsForGroupDelegate <NSObject>

- (void) groupController:(UITableViewController*)grpCtrl addContactsForGroup:(ME_GroupModel*)grpModel;

@end

@interface ME_ContactsForGroupController : UITableViewController

@property (nonatomic, weak) id<ME_ContactsForGroupDelegate> delegate;
@property (nonatomic, strong) ME_GroupModel* grpModel;

@end
