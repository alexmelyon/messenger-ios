//
//  ME_GroupCell.m
//  ME_Messme
//
//  Created by MessMe on 17/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_GroupCell.h"
#import "ME_ImageCache.h"
#import "NSString+Util.h"
#import "NSDate+Utils.h"
#import "AppDelegate.h"

@interface ME_GroupCell ()
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *middleLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end

@implementation ME_GroupCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (ME_GroupCell*) configureWithDialog:(ME_Dialog*)dialog
{
    NSString* avatar = dialog.fromAvatar;
    if([avatar trim].length) {
        NSString* name = [dialog getSourceName];
        [ME_ImageCache getAvatar:avatar login:name name:name andThen:^(UIImage* image) {
            self.iconImageView.image = image;
            self.iconImageView.layer.cornerRadius = self.iconImageView.frame.size.width / 2;
            self.iconImageView.clipsToBounds = YES;
        }];
    } else {
        self.iconImageView.image = [UIImage imageNamed:@"TODO"];
    }
    self.topLabel.text = dialog.fromName;
    self.middleLabel.text = dialog.fromSurname;
    self.bottomLabel.text = [NSString stringWithFormat:@"%d из %d", dialog.msgUnread, dialog.msgTotal];
    self.timeLabel.text = [[dialog.dateStr toDateFromUtc] fromNow];
    //
    return self;
}

@end
