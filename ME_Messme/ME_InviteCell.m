//
//  InviteCell.m
//  ME_Messme
//
//  Created by MessMe on 08/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_InviteCell.h"

@implementation ME_InviteCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) inviteAction:(UIButton*)sender
{
//    CGPoint buttonOriginInTableView = [sender convertPoint:CGPointZero toView:self.tableView];
//    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonOriginInTableView];
//    NSString* phone = [self.contactsToInvite objectForKey:[NSString stringWithFormat:@"%ld-%ld", indexPath.section, indexPath.row]];
    NSLog(@"inviteAction: %@", self.phone);
    [self.delegate inviteFromCell:self withPhone:self.phone];
    //
}
@end
