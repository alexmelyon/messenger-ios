//
//  CNContact+Utils.h
//  ME_Messme
//
//  Created by MessMe on 14/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Contacts/Contacts.h>

@interface CNContact (Utils)

- (NSString*) getFirstNumber;
- (NSString*) generateVCard;

@end
