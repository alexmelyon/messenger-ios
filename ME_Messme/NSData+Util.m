//
//  NSData+Util.m
//  ME_Messme
//
//  Created by MessMe on 03/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "NSData+Util.h"

@implementation NSData (Util)

- (void) saveTo:(NSString*)filename
{
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString* fullpath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, filename];
    NSString* fullpath = [[self class] docPathWith:filename];
    [self writeToFile:fullpath atomically:YES];
}

+ (NSData*) readFrom:(NSString*)filename
{
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString* fullpath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, filename];
    NSString* fullpath = [[self class] docPathWith:filename];
    return [NSData dataWithContentsOfFile:fullpath];
}

+ (NSString*) docPathWith:(NSString*)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* fullpath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, filename];
    return fullpath;
}

@end
