//
//  EULAController.m
//  ME_Messme
//
//  Created by MessMe on 23/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_EULAController.h"
//#import <QuartzCore/QuartzCore.h>

@interface ME_EULAController ()
@property (weak, nonatomic) IBOutlet UIWebView *webview;
- (IBAction)nextAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end

@implementation ME_EULAController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //
    self.nextButton.layer.borderWidth = 1.0f;
    self.nextButton.layer.borderColor = [UIColor colorWithRed:127/255.0 green:197/255.0 blue:239/255.0 alpha:1.0].CGColor;
    self.nextButton.layer.cornerRadius = 5;
    [self.webview loadHTMLString:@"Пожалуйста, подождите..." baseURL:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString* file = [[NSBundle mainBundle] pathForResource:@"EULA" ofType:@"txt"];
    NSError* err;
    NSString* html = [NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:&err];
    if(err) {
        NSLog(@"%@", err);
    }
    [self.webview loadHTMLString:html baseURL:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)nextAction:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
    [self.delegate callLoginSegueFrom:self];
}
@end
