//
//  ME_FriendAnnotation.m
//  ME_Messme
//
//  Created by MessMe on 01/03/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_FriendAnnotation.h"


@implementation ME_FriendAnnotation
- (NSString *)title {
    return [self getPresentName];
}
- (NSString *)subtitle {
    return self.login;
}
- (CLLocationCoordinate2D)coordinate {
    return CLLocationCoordinate2DMake(self.lat, self.lng);
}
@end
