//
//  ME_PromoController.m
//  ME_Messme
//
//  Created by MessMe on 03/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_PromoController.h"

@interface ME_PromoController ()
@property (weak, nonatomic) IBOutlet UIWebView *webview;
- (IBAction)backAction:(id)sender;

@end

@implementation ME_PromoController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //
    [self.webview loadHTMLString:@"Пожалуйста, подождите..." baseURL:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //
    NSString* file = [[NSBundle mainBundle] pathForResource:@"EULA" ofType:@"txt"];
    NSError* err;
    NSString* html = [NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:&err];
    if(err) {
        NSLog(@"%@", err);
    }
    [self.webview loadHTMLString:html baseURL:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
