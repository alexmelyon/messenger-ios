//
//  ME_GeoViewController.h
//  ME_Messme
//
//  Created by MessMe on 27/01/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ME_GeoViewControllerDelegate

- (void) ctrl:(UIViewController*)ctrl chooseGeoLat:(double)lat lng:(double)lng;

@end

@interface ME_GeoViewController : UIViewController

@property double lat;
@property double lng;
@property (nonatomic, weak) id<ME_GeoViewControllerDelegate> delegate;

@end
