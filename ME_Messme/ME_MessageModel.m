//
//  ME_MessageModel.m
//  ME_Messme
//
//  Created by MessMe on 11/01/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_MessageModel.h"
#import <UIKit/UIKit.h>
#import "ME_Attachment.h"
#import "NSString+Util.h"
#import "AppDelegate.h"

@implementation ME_MessageModel

- (ME_MessageModel*) updateWithDict:(NSDictionary*)dict
{
    self.ID = dict[@"id"];
    self.status = [dict[@"status"] intValue];
    self.date = dict[@"date"];
    return self;
}

- (ME_MessageModel*) initWithDict:(NSDictionary*)dict andAttachArr:(NSArray*)attachArr
{
    ME_MessageModel *msgModel = [self initWithDict:dict];
    msgModel.attachArr = [NSMutableArray arrayWithArray:attachArr];
    return msgModel;
}

- (ME_MessageModel*) initWithDict:(NSDictionary*)dict
{
    self = [super init];
    if(dict[@"options"]) {
        // outgoing
        NSDictionary* opts = dict[@"options"];
        self.owner = 1;
        self.message = opts[@"message"];
        self.userid = [[[AppDelegate shared] phone] longLongValue];
        self.attachArr = [NSMutableArray array];
        self.type = [opts[@"type"] intValue];
        self.lat = [opts[@"lat"] doubleValue];
        self.lng = [opts[@"lng"] doubleValue];
        self.timer = [opts[@"timer"] intValue];
        //
    } else {
        // incoming
        self.owner = [dict[@"owner"] intValue];
        self.message = dict[@"message"];
        self.date = dict[@"date"];
        self.userid = [dict[@"userid"] longLongValue];
        self.attachArr = [NSMutableArray array];
        for(NSDictionary* item in dict[@"attach"]) {
            [self.attachArr addObject:[[ME_Attachment alloc] initWithDict:item]];
        }
        self.ID = dict[@"id"];
        self.type = [dict[@"type"] intValue];
        self.status = [dict[@"status"] intValue];
        self.lat = [dict[@"lat"] doubleValue];
        self.lng = [dict[@"lng"] doubleValue];
        self.timer = [dict[@"timer"] intValue];
    }
    return self;
}

- (double) getHeightForCell
{
    double res = 40; // username
    if(self.message.length) {
        double h = [self.message getHeightForWidth:MESSSAGE_WIDTH andFontSize:17];
        res += h;
    }
    if(self.attachArr.count) {
        for(int i = 0; i < self.attachArr.count; i++) {
            ME_Attachment* att = self.attachArr[i];
            double viewHeight = [att getViewHeight];
            res += viewHeight + 5;
        }
    }
    if(self.message.length) {
        res += 30;
    } else {
        res += 25; // time
    }
    return res;
}
- (BOOL) hasGeo
{
    return self.lat && self.lng;
}
@end
