//
//  ME_ChatCell.m
//  ME_Messme
//
//  Created by MessMe on 23/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_ChatCell.h"
#import "NSDate+Utils.h"
#import "AppDelegate.h"
#import "NSString+Util.h"
#import "ME_ImageCache.h"
#import "UIImage+Utils.h"
#import "UIImageView+Utils.h"
#import "ME_Attachment.h"
#import "NSString+Util.h"
#import "UIImageView+Utils.h"
#import <MapKit/MapKit.h>
#import "JSON.h"
#import "ME_AttachContactView.h"

static double AUDIO_PROGRESS_WIDTH = 170;

@interface ME_ChatCell () <ME_AttachContactViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property BOOL isOwner;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftBubbleConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightAttachmentsConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthBubbleConstraint;
@property (weak, nonatomic) IBOutlet UIView *attachView;
//@property (strong, nonatomic) UILabel *bodyLabel;
@property (weak, nonatomic) IBOutlet UIView *bubbleView;
@property (weak, nonatomic) IBOutlet UIImageView *geoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *tickImageView;
@property (weak, nonatomic) IBOutlet UIImageView *snapImageView;

- (UIView*) getViewForAttach:(ME_Attachment*)attachObj withTag:(int)tag;
- (void) removeAllAttaches;
- (void) addAttaches:(ME_MessageModel*)msgModel;

@end

static UIImage* lefthandImage = nil;
static UIImage* righthandImage = nil;

@implementation ME_ChatCell

+ (void)initialize
{
    if (self == [ME_ChatCell class])
    {
//        lefthandImage = [[UIImage imageNamed:@"BubbleLefthand"] stretchableImageWithLeftCapWidth:20 topCapHeight:19];
//        righthandImage = [[UIImage imageNamed:@"BubbleRighthand"] stretchableImageWithLeftCapWidth:20 topCapHeight:19];
        lefthandImage = [[UIImage imageNamed:@"bubble_your"] stretchableImageWithLeftCapWidth:38 topCapHeight:50];
        righthandImage = [[UIImage imageNamed:@"bubble_my"] stretchableImageWithLeftCapWidth:38 topCapHeight:50];
    }
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubviews {
    [super layoutSubviews];
    //
//    [self allSubviewsForView:self.contentView callback:^(UIView* v) {
//        if(v.hidden) {
//            v.hidden = NO;
//        }
//    }];
}

//- (void) allSubviewsForView:(UIView*)view callback:(void(^)(UIView* v))callback
//{
//    for(UIView* temp in view.subviews) {
//        callback(temp);
//        [self allSubviewsForView:temp callback:callback];
//    }
//}


- (void)drawRect:(CGRect)rect
{
    CGRect b = CGRectInset(self.bounds, 0, 0);
    if(self.isOwner) {
        b = CGRectMake(b.size.width - BUBBLE_WIDTH, b.origin.y, BUBBLE_WIDTH, b.size.height);
        CGFloat w = righthandImage.size.width;
        CGFloat h = righthandImage.size.height;
        [[righthandImage stretchableImageWithLeftCapWidth:w/2 topCapHeight:h/2] drawInRect:b];
    } else {
        b = CGRectMake(b.origin.x, b.origin.y, BUBBLE_WIDTH, b.size.height);
        CGFloat w = lefthandImage.size.width;
        CGFloat h = lefthandImage.size.height;
        [[lefthandImage stretchableImageWithLeftCapWidth:w/2 topCapHeight:h/2] drawInRect:b];
    }
}

- (void) setModel:(ME_MessageModel*)msgModel
{
    self.msgModel = msgModel;
    //
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.attachView.backgroundColor = [UIColor clearColor];
        self.bubbleView.backgroundColor = [UIColor clearColor];
    //
    self.widthBubbleConstraint.constant = BUBBLE_WIDTH;
    //
    self.isOwner = 1 == msgModel.owner;
    if(self.isOwner) {
        self.leftBubbleConstraint.constant = 70;
    } else {
        self.leftBubbleConstraint.constant = 0;
    }
//    self.bodyLabel.text = msgModel.message;
    self.timeLabel.text = [[msgModel.date toDateFromUtc] getHHmm];
    //
//    [[AppDelegate shared] getUserById:msgModel.userid andThen:^(ME_Friend *friend) {
//        NSString* presentName = [friend getPresentName];
//        self.userLabel.text = presentName;
//        [ME_ImageCache getAvatar:friend.avatar forUsername:presentName andThen:^(UIImage *image) {
//            [self.avatarImageView setRoundedImage:image];
//        }];
//    }];
    //
    [self removeAllAttaches];
    [self addAttaches:msgModel];
    //
    self.geoImageView.hidden = ![self hasGeoAttachment];
    self.snapImageView.hidden = !self.msgModel.type == MSG_TYPE_1_SNAP;
    //
    if(self.msgModel.status == MSG_STATUS_UNREAD) {
        self.tickImageView.image = nil;
    } else if(self.msgModel.status == MSG_STATUS_0_SENT) {
        self.tickImageView.image = [UIImage imageNamed:@"tick_gray"];
    } else if(self.msgModel.status == MSG_STATUS_1_DELIVERED) {
        self.tickImageView.image = [UIImage imageNamed:@"tick_gray_multiple"];
    } else if(self.msgModel.status == MSG_STATUS_2_READED) {
        self.tickImageView.image = [UIImage imageNamed:@"tick_lightble_multiple"];
    }
    //
    [self setNeedsDisplay]; // redraw bubble
}

- (BOOL) hasGeoAttachment
{
    return [self.msgModel hasGeo];
}

- (void) addAttaches:(ME_MessageModel*)msgModel
{
    double yPos = 0;
    //
    if([msgModel.message length]) {
        double height = [msgModel.message getHeightForWidth:MESSSAGE_WIDTH andFontSize:17] + 5;
        UILabel* bodyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, yPos, MESSSAGE_WIDTH, height)];
        bodyLabel.numberOfLines = 0;
        bodyLabel.text = msgModel.message;
        [self.attachView addSubview:bodyLabel];
        yPos += bodyLabel.frame.size.height + 5;
    }
    //
    NSArray* attachArr = msgModel.attachArr;
    for(int i = 0; i < attachArr.count && i < 10; i++) {
        //
        UIView* v = [self getViewForAttach:attachArr[i] withTag:i];
        v.frame = CGRectMake(0, yPos, v.frame.size.width, v.frame.size.height);
        [self.attachView addSubview:v];
        [self.attachView bringSubviewToFront:v];
        yPos += v.frame.size.height + 5;
    }
    self.heightAttachmentsConstraint.constant = yPos - 5;
}

- (void) removeAllAttaches
{
    for(int i = 0; i < self.attachView.subviews.count; i++) {
        [self.attachView.subviews[i] removeFromSuperview];
    }
}

- (UIView*) getViewForAttach:(ME_Attachment*)attachObj withTag:(int)tag
{
    if(ME_ATTACHMENT_1_IMAGE == attachObj.type) {
        UIImageView* imgView = [[UIImageView alloc] init];
        // incoming data
        [ME_ImageCache getImage:[attachObj.ID plus:@"_small"] subPath:@"message" andThen:^(UIImage *image) {
            UIImage* img = [image cropCenterToSize:CGSizeMake(200, 200)];
            img = [UIImage imageWithCGImage:[img CGImage] scale:1.0 orientation:UIImageOrientationUp];
            imgView.image = img;
        }];
        double height = [attachObj getViewHeight];
        imgView.frame = CGRectMake(0, 0, 200, height);
        imgView.tag = tag;
        // outgoing data
        NSData* data = [attachObj getNSData];
        if(data) {
            UIImage* localImg = [data toImage];
            localImg = [localImg cropCenterToSize:CGSizeMake(200, 200)];
            localImg = [UIImage imageWithCGImage:[localImg CGImage] scale:1.0 orientation:UIImageOrientationUp];
            [imgView performSelector:@selector(setImage:) withObject:localImg afterDelay:0.1]; // dirty hack
//            imgView.image = localImg;
        }
        //
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(attachClicked:)];
        [imgView addGestureRecognizer:tap];
        [imgView setUserInteractionEnabled:YES];
        return imgView;
    } else if(ME_ATTACHMENT_2_VIDEO == attachObj.type) {
        UIImageView* imgView = [[UIImageView alloc] init];
        [ME_ImageCache getImage:[attachObj.ID plus:@"_small"] subPath:@"message" andThen:^(UIImage *image) {
            UIImage* img = [image cropCenterToSize:CGSizeMake(200, 200)];
            imgView.image = img;
        }];
        double height = [attachObj getViewHeight];
        imgView.frame = CGRectMake(0, 0, 200, height);
        imgView.tag = tag;
        // outgoing data
        NSData* data = [attachObj getNSData];
        if(data) {
            // jcd
        }
        //
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(attachClicked:)];
        [imgView addGestureRecognizer:tap];
        [imgView setUserInteractionEnabled:YES];
        return imgView;
        //
    } else if(ME_ATTACHMENT_3_AUDIO == attachObj.type || ME_ATTACHMENT_4_VOICE == attachObj.type) {
        UIView* audioView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MESSSAGE_WIDTH, 40)];
        UIButton* playButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        playButton.tag = tag;
//        playButton.imageView.image = [UIImage imageNamed:@"audio_play"];
        [playButton setImage:[UIImage imageNamed:@"audio_play"] forState:UIControlStateNormal];
        [playButton addTarget:self action:@selector(audioClicked:) forControlEvents:UIControlEventTouchUpInside];
        [audioView addSubview:playButton];
        //
        UIImageView* progress = [[UIImageView alloc] initWithFrame:CGRectMake(45, 0, AUDIO_PROGRESS_WIDTH, 40)];
        progress.image = [UIImage imageNamed:@"audio_progress"];
        progress.contentMode = UIViewContentModeLeft;
        [audioView addSubview:progress];
        //
        UIImageView* activeProgress = [[UIImageView alloc] initWithFrame:CGRectMake(45, 0, 0, 40)];
        activeProgress.contentMode = UIViewContentModeLeft;
        activeProgress.image = [UIImage imageNamed:@"audio_progress_active"];
        [audioView addSubview:activeProgress];
        //
        return audioView;
    } else if(ME_ATTACHMENT_10_CONTACT == attachObj.type) {
        //
        ME_AttachContactView* view = [[ME_AttachContactView alloc] initWithFrame:CGRectMake(0, 0, MESSSAGE_WIDTH, 40)];
        view.delegate = self;
        view.attach = attachObj;
        return view;
        //
    } else if(ME_ATTACHMENT_11_GEO == attachObj.type) {
        //
        UIImageView* imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
        imgView.tag = tag;
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(attachClicked:)];
        [imgView addGestureRecognizer:tap];
        //
        NSData* fileData = [NSData dataWithContentsOfFile:attachObj.ID];
//        fileData = nil; // TODO
        if(fileData) {
            imgView.image = [fileData toImage];
        } else {
            static MKMapView* mapView = nil;
            static MKPointAnnotation* point = nil;
//            if(!mapView) {
                //            _mapView = [[MKMapView alloc] init];
                mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
//            }
            //
            [attachObj updateNSDataAndThen:^(ME_Attachment* attach) {
                double lat = [attach getGeoLat];
                double lng = [attach getGeoLng];
                CLLocationCoordinate2D myLoc = CLLocationCoordinate2DMake(lat, lng);
                MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(myLoc, 1000, 1000);
                MKCoordinateRegion adjustedRegion = [mapView regionThatFits:viewRegion];
                [mapView setRegion:adjustedRegion animated:NO];
                // Doesn't works without delegate :(
//                point = [[MKPointAnnotation alloc] init];
//                [point setCoordinate:myLoc];
//                [mapView removeAnnotations:mapView.annotations];
//                [mapView addAnnotation:point];
                
                //
                MKMapSnapshotOptions *options = [[MKMapSnapshotOptions alloc] init];
                options.region = mapView.region;
                options.scale = [UIScreen mainScreen].scale;
                options.size = mapView.frame.size;
                MKMapSnapshotter *snapshotter = [[MKMapSnapshotter alloc] initWithOptions:options];
                [snapshotter startWithCompletionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
                    UIImage *image = snapshot.image;
                    //
                    NSData *data = UIImagePNGRepresentation(image);
                    [data writeToFile:attach.ID atomically:YES];
                    imgView.image = image;
                }];
            }];
        }
        return imgView;
    }
    NSLog(@"WRONG TYPE");
    return nil;
}

- (void) attach:(ME_AttachContactView *)attach addClickedContact:(CNContact *)contact
{
    [self.delegate chatCell:self addContactClicked:contact];
}

- (void) attachClicked:(UIGestureRecognizer*)recognizer
{
    NSLog(@"ATTACH CLICKED");
    UIView* view = recognizer.view;
    ME_Attachment* attach = self.msgModel.attachArr[view.tag];
    if(ME_ATTACHMENT_11_GEO == attach.type) {
        [self.delegate chatCell:self geoClicked:attach];
    } else if(ME_ATTACHMENT_1_IMAGE == attach.type) {
        [self.delegate chatCell:self imageClicked:attach];
    } else if(ME_ATTACHMENT_2_VIDEO == attach.type) {
        [self.delegate chatCell:self videoClicked:attach];
    } else {
        NSLog(@"WRONG ATTACH");
    }
}

- (void) audioClicked:(id)sender
{
    NSLog(@"AUDIO CLICKED");
    UIView *view = sender;
    ME_Attachment* attach = self.msgModel.attachArr[view.tag];
    //
    if(ME_ATTACHMENT_3_AUDIO == attach.type || ME_ATTACHMENT_4_VOICE == attach.type) {
//        self.isAudioPlaying = !self.isAudioPlaying;
        [self.delegate chatCell:self audioClicked:attach];
        //
    } else {
        NSLog(@"SOMETHING WRONG");
    }
}

- (void) setPlaying:(BOOL)isPlaying attach:(ME_Attachment*)attach progress:(double)duration
{
    int attachIndex = 0;
    for(int i = 0; i < self.msgModel.attachArr.count; i++) {
        ME_Attachment* att;
        attachIndex = i;
        att = self.msgModel.attachArr[i];
        if([attach.ID isEqualToString:att.ID]) {
            break;
        }
    }
    UIView* audioView = self.attachView.subviews[attachIndex];
    UIButton* playButton = audioView.subviews[0];
    //    UIImageView* progress = audioView.subviews[1];
    UIImageView* activeProgress = audioView.subviews[2];
    CGRect f = activeProgress.frame;
    if(!isPlaying) {
        [playButton setImage:[UIImage imageNamed:@"audio_play"] forState:UIControlStateNormal];
        activeProgress.frame = CGRectMake(f.origin.x, f.origin.y, 0, f.size.height);
    } else {
        [playButton setImage:[UIImage imageNamed:@"audio_pause"] forState:UIControlStateNormal];
//        player currentTime
//        AVPlayerItem* item = [player currentItem];
//        double progress = 0;
//        if(item.status == AVPlayerItemStatusReadyToPlay) {
//            CMTime total = item.duration;
//            CMTime current = item.currentTime;
//            progress = current.value/total.value;
//        }
        activeProgress.frame = CGRectMake(f.origin.x, f.origin.y, AUDIO_PROGRESS_WIDTH * duration, 40);
    }
}

@end
