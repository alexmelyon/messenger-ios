//
//  ME_ContactsForGroupController.m
//  ME_Messme
//
//  Created by MessMe on 18/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_ContactsForGroupController.h"
#import "AppDelegate.h"
#import "ME_ForGroupCell.h"
#import "ME_Network.h"
#import "NSString+Util.h"

@interface ME_ContactsForGroupController ()

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (nonatomic, strong) NSMutableArray* checkedArr;

- (IBAction)backAction:(id)sender;
- (IBAction)doneAction:(id)sender;

@end

@implementation ME_ContactsForGroupController

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    self.checkedArr = [NSMutableArray array];
    // :/
    for(int i = 0; i < [AppDelegate shared].meContactsArr.count; i++) {
        [self.checkedArr addObject:@(NO)];
    }
    // minimum 2
    self.doneButton.enabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger res = [AppDelegate shared].meContactsArr.count;
    return res;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ME_ForGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    // Configure the cell...
    ME_Friend* friend = [AppDelegate shared].meContactsArr[indexPath.row];
//    cell.textLabel.text = [friend getPresentName];
    [cell configureWithFriend:friend];
    BOOL checked = [self.checkedArr[indexPath.row] boolValue];
    [cell setChecked:checked];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL val =     [self.checkedArr[indexPath.row] boolValue];
    [self.checkedArr replaceObjectAtIndex:indexPath.row withObject:@(!val)];
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:NO];
    int k = 0;
    for(int i = 0; i < self.checkedArr.count; i++) {
        k+= [self.checkedArr[i] boolValue];
    }
    self.doneButton.enabled = k >= 2;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doneAction:(id)sender {
    NSLog(@"doneAction");
    NSMutableArray* userArr = [NSMutableArray array];
    for(int i = 0; i < self.checkedArr.count; i++) {
        if([self.checkedArr[i] boolValue]) {
            ME_Friend* friend = [AppDelegate shared].meContactsArr[i];
            [userArr addObject:@(friend.ID)];
        }
    }
    NSString* avatar = self.grpModel.avatar;
    if(![avatar trim].length) {
        avatar = @"";
    }
    id opts = @{@"id":self.grpModel.ID, @"name":self.grpModel.name, @"description":self.grpModel.desc, @"avatar":avatar, @"userlist":userArr};
    id msg = @{@"action":@"groupchat.update",@"options":opts};
    [ME_Network send:msg andThen:^(NSDictionary *json) {
        if(1000 == [json[@"status"] intValue]) {
            NSDictionary* result = json[@"result"];
//            ME_GroupModel* grpModel = [[ME_GroupModel alloc] initWithDict:result];
            [self.grpModel updateWithDict:result];
//            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//            [self.navigationController popViewControllerAnimated:NO];
            [self.delegate groupController:self addContactsForGroup:self.grpModel];
        } else {
            NSLog(@"ERROR WITH GROUPCHAT CREATE");
        }
    }];
}
@end
