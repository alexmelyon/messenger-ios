//
//  CNContact+Utils.m
//  ME_Messme
//
//  Created by MessMe on 14/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "CNContact+Utils.h"
//@import Contacts;
//@import ContactsUI;

@implementation CNContact (Utils)

- (NSString*) getFirstNumber
{
    CNContact* contact = self;
    NSArray <CNLabeledValue<CNPhoneNumber*>*> *phones = contact.phoneNumbers;
    if(phones.count) {
        CNLabeledValue<CNPhoneNumber*> *first = [phones firstObject];
        CNPhoneNumber* number = first.value;
        NSString* digits = number.stringValue;
        return digits;
    }
    return nil;
}

- (NSData*) generateVCard
{
    NSError* err = nil;
    NSData* data = [CNContactVCardSerialization dataWithContacts:@[self] error:&err];
    if(err) {
        NSLog(@"FAILED TO SERIALIZE CONTACT");
    }
    return data;
}

@end
