//
//  ME_UserCell.h
//  ME_Messme
//
//  Created by MessMe on 17/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ME_Dialog.h"

@protocol ME_UserCellDelegate

- (void) userCell:(UITableViewCell*)cell clickedAvatarWithDialog:(ME_Dialog*)dialog;

@end

@interface ME_UserCell : UITableViewCell

@property (nonatomic, weak) id<ME_UserCellDelegate> delegate;
- (ME_UserCell*) configureWithDialog:(ME_Dialog*)dialog;

@end
