//
//  ME_GroupModel.h
//  ME_Messme
//
//  Created by MessMe on 19/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    GRP_ISSECRET_NO = 0,
    GRP_ISSECRET_YES = 1
} GRP_ISSECRET;

@interface ME_GroupModel : NSObject

@property long long adminid;
@property (nonatomic, copy) NSString* avatar;
@property (nonatomic, copy) NSString* dateStr;
@property (nonatomic, copy) NSString* desc;
@property (nonatomic, copy) NSString* ID;
@property GRP_ISSECRET isSecret;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* password;
@property long long userid;
//@property (nonatomic, copy) NSString* userlogin;
//@property (nonatomic, copy) NSString* adminlogin;
@property (nonatomic, strong) NSMutableArray* userlist;

- (ME_GroupModel*) initWithDict:(NSDictionary*)dict;
- (void) updateWithDict:(NSDictionary*)dict;

@end
