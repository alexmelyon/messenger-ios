//
//  NSStrimg+Trim.m
//  ME_Messme
//
//  Created by MessMe on 04/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "NSString+Util.h"
#import <UIKit/UIKit.h>

@implementation NSString (Util)

+ (NSString*) russianDeclensionForNum:(NSInteger)num one:(NSString*)one two:(NSString*)two five:(NSString*)five {
    return [NSString russianDeclensionForStr:[NSString stringWithFormat:@"%ld", num] one:one two:two five:five];
}

+ (NSString*) russianDeclensionForStr:(NSString*)str one:(NSString*)one two:(NSString*)two five:(NSString*)five {
    NSString *res = nil;
    if ([str hasSuffix:@"1"] && ![str hasSuffix:@"11"]) {
        res = [NSString stringWithFormat:@"%@ %@", str, one];
    } else if([str hasSuffix:@"11"]
              || [str hasSuffix:@"12"]
              || [str hasSuffix:@"13"]
              || [str hasSuffix:@"14"]) {
        res = [NSString stringWithFormat:@"%@ %@", str, five];
    } else if ([str hasSuffix:@"2"] || [str hasSuffix:@"3"] || [str hasSuffix:@"4"]) {
        res = [NSString stringWithFormat:@"%@ %@", str, two];
    } else if ([str hasSuffix:@"0"]
               || [str hasSuffix:@"5"]
               || [str hasSuffix:@"6"]
               || [str hasSuffix:@"7"]
               || [str hasSuffix:@"8"]
               || [str hasSuffix:@"9"]) {
        res = [NSString stringWithFormat:@"%@ %@", str, five];
    } else {
        res = str;
    }
    return res;
}

- (double) getHeightForWidth:(double)width andFontSize:(double)fontSize
{
    CGRect r = [self boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                  options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                               attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}
                                  context:nil];
    return r.size.height;
}

- (double) getHeightForWidth:(double)width
{
    return [self getHeightForWidth:width andFontSize:14];
}

- (NSString*) trim
{
    NSString* trimmed = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    return trimmed;
}

- (NSString*) plus:(NSString*)second
{
    return [NSString stringWithFormat:@"%@%@", self, second];
}

- (NSString*) plus:(NSString*)second :(NSString*)third
{
    if(!second) {
        second = @"";
    }
    if(!third) {
        third = @"";
    }
    return  [NSString stringWithFormat:@"%@%@%@", self, second, third];
}

- (NSString*) plusLongLong:(long long)second
{
    return [NSString stringWithFormat:@"%@%lld", self, second];
}

- (NSString*) plusParams:(id)second, ...
{
    return @"";
}

@end
