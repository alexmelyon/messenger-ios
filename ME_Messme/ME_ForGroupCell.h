//
//  ME_ForGroupCell.h
//  ME_Messme
//
//  Created by MessMe on 19/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ME_Friend.h"

@interface ME_ForGroupCell : UITableViewCell

- (void) configureWithFriend:(ME_Friend*)friend;
- (void) setChecked:(BOOL)checked;

@end
