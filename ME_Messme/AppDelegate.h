//
//  AppDelegate.h
//  ME_Messme
//
//  Created by MessMe on 16/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ME_UserProfile.h"
#import "ME_Friend.h"
#import "ME_PhoneContact.h"

@protocol AppDelegateContactsListener <NSObject>

- (void) onContactsUpdated:(NSArray*)contacts;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (AppDelegate*) shared;
@property (nonatomic, copy) NSString* phone;
@property (nonatomic, copy) NSString* token;
@property (nonatomic, copy) NSString* phonecode;
@property BOOL isGoingToTabs;
@property BOOL isUserAlreadyExists;
@property BOOL isNewDeviceNeedRelogin;
//  @{
//@"continentid":@(4),
//@"continentname":@"Европа",
//@"countryid":@(2017370),
//@"countryname":@"Россия",
//@"iso":@(643),
//@"phonecode":@(7)
//};
@property NSDictionary* country;
@property NSDictionary* city;
//@property (nonatomic, strong) NSMutableArray* meEnemiesArr; // users not containing in friends
@property (nonatomic, strong) NSMutableDictionary* meEnemiesDict;
@property (nonatomic, strong) NSMutableArray<ME_Friend*>* meContactsArr; // only server contacts
@property (nonatomic, strong) NSArray* meContactsSections;
@property NSMutableArray<ME_PhoneContact*>* phoneContactsArr; // only phone contacts
//@property (nonatomic, strong) NSMutableArray<ME_Friend*>* friends;
@property (nonatomic, strong) NSMutableArray* friends;
- (void) updateSections;
- (void) updateFriends:(void(^)(NSMutableArray* friends))onUpdate;
- (void) fetchContactsAndThen:(void(^)(NSMutableArray* arr))onFetched;
- (NSInteger) getNumberOfSectionsForContacts;
- (NSInteger) getNumberOfRowsForContactsSection:(NSInteger)section;
- (NSString*) getTitleForHeaderInContactsSection:(NSInteger)section;
- (void) addContactsListener:(id)sender;
- (void) removeContactsListener:(id)sender;
- (NSArray*) getPhoneNumbersArr;
@property (nonatomic, strong) ME_UserProfile* userProfile;
- (void) exitProfile;
@property BOOL isNeededToClearFriends;
- (ME_Friend*) getFriendByUserid:(long long)userid;
- (NSString*) getPresentnameForUserid:(long long)userid;
- (void) saveUserDefaults;
- (void) getUserById:(long long)userid andThen:(void(^)(ME_Friend* meFriend))callback;
@property (nonatomic, strong) NSMutableDictionary* dialogs;
- (void) showProfile:(long long)userid;

@end

