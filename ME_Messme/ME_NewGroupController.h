//
//  ME_NewGroupController.h
//  ME_Messme
//
//  Created by MessMe on 24/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ME_GroupModel.h"

@protocol ME_NewGroupControllerDelegate <NSObject>

- (void) newGroupCtrl:(UIViewController*)ctrl groupUpdated:(ME_GroupModel*)grpModel;

@end

@interface ME_NewGroupController : UIViewController

@property (nonatomic, weak) id<ME_NewGroupControllerDelegate> delegate;

@end
