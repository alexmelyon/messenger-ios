//
//  ME_AudioPlayer.h
//  ME_Messme
//
//  Created by MessMe on 22/01/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ME_ChatCell.h"

@interface ME_AudioPlayer : NSObject

@property (nonatomic, strong) ME_ChatCell* audioCell;
@property (nonatomic, copy) NSString* audioCellId;
@property (nonatomic, copy) NSString* audioAttachId;
@property (nonatomic, copy) NSString* audioRecName;

- (void) initAudio;
- (void) playFromCell:(ME_ChatCell*)cell attach:(ME_Attachment*)attach;
- (void) pausePlaying;
- (void) stopPlaying;
- (void) startRecording;
- (void) stopRecording;
- (NSURL*) getRecUrl;
- (void) updateCell;
- (void) playVideoFromUrl:(NSURL*)url;

@end
