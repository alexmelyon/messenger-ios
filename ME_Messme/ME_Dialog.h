//
//  ME_DIalog.h
//  ME_Messme
//
//  Created by MessMe on 18/01/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ME_MessageModel.h"

typedef enum {
    ME_DIALOG_TYPE_0_USER = 0,
    ME_DIALOG_TYPE_1_GROUP = 1,
    ME_DIALOG_TYPE_2_MAILING = 2
} ME_DIALOG_TYPE;

@interface ME_Dialog : NSObject

@property int cntOnline;
@property int cntTotal;
@property (nonatomic, copy) NSString* dateStr;
@property (nonatomic, copy) NSString* entryId;
@property (nonatomic, copy) NSString* fromAvatar;
@property (nonatomic, copy) NSString* fromLogin;
@property (nonatomic, copy) NSString* fromName;
@property int fromStatus;
@property (nonatomic, copy) NSString* fromSurname;
@property long long fromUserid;
@property (nonatomic, copy) NSString* message;
@property int msgTotal;
@property int msgType;
@property int msgUnread;
@property int owner;
@property int status;
@property (nonatomic, copy) NSString* toAvatar;
@property (nonatomic, copy) NSString* toLogin;
@property (nonatomic, copy) NSString* toName;
@property int toStatus;
@property (nonatomic, copy) NSString* toSurname;
@property long long toUserid;
@property int type;

- (ME_Dialog*) initWithDict:(NSDictionary*)dict;
- (ME_Dialog*) initWithMsg:(ME_MessageModel*)msgModel;
- (long long) getSourceUserid;
- (NSString*) getSourceName;
- (NSString*) getSourceSurname;
- (NSString*) getSourceLogin;
- (NSString*) getSourceAvatar;
- (int) getSourceStatus;
- (void) updateWithMsg:(ME_MessageModel*)msgModel;

@end
