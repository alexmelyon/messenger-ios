
//
//  ME_CityController.m
//  ME_Messme
//
//  Created by MessMe on 02/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_CityController.h"
#import "ME_Network.h"

@interface ME_CityController () <UISearchDisplayDelegate>

@property NSArray* citiesArr;
//@property NSArray* filteredArr;
@property NSTimer* searchTimer;
- (IBAction)backAction:(id)sender;
@property NSString* query;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
- (IBAction)doneAction:(id)sender;

@end

@implementation ME_CityController

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    self.doneButton.hidden = !self.isShowDoneButton;
    [self.doneButton setTitle:self.doneButtonText forState:UIControlStateNormal];
    //
    [self doSearch:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if(tableView == self.searchDisplayController.searchResultsTableView) {
//        return self.filteredArr.count;
//    } else {
//        return self.citiesArr.count;
//    }
    return self.citiesArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cityCell" forIndexPath:indexPath];
    //
    NSDictionary* obj = [self.citiesArr objectAtIndex:indexPath.row];
    
//    if(tableView == self.searchDisplayController.searchResultsTableView) {
//        obj = [self.filteredArr objectAtIndex:indexPath.row];
//    }
    cell.textLabel.text = [obj objectForKey:@"cityname"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [obj objectForKey:@"regionname"]];
    //
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* city = [self.citiesArr objectAtIndex:indexPath.row];
//    if(tableView == self.searchDisplayController.searchResultsTableView) {
//        city = [self.filteredArr objectAtIndex:indexPath.row];
//    }
    [self.delegate selectCity:city from:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    NSString* trimmed = [searchString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if(searchString == nil || [trimmed isEqualToString:@""]) {
//        self.filteredArr = self.citiesArr;
    } else {
        self.query = [trimmed lowercaseString];
        [self.searchTimer invalidate];
        self.searchTimer = [NSTimer scheduledTimerWithTimeInterval:0.0 target:self selector:@selector(doSearch:) userInfo:nil repeats:NO];
    }
    return YES;
}

- (void) doSearch:(id)sender
{
    NSLog(@"doSearch");
    //
    NSString* mask = self.query;
    if(!mask) {
        mask = @"";
    }
    [ME_Network send:@{@"action":@"geo.city.list", @"options":@{@"continent":@[], @"country":@[@(self.countryid)], @"region":@[], @"locale":@"ru", @"mask":mask}} andThen:^(NSDictionary *json) {
        if(1000 == [json[@"status"] intValue]) {
            self.citiesArr = [json objectForKey:@"result"];
            [self.tableView reloadData];
            [self.searchDisplayController.searchResultsTableView reloadData];
        }
    }];
//    NSPredicate* predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
//        NSDictionary* obj = evaluatedObject;
//        NSString* cityname = [[obj objectForKey:@"cityname"] lowercaseString];
//        BOOL res = [cityname containsString:self.query];
//        return res;
//    }];
//    self.filteredArr = [self.citiesArr filteredArrayUsingPredicate:predicate];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)doneAction:(id)sender {
    [self.delegate selectCity:nil from:self];
}
@end
