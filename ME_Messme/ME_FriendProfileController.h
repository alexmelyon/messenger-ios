//
//  ME_FriendProfileController.h
//  ME_Messme
//
//  Created by MessMe on 29/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface ME_FriendProfileController : UIViewController

@property (nonatomic, strong) ME_Friend* friend;

+ (void) showProfileUserid:(long long)userid fromCtrl:(UIViewController*)ctrl;

@end
