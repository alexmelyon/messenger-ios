//
//  ME_Network.h
//  ME_Messme
//
//  Created by MessMe on 16/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SRWebSocket.h"
#import "JSON.h"

typedef void (^OnOpen)(SRWebSocket* webSocket);
typedef void (^OnMessage)(NSDictionary* json);
typedef void (^OnDownload)(NSData* data);
typedef void (^OnUpload)();

@interface ME_Network : NSObject
+ (void)connectTo:(NSString*)path doIt:(BOOL)doIt andThen:(OnOpen)callback;
+ (void)connectTo:(NSString*)path andThen:(OnOpen)callback;
+ (void)send:(NSDictionary*)obj doIt:(BOOL)doIt andThen:(OnMessage)onMessage;
+ (void)send:(NSDictionary*)obj andThen:(OnMessage)onMessage;
+ (void)setOnFirstMessage:(OnMessage)onMessage;
+ (BOOL)isAvailable;
+ (void)downloadFileFromPath:(NSString*)path andThen:(OnDownload)onDataMessage;
+ (void)downloadFileFrom:(NSString*)urlString andThen:(OnDownload)onDataMessage;
+ (BOOL)isConnected;
+ (void)setReconnectRequired:(BOOL)required;
+ (void)onAction:(NSString*)action setCallback:(OnMessage)onMessage;
+ (void) onActionOutgoing:(NSString*)action setCallback:(OnMessage)onMessage;
+ (void)uploadFile:(NSData*)data filename:(NSString*)filename userdata:(NSArray*)userdataArr contentType:(NSString*)contentType andThen:(OnUpload)callback;
+ (void)uploadDoIt:(BOOL)doIt data:(NSData*)data filename:(NSString*)filename userdata:(NSArray*)userdataArr contentType:(NSString*)contentType andThen:(OnUpload)callback;
//+ (void) onActionOutgoingResult:(NSString*)action setCallback:(OnMessage)onMessage;
@end
