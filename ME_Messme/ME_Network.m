//
//  ME_Network.m
//  ME_Messme
//
//  Created by MessMe on 16/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_Network.h"
#import "Reachability.h"

@interface ME_Network () <SRWebSocketDelegate, NSURLSessionDataDelegate>

@property (nonatomic, copy) OnOpen onOpenCallback;
@property (nonatomic, copy) OnMessage onFirstMessage;
@property (nonatomic, copy) NSURL* lastUrl;
//@property BOOL socketWasClosedUnexpectedly;
//@property (nonatomic, copy) NSDictionary* msgBeforeClosed;
@property BOOL isReconnectCalled;
//@property (nonatomic, copy) NSString* firstMessage;
@property BOOL isConnected;
@property BOOL isReconnectRequired;
@property (nonatomic, strong) NSMutableArray* toSendArr;
@property (nonatomic, strong) NSMutableDictionary* onActionCallbackDict;
@property (nonatomic, strong) NSMutableDictionary* onActionOutgoingCallbackDict;
@property (nonatomic, strong) NSMutableDictionary* onActionOutgoingResultDict;

@end

@implementation ME_Network
static SRWebSocket* _socket;
static ME_Network* _network;
static NSMutableDictionary* _callbacks;

+ (void)onAction:(NSString*)action setCallback:(OnMessage)onMessage
{
    if(!_network.onActionCallbackDict) {
        _network.onActionCallbackDict = [[NSMutableDictionary alloc] init];
        NSMutableArray* arr = [NSMutableArray arrayWithObject:onMessage];
        [_network.onActionCallbackDict setObject:arr forKey:action];
    } else {
        NSMutableArray* arr = _network.onActionCallbackDict[action];
        if(!arr) {
            arr = [NSMutableArray arrayWithObject:onMessage];
        } else {
            [arr addObject:onMessage];
        }
        [_network.onActionCallbackDict setObject:arr forKey:action];
    }
}

+ (void) onActionOutgoing:(NSString*)action setCallback:(OnMessage)onMessage
{
    NSDictionary* temp = _network.onActionOutgoingCallbackDict;
    [[self class] onAction:action setCallBack:onMessage withDictArr:&temp];
//    if(!_network.onActionOutgoingCallbackDict) {
//        _network.onActionOutgoingCallbackDict = [[NSMutableDictionary alloc] init];
//        NSMutableArray* arr = [NSMutableArray arrayWithObject:onMessage];
//        [_network.onActionOutgoingCallbackDict setObject:arr forKey:action];
//        NSLog(@"123");
//    } else {
//        NSMutableArray* arr = _network.onActionOutgoingCallbackDict[action];
//        if(!arr) {
//            arr = [NSMutableArray arrayWithObject:onMessage];
//        } else {
//            [arr addObject:onMessage];
//        }
//        [_network.onActionOutgoingCallbackDict setObject:arr forKey:action];
//    }
}

//+ (void) onActionOutgoingResult:(NSString*)action setCallback:(OnMessage)onMessage
//{
//    NSDictionary* temp = _network.onActionOutgoingResultDict;
//    [[self class] onAction:action setCallBack:onMessage withDictArr:&(temp)];
//}

+ (void) onAction:(NSString*)action setCallBack:(OnMessage)onMessage withDictArr:(NSMutableDictionary**)dictArr
{
    NSMutableDictionary* temp = *dictArr;
    if(!temp) {
        temp = [[NSMutableDictionary alloc] init];
        NSMutableArray* arr = [NSMutableArray arrayWithObject:onMessage];
        [temp setObject:arr forKey:action];
    } else {
        NSMutableArray* arr = temp[action];
        if(!arr) {
            arr = [NSMutableArray arrayWithObject:onMessage];
        } else {
            [arr addObject:onMessage];
        }
        [temp setObject:arr forKey:action];
    }
}

+ (void)setOnFirstMessage:(OnMessage)onMessage
{
    _network.onFirstMessage = onMessage;
}

+ (void)setReconnectRequired:(BOOL)required
{
    _network.isReconnectRequired = required;
}

+ (void)connectTo:(NSString*)path doIt:(BOOL)doIt andThen:(OnOpen)callback
{
    if(doIt) {
        [ME_Network connectTo:path andThen:callback];
    } else {
        if(callback) {
            callback(nil);
        }
    }
}

+ (void)connectTo:(NSString*)path andThen:(OnOpen)callback
{
    NSString* urlString = [NSString stringWithFormat:@"wss://login.messme.me:8101%@", path];
    NSLog(@"CONNECT TO %@", urlString);
    NSURL* url = [NSURL URLWithString:urlString];
    if(!_socket || SR_CLOSED == _socket.readyState) {
        _network.isReconnectCalled = NO;
        _socket = [[SRWebSocket alloc] initWithURL:url];
    } else {
        _network.isReconnectCalled = YES;
        [_socket close];
        _socket = [[SRWebSocket alloc] initWithURL:url];
    }
    if(!_network) {
        _network = [[ME_Network alloc] init];
    }
    _network.lastUrl = url;
    _network.onOpenCallback = callback;
    _socket.delegate = _network;
    [_socket open];
}

+ (void)send:(NSDictionary*)obj doIt:(BOOL)doIt andThen:(OnMessage)onMessage
{
    if(doIt) {
        [ME_Network send:obj andThen:onMessage];
    } else {
        if(onMessage) {
            onMessage(nil);
        }
    }
}

+ (void) send:(NSDictionary*)obj andThen:(OnMessage)onMessage
{
    if(!_socket) {
        NSLog(@"NOT CONNECTED TO THE INTERNET");
        return;
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:obj];
//    [self doHandleOnActionOutgoing:dict];
    [self doHandleOnAction:dict withDictArr:_network.onActionOutgoingCallbackDict];
    //
    if(![dict objectForKey:@"mid"]) {
        NSString* mid = [[NSUUID UUID] UUIDString];
        [dict setObject:mid forKey:@"mid"];
    }
    if(!_callbacks) {
        _callbacks = [[NSMutableDictionary alloc] init];
    }
    [_callbacks setValue:onMessage forKey:[dict objectForKey:@"mid"]];
    //
    if(_socket.readyState == SR_CONNECTING) {
        if(!_network.toSendArr) {
            _network.toSendArr = [NSMutableArray array];
        }
        [_network.toSendArr addObject:[dict JSONRepresentation]];
    } else if(_socket.readyState == SR_CLOSED) {
        
        if(!_network.toSendArr) {
            _network.toSendArr = [NSMutableArray array];
        }
        [_network.toSendArr addObject:[dict JSONRepresentation]];
        NSString* path = [NSString stringWithFormat:@"%@?%@", _network.lastUrl.path, _network.lastUrl.query];
        [[self class] connectTo:path andThen:nil];
    } else {
        NSLog(@"[Socket Command] :: %@", [dict JSONRepresentation]);
        [_socket send:[dict JSONRepresentation]];
    }
}

+ (void)downloadFileFromPath:(NSString*)path andThen:(OnDownload)onDataMessage
{
    NSString* urlString = [NSString stringWithFormat:@"https://login.messme.me:8102%@", path];
    [ME_Network downloadFileFrom:urlString andThen:onDataMessage];
}

+ (void)downloadFileFrom:(NSString*)urlString andThen:(OnDownload)onDataMessage
{
    NSURL* url = [NSURL URLWithString:urlString];
    NSLog(@"DOWNLOAD FILE %@", url);
//    NSData* urlData = [NSData dataWithContentsOfURL:url];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    NSOperationQueue* queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:req queue:queue completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable err) {
        if(err) {
            NSLog(@"DOWNLOAD ERROR: %@", err.description);
        }
        if(data) {
            dispatch_async(dispatch_get_main_queue(), ^{
                onDataMessage(data);
            });
        } else if(!data && !err) {
            NSLog(@"SOMETHING BAD");
        }
    }];
}

+ (void)uploadDoIt:(BOOL)doIt data:(NSData*)data filename:(NSString*)filename userdata:(NSArray*)userdataArr contentType:(NSString*)contentType andThen:(OnUpload)callback
{
    if(doIt) {
        [[self class] uploadFile:data filename:filename userdata:userdataArr contentType:contentType andThen:callback];
    } else {
        if(callback) {
            callback();
        }
    }
}

+ (void)uploadFile:(NSData*)data filename:(NSString*)filename userdata:(NSArray*)userdataArr contentType:(NSString*)contentType andThen:(OnUpload)callback
{
    NSLog(@"UPLOAD FILE %@ %lu", filename, data.length);
    //
    NSURL* url = [NSURL URLWithString:@"https://files.messme.me:8102/message/"];
//    NSURL* url = [NSURL URLWithString:@"http://192.168.0.27:8102/message/"];
    //
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    NSString* headerJson = [userdataArr JSONRepresentation];
    [request setValue:headerJson forHTTPHeaderField:@"User-Data"];
    //
    [request setHTTPMethod:@"POST"];
    //
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *fullContentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:fullContentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", contentType] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[NSData dataWithData:data]];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:postbody];
    //
//    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//    [request setHTTPBody:data];
    //
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable err) {
        if(err) {
            NSLog(@"UPLOAD ERROR: %@", err.description);
        }
        if(data) {
            NSLog(@"UPLOADED %@ %lu", filename, data.length);
            NSString* response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"RESPONSE: %@", response);
            callback();
        }
    }];
}


+ (BOOL)isConnected
{
    return _network.isConnected;
}

#pragma mark SRWebSocketDelegate
- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message
{
    NSLog(@"[Socket Response] :: %@", message);
    NSDictionary* dict = [message JSONValue];
    if([[dict objectForKey:@"action"] isEqualToString:@"onopen"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(self.onFirstMessage) {
                self.onFirstMessage(dict);
                self.onFirstMessage = nil;
            }
        });
    }
    for(NSString* key in _callbacks) {
        NSString* mid = [dict objectForKey:@"mid"];
        if([key isEqualToString:mid]) {
            OnMessage block = [_callbacks objectForKey:key];
            dispatch_async(dispatch_get_main_queue(), ^{
                block(dict);
            });
        }
    }
    [[self class] doHandleOnAction:dict withDictArr:_network.onActionCallbackDict];
}

//+ (void) doHandleOnAction:(NSDictionary*)dict
//{
//    for(NSString* key in _network.onActionCallbackDict) {
//        NSString* dictAction = [dict objectForKey:@"action"];
//        if([key isEqualToString:dictAction]) {
//            NSMutableArray* arr = _network.onActionCallbackDict[key];
//            for(OnMessage callback in arr) {
//                if(callback) {
//                    callback(dict);
//                } else {
//                    NSLog(@"SOMETHING WRONG");
//                }
//            }
//        }
//    }
//}

+ (void) doHandleOnAction:(NSDictionary*)dict withDictArr:(NSDictionary*)dictArr
{
    for(NSString* key in dictArr) {
        NSString* dictAction = [dict objectForKey:@"action"];
        if([key isEqualToString:dictAction]) {
            NSMutableArray* arr = dictArr[key];
            for(OnMessage callback in arr) {
                if(callback) {
                    callback(dict);
                } else {
                    NSLog(@"SOMETHING WRONG");
                }
            }
        }
    }
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket
{
    NSLog(@"didOpen %@", webSocket.url);
    _network.isConnected = YES;
    //
    if(_network.onOpenCallback) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _network.onOpenCallback(webSocket);
            _network.onOpenCallback = nil;
        });
    }
    for(int i = 0; i < _network.toSendArr.count; i++) {
        NSString* json = _network.toSendArr[i];
        NSLog(@"[Socket Command] :: %@", json);
        @try {
            if(_socket.readyState != SR_CONNECTING) {
                [_socket send:json];
            } else {
                NSLog(@"FUCKING SOCKET");
                [_network.toSendArr addObject:json];
            }
            [_network.toSendArr removeObjectAtIndex:i];
        }
        @catch(NSError* err) {
            NSLog(@"NETWORK ERROR %@", err.description);
        }
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError %@ %@", webSocket.url, error);
    _network.isConnected = NO;
}
- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean
{
    NSLog(@"didCloseWithCode %@ code:%ld reason:%@ wasClean:%d", webSocket.url,(long)code, reason, wasClean);
    _network.isConnected = NO;
    if(_network.isReconnectCalled || !_network.isReconnectRequired) {
        // do nothing
        NSLog(@"DO NOT RECONNECT");
    } else {
        NSLog(@"RECONNECT");
        _socket = [[SRWebSocket alloc] initWithURL:_network.lastUrl];
        _socket.delegate = _network;
        [_socket open];
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload
{
    NSLog(@"didReceivePong %@ %@", webSocket.url, pongPayload);
}

+ (BOOL)isAvailable
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable;
}

@end
