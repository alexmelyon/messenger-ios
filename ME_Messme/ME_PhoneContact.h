//
//  ME_PhoneContact.h
//  ME_Messme
//
//  Created by MessMe on 08/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ME_PhoneContact : NSObject

@property NSString* phone;
@property (nonatomic, copy) NSString* fullname;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* surname;

+ (ME_PhoneContact*) phoneContactWithDictionary:(NSDictionary*)dict;

@end
