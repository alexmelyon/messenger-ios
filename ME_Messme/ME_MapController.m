//
//  ME_MapController.m
//  ME_Messme
//
//  Created by MessMe on 20/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_MapController.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "ME_Network.h"
#import "NSString+Util.h"
#import "AppDelegate.h"
#import "ME_ImageCache.h"
@import Foundation;
#import "ME_ChatDetailController.h"
#import "ME_FriendProfileController.h"
#import "ME_FriendAnnotation.h"
#import "ME_CountryController.h"
#import "ME_CityController.h"
#import "ME_Country.h"
#import "ME_City.h"
#import "NSString+Util.h"
#import "UIImageView+Utils.h"
#import "UIImage+Utils.h"

static NSString* SEGUE_CHAT = @"segueChat";

@interface ME_MapController ()
<CLLocationManagerDelegate,
MKMapViewDelegate,
UIActionSheetDelegate,
UIPickerViewDataSource,
UIPickerViewDelegate,
ME_CountryDelegate,
ME_CityDelegate,
UITextFieldDelegate>

@property (nonatomic, strong) CLLocationManager* locationManager;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) CLLocation* currentLocation;
@property BOOL isFirstShow;
@property BOOL isFriend;
@property (nonatomic, strong) NSMutableArray* userlist;
@property long long useridToSend;
@property (weak, nonatomic) IBOutlet UIView *filterView;
@property (weak, nonatomic) IBOutlet UILabel *showMeLabel;
@property (weak, nonatomic) IBOutlet UISwitch *showMeSwitch;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UITextField *ageValueField;
@property (weak, nonatomic) IBOutlet UILabel *sexLabel;
@property (weak, nonatomic) IBOutlet UILabel *sexValueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *countryImageView;
@property (weak, nonatomic) IBOutlet UILabel *countryCityLabel;
@property (weak, nonatomic) IBOutlet UIImageView *countryCityArrowImageView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *membersSegmented;
- (IBAction)membersChangeAction:(id)sender;
- (IBAction)showMeAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *menuListButton;
- (IBAction)menuListAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *menuFilterButton;
- (IBAction)menuFilterAction:(id)sender;
@property (nonatomic, strong) UIPickerView* agePicker;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UIButton *minusButton;
@property (weak, nonatomic) IBOutlet UIButton *whereamiButton;
- (IBAction)plusAction:(id)sender;
- (IBAction)minusAction:(id)sender;
- (IBAction)whereamiAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
// model
@property BOOL filterIsShowMe;
@property BOOL filterIsMembersAll;
@property (nonatomic, strong) ME_Country* filterCountry;
@property (nonatomic, strong) ME_City* filterCity;
//@property int filterCountryId;
//@property int filterCityId;
@property NSString* filterSex;
@property int filterMinAge;
@property int filterMaxAge;
@property (nonatomic, copy) NSString* filterName;

@end

@implementation ME_MapController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    self.mapView.delegate = self;
    // make navbar transparent
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    // back to default
//    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    //
    self.filterView.hidden = YES;
    self.showMeLabel.userInteractionEnabled = YES;
    [self.showMeLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMeAction:)]];
    [self configureAgeViews];
    self.sexLabel.userInteractionEnabled = YES;
    [self.sexLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeSexAction:)]];
    self.sexValueLabel.userInteractionEnabled = YES;
    [self.sexValueLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeSexAction:)]];
    self.countryImageView.userInteractionEnabled = YES;
    [self.countryImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeCountryCityAction:)]];
    self.countryCityLabel.userInteractionEnabled = YES;
    [self.countryCityLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeCountryCityAction:)]];
    self.countryCityArrowImageView.userInteractionEnabled = YES;
    [self.countryCityArrowImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeOrRemoveCountryCityAction:)]];
    self.plusButton.layer.cornerRadius = 5;
    self.minusButton.layer.cornerRadius = 5;
    self.whereamiButton.layer.cornerRadius = 5;
    //
    self.filterIsShowMe = YES;
    self.showMeSwitch.selected = YES;
    [self updateCountryCityViews];
    self.menuListButton.hidden = YES;
}

- (void) configureAgeViews
{
    self.ageLabel.userInteractionEnabled = YES;
    [self.ageLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickAgeLabelAction:)]];
    self.agePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    self.agePicker.dataSource = self;
    self.agePicker.delegate = self;
    self.agePicker.showsSelectionIndicator = YES;
    self.ageValueField.inputView = self.agePicker;
    //
    UIToolbar* toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Отмена"
                                                            style:UIBarButtonItemStylePlain
                                                           target:self
                                                           action:@selector(cancelToolbar:)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"ОК" style:UIBarButtonItemStyleDone
                                                           target:self
                                                           action:@selector(doneToolbar:)]];
    [toolbar sizeToFit];
    self.ageValueField.inputAccessoryView = toolbar;
}

- (void) cancelToolbar:(id)sender
{
    NSLog(@"cancelToolbar");
    [self.ageValueField resignFirstResponder];
}

- (void) doneToolbar:(id)sender
{
    NSLog(@"doneToolbar");
    [self.ageValueField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager setDelegate:self];
    [self.locationManager requestLocation];
    //
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if([annotation isKindOfClass:[ME_FriendAnnotation class]]) {
        static NSString* identifier = @"friendAnnotation";
        MKAnnotationView* av = [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if(!av) {
            av = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            av.canShowCallout = YES;
        }
        ME_FriendAnnotation* friend = (ME_FriendAnnotation*)annotation;
        av.image = friend.image;
        av.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage: friend.image];
        av.leftCalloutAccessoryView.userInteractionEnabled = YES;
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickAvatarAction:)];
        [av.leftCalloutAccessoryView addGestureRecognizer:tap];
        UIButton* b = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 34, 34)];
        [b setImage:[UIImage imageNamed:@"tab_messages"] forState:UIControlStateNormal];
        [b addTarget:self action:@selector(calloutAction:) forControlEvents:UIControlEventTouchUpInside];
        av.rightCalloutAccessoryView = b;
        return av;
    }
    return nil;
}

- (void) clickAvatarAction:(UITapGestureRecognizer*)sender
{
    NSLog(@"clickAvatarAction");
    // ppc
    MKAnnotationView* a = (MKAnnotationView*)[[[[[[[[[sender view] superview] superview] superview] superview] superview] superview] superview] superview];
    ME_FriendAnnotation* friend = [a annotation];
    //
    [ME_FriendProfileController showProfileUserid:friend.ID fromCtrl:self];
}

- (void) calloutAction:(id)sender
{
    NSLog(@"calloutAction");
    // ppc
    MKAnnotationView* a = (MKAnnotationView*)[[[[[[[[sender superview] superview] superview] superview] superview] superview] superview] superview];
    ME_FriendAnnotation* friend = [a annotation];
    // go to messages
    self.useridToSend = friend.ID;
    [self performSegueWithIdentifier:SEGUE_CHAT sender:nil];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    
    NSLog(@"didChangeAuthorizationStatus %d", status);
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    if ([locations count] == 0) {
        // error
        return;
    }
    
    // success
    self.mapView.showsUserLocation = self.filterIsShowMe;
    if(!self.isFirstShow) {
        self.isFirstShow = YES;
        self.currentLocation = [locations lastObject];
        CLLocationCoordinate2D myLoc = CLLocationCoordinate2DMake(self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude);
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(myLoc, 2000, 2000);
        MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
        [self.mapView setRegion:adjustedRegion animated:NO];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(nonnull NSError *)error
{
    NSLog(@"locationManager didFailWithError");
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([SEGUE_CHAT isEqualToString:segue.identifier]) {
        UINavigationController* navCtrl = segue.destinationViewController;
        ME_ChatDetailController* ctrl = navCtrl.viewControllers[0];
        ctrl.userlist = [NSMutableArray arrayWithObject:@(self.useridToSend)];
    }
}


- (IBAction)membersChangeAction:(UISegmentedControl*)sender {
    NSLog(@"membersChangeAction");
    self.filterIsMembersAll = sender.selectedSegmentIndex == 0;
    [self updateMap];
}
- (IBAction)showMeAction:(id)sender {
    NSLog(@"showMeAction");
    self.filterIsShowMe = !self.filterIsShowMe;
//    self.mapView.showsUserLocation = self.filterIsShowMe;
}
- (IBAction)menuListAction:(id)sender {
    NSLog(@"menuListAction");
}
- (IBAction)menuFilterAction:(id)sender {
    NSLog(@"menuFilterAction");
    self.filterView.hidden = !self.filterView.hidden;
}
- (void) clickAgeLabelAction:(id)sender
{
    NSLog(@"clickAgeLabelAction");
    [self.ageValueField becomeFirstResponder];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 100;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(row) {
        return [NSString stringWithFormat:@"%d", (int)row];
    } else {
        return @"Нет";
    }
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(0 == component) {
        self.filterMinAge = (int)row;
    } else if(1 == component) {
        self.filterMaxAge = (int)row;
    }
    if(self.filterMinAge || self.filterMaxAge) {
        NSString* min = self.filterMinAge ? [NSString stringWithFormat:@"%d", self.filterMinAge] : @"..";
        NSString* max = self.filterMaxAge ? [NSString stringWithFormat:@"%d", self.filterMaxAge] : @"..";
        self.ageValueField.text = [NSString stringWithFormat:@"%@ - %@", min, max];
    } else {
        self.ageValueField.text = @"Не задан";
    }
}
- (void) changeSexAction:(id)sender
{
    NSLog(@"changeSexAction");
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Отмена"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Мужской", @"Женский", @"Любой", nil];
    [actionSheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(0 == buttonIndex) {
        self.filterSex = @"m";
        self.sexValueLabel.text = @"Мужской";
    } else if(1 == buttonIndex) {
        self.filterSex = @"f";
        self.sexValueLabel.text = @"Женский";
    } else if(2 == buttonIndex) {
        self.filterSex = @"";
        self.sexValueLabel.text = @"Любой";
    }
    [self updateMap];
}
- (void) changeOrRemoveCountryCityAction:(id)sender
{
    if(self.filterCountry) {
        self.filterCountry = nil;
        self.filterCity = nil;
        [self updateCountryCityViews];
    } else {
        [self changeCountryCityAction:sender];
    }
}
- (void) changeCountryCityAction:(id)sender
{
    NSLog(@"changeCountryCityAction");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ME_CountryController* ctrl = [storyboard instantiateViewControllerWithIdentifier:@"countryCtrl"];
    ctrl.delegate = self;
    [self.navigationController pushViewController:ctrl animated:YES];
}
- (void)selectCountry:(NSDictionary *)country from:(id)sender
{
    NSLog(@"selectCountry");
    self.filterCountry = [[ME_Country alloc] initWithDict:country];
    [self.navigationController popViewControllerAnimated:NO];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ME_CityController* ctrl = [storyboard instantiateViewControllerWithIdentifier:@"cityCtrl"];
    ctrl.countryid = self.filterCountry.countryId;
    ctrl.delegate = self;
    ctrl.isShowDoneButton = YES;
    ctrl.doneButtonText = @"Любой";
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (void)selectCity:(NSDictionary *)city from:(id)sender
{
    NSLog(@"selectCity");
    self.filterCity = [[ME_City alloc] initWithDict:city];
    [self.navigationController popViewControllerAnimated:YES];
    [self updateCountryCityViews];
    [self updateMap];
}
- (void) updateCountryCityViews
{
    if(self.filterCountry) {
        NSString* iso = [NSString stringWithFormat:@"%d", self.filterCountry.iso];
        [self.countryImageView setRoundedImage:[UIImage imageNamed:iso]];
        if(self.filterCity) {
            self.countryCityLabel.text = [[self.filterCountry.countryName plus:@", "] plus:self.filterCity.cityName];
        } else {
            self.countryCityLabel.text = self.filterCountry.countryName;
        }
        self.countryCityArrowImageView.image = [UIImage imageNamed:@"no-sm"];
    } else {
        [self.countryImageView setRoundedImage:[UIImage imageNamed:@"_united-nations"]];
        self.countryCityLabel.text = @"Все страны";
        self.countryCityArrowImageView.image = [UIImage imageNamed:@"arrow_right_gray"];
    }
}

- (int) filterCountryId
{
    return self.filterCountry.countryId;
}

- (int) filterCityId
{
    return self.filterCity.cityId;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString* result = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.filterName = result;
    [self updateMap];
    return YES;
}

- (void) updateMap
{
    int isFriend = self.filterIsMembersAll ? -1 : 1;
    int countryId = self.filterCountryId;
    int cityId = self.filterCityId;
    NSString* sex = self.filterSex.length ? self.filterSex : @"";
    int minAge = self.filterMinAge;
    int maxAge = self.filterMaxAge;
    NSString* mask = [self.filterName trim].length ? self.filterName : @"";
    double lat1 = 0;
    double lng1 = 0;
    double lat2 = 0;
    double lng2 = 0;
    id opts = @{@"userlist":@[],
                @"status":@(1),
                @"locale":@"ru",
                @"isfriend":@(isFriend),
                @"country":@(countryId),
                @"city":@(cityId),
                @"sex":sex,
                @"minage":@(minAge),
                @"maxage":@(maxAge),
                @"mask":mask,
                @"lat1":@(lat1),
                @"lng1":@(lng1),
                @"lat2":@(lat2),
                @"lng2":@(lng2)};
    id msg = @{@"action":@"user.list", @"options":opts};
    [ME_Network send:msg andThen:^(NSDictionary *json) {
        if(1000 == [json[@"status"] intValue]) {
            NSArray* result = json[@"result"];
            self.userlist = [NSMutableArray array];
            for(NSDictionary* item in result) {
                ME_FriendAnnotation* friend = [[ME_FriendAnnotation alloc] initWithDict:item];
                [self.userlist addObject:friend];
                [self.mapView addAnnotation:friend];
                [ME_ImageCache getAvatarForFriend:friend andThen:^(UIImage *image) {
                    MKAnnotationView* av = [self.mapView viewForAnnotation:friend];
                    av.image = [image resizeTo:CGSizeMake(25.0, 25.0)];
                    av.layer.cornerRadius = image.size.width / 2;
                    av.layer.masksToBounds = YES;
                    av.layer.borderWidth = 2;
                    av.layer.borderColor = [[UIColor blueColor] CGColor];
                }];
            }
        }
    }];
}

- (IBAction)plusAction:(id)sender {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    region.center=self.mapView.region.center;
    span.latitudeDelta=self.mapView.region.span.latitudeDelta /2.0002;
    span.longitudeDelta=self.mapView.region.span.longitudeDelta /2.0002;
    region.span=span;
    [self.mapView setRegion:region animated:YES];
}

- (IBAction)minusAction:(id)sender {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    region.center=self.mapView.region.center;
    span.latitudeDelta=self.mapView.region.span.latitudeDelta *2;
    span.longitudeDelta=self.mapView.region.span.longitudeDelta *2;
    region.span=span;
    [self.mapView setRegion:region animated:YES];
}

- (IBAction)whereamiAction:(id)sender {
    self.isFirstShow = NO;
}
@end
