//
//  ME_ContactCellModel.m
//  ME_Messme
//
//  Created by MessMe on 12/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_SearchItem.h"

@implementation ME_SearchItem

- (ME_SearchItem*) initWithName:(NSString*)name surname:(NSString*)surname ID:(long long)ID login:(NSString*)login avatar:(NSString*)avatar
{
    self = [super init];
    self.name = name;
    self.surname = surname;
    self.ID = ID;
    self.login = login;
    self.avatar = avatar;
    return self;
}
@end
