//
//  ME_Country.m
//  ME_Messme
//
//  Created by MessMe on 02/03/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_Country.h"

@implementation ME_Country

- (instancetype) initWithDict:(NSDictionary*)dict
{
    self = [super init];
    self.continentId = [dict[@"continentid"] intValue];
    self.continentName = dict[@"continentname"];
    self.countryId = [dict[@"countryid"] intValue];
    self.countryName = dict[@"countryname"];
    self.iso = [dict[@"iso"] intValue];
    self.phoneCode = [dict[@"phonecode"] intValue];
    return self;
}

@end
