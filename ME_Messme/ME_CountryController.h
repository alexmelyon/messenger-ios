//
//  ME_CountryController.h
//  ME_Messme
//
//  Created by MessMe on 25/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ME_CountryDelegate <NSObject>

- (void) selectCountry:(NSDictionary*)country from:(id)sender;

@end

@interface ME_CountryController : UITableViewController

@property (nonatomic, weak) id<ME_CountryDelegate> delegate;

@end
