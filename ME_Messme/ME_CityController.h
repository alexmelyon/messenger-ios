//
//  ME_CityController.h
//  ME_Messme
//
//  Created by MessMe on 02/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ME_CityDelegate <NSObject>

- (void) selectCity:(NSDictionary*)city from:(id)sender;

@end

@interface ME_CityController : UITableViewController

@property int countryid;
@property (nonatomic, weak) id<ME_CityDelegate> delegate;
@property BOOL isShowDoneButton;
@property (nonatomic, copy) NSString* doneButtonText;

@end
