//
//  ME_NewGroupController.m
//  ME_Messme
//
//  Created by MessMe on 24/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_NewGroupController.h"
#import "ME_Network.h"
#import "UIImage+Utils.h"
#import "UIImageView+Utils.h"
#import "ME_ContactsForGroupController.h"
#import "NSString+Util.h"

static NSString* SEGUE_CONTACTS = @"segueContacts";

@interface ME_NewGroupController () <
UITextFieldDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
ME_ContactsForGroupDelegate,
UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UITextField *themeField;
- (IBAction)backAction:(id)sender;
- (IBAction)doneAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIButton *chooseContactsButton;
@property (weak, nonatomic) IBOutlet UIImageView *chooseContactsImageView;
//@property (weak, nonatomic) IBOutlet UILabel *createdLabel;
- (IBAction)selectContacts:(id)sender;
@property (nonatomic, copy) NSString* themeString;
@property (nonatomic, strong) UIImage* avatarImage;
@property (weak, nonatomic) IBOutlet UITextView *descTextView;
@property (nonatomic, copy) NSString* descString;
@property (nonatomic, strong) ME_GroupModel* grpModel;

@end

@implementation ME_NewGroupController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer* avatarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectAvatar:)];
    [self.avatarImageView addGestureRecognizer:avatarTap];
    //
    UITapGestureRecognizer* chooseContactsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectContacts:)];
    [self.chooseContactsImageView addGestureRecognizer:chooseContactsTap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) selectAvatar:(id)sender
{
    NSLog(@"selectAvatar");
    UIImagePickerController* ctrl = [[UIImagePickerController alloc] initWithRootViewController:self];
    ctrl.delegate = self;
    ctrl.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    [self.navigationController presentViewController:ctrl animated:YES completion:nil];
//    [self presentModalViewController:ctrl animated:YES];
//    [self presentViewController:ctrl animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage* img = [info objectForKey:UIImagePickerControllerOriginalImage];
    if(img) {
        UIImage* newImage = [img cropCenterToSize:CGSizeMake(77, 77)];
        self.avatarImage = newImage;
//        self.avatarImageView.image = newImage;
//        self.avatarImageView.layer.cornerRadius = 77.0/2;
//        self.avatarImageView.clipsToBounds = YES;
        [self.avatarImageView setRoundedImage:newImage];
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    textView.text = @"";
    textView.textColor = [UIColor blackColor];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString* result = [textView.text stringByReplacingCharactersInRange:range withString:text];
    self.descString = result;
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString* result = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.themeString = result;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL) textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)selectContacts:(id)sender {
    NSLog(@"selectContacts");
    if(![self.themeString trim].length) {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Пожалуйста, введите тему группы" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    NSString* avatar64 = [self.avatarImage toBase64String];
    if(!avatar64) {
        avatar64 = @"";
    }
    if(![self.descString trim].length) {
        self.descString = @"";
    }
    id opts = @{@"name":self.themeString, @"description":self.descString, @"avatar":avatar64, @"userlist":@[]};
    id msg = @{@"action":@"groupchat.create",@"options":opts};
    [ME_Network send:msg andThen:^(NSDictionary *json) {
        if(1000 == [json[@"status"] intValue]) {
            NSDictionary* result = json[@"result"];
//            [self.grpModel updateWithDict:result];
            self.grpModel = [[ME_GroupModel alloc] initWithDict:result];
            [self performSegueWithIdentifier:SEGUE_CONTACTS sender:nil];
        } else {
            NSLog(@"ERROR GROUPCHAT UPDATE");
        }
    }];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([SEGUE_CONTACTS isEqualToString:segue.identifier]) {
        ME_ContactsForGroupController* ctrl = segue.destinationViewController;
        ctrl.delegate = self;
        ctrl.grpModel = self.grpModel;
    }
}

//- (void)groupController:(UITableViewController *)grpCtrl createGroup:(ME_GroupModel *)grpModel
- (void) groupController:(UITableViewController*)grpCtrl addContactsForGroup:(ME_GroupModel*)grpModel;
{
    NSLog(@"createGroup");
    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
    [self.delegate newGroupCtrl:self groupUpdated:grpModel];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doneAction:(id)sender {
    [self selectContacts:sender];
}
@end
