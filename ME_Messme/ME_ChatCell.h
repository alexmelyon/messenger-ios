//
//  ME_ChatCell.h
//  ME_Messme
//
//  Created by MessMe on 23/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ME_MessageModel.h"
#import "ME_Attachment.h"
@import Contacts;

@protocol ME_ChatCellDelegate

- (void) chatCell:(UITableViewCell*)cell imageClicked:(ME_Attachment*)attach;
- (void) chatCell:(UITableViewCell*)cell videoClicked:(ME_Attachment*)attach;
- (void) chatCell:(UITableViewCell*)cell audioClicked:(ME_Attachment*)attach;
- (void) chatCell:(UITableViewCell*)cell geoClicked:(ME_Attachment*)attach;
- (void) chatCell:(UITableViewCell*)cell addContactClicked:(CNContact*)contact;

@end

@interface ME_ChatCell : UITableViewCell

@property (nonatomic, weak) id<ME_ChatCellDelegate> delegate;
@property (nonatomic, weak) IBOutlet UILabel *userLabel;
//@property BOOL isAudioPlaying;
@property (nonatomic, strong) ME_MessageModel* msgModel;

- (void) setModel:(ME_MessageModel*)dict;
- (void) setPlaying:(BOOL)isPlaying attach:(ME_Attachment*)attach progress:(double)duration;

@end
