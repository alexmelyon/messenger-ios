//
//  ME_UserCell.m
//  ME_Messme
//
//  Created by MessMe on 17/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_UserCell.h"
#import "ME_ImageCache.h"
#import "NSString+Util.h"
#import "NSDate+Utils.h"
#import "AppDelegate.h"
#import "UIImage+Utils.h"
#import "UIImageView+Utils.h"

@interface ME_UserCell ()
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *topLabel; // username
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel; // message
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *badgeLabel; // unread
@property (nonatomic, strong) ME_Dialog* dialog;

@end

@implementation ME_UserCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (ME_UserCell*) configureWithDialog:(ME_Dialog*)dialog
{
    self.dialog = dialog;
    [[AppDelegate shared] getUserById:[dialog getSourceUserid] andThen:^(ME_Friend *meFriend) {
        NSString* name = [meFriend getPresentName];
        self.topLabel.text = name;
        self.iconImageView.image = [UIImage blankImageWithSize:self.iconImageView.frame.size color:[UIColor whiteColor]];
        [ME_ImageCache getAvatarForFriend:meFriend andThen:^(UIImage* image) {
            [self.iconImageView setRoundedImage:image];
        }];
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectAvatar:)];
        self.iconImageView.userInteractionEnabled = YES;
        [self.iconImageView addGestureRecognizer:tap];
    }];
//    self.bottomLabel.text = dialog.message; // no debug
    self.bottomLabel.text = [dialog.message plusLongLong:[dialog getSourceUserid]]; // debug
    self.timeLabel.text = [[dialog.dateStr toDateFromUtc] fromNow];
    if(dialog.msgUnread) {
        self.badgeLabel.text = [@"" plusLongLong:dialog.msgUnread];
    } else {
        self.badgeLabel.text = @"";
    }
    return self;
}

- (void) selectAvatar:(UITapGestureRecognizer*)sender
{
    [self.delegate userCell:self clickedAvatarWithDialog:self.dialog];
}

@end
