//
//  ME_GeoViewController.m
//  ME_Messme
//
//  Created by MessMe on 27/01/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_GeoViewController.h"
//#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface ME_GeoViewController () <CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

- (IBAction)backAction:(id)sender;
@property (nonatomic, strong) CLLocationManager* locationMgr;
@property (nonatomic, strong) MKPointAnnotation* point;

@end

@implementation ME_GeoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //
    self.mapView.showsUserLocation = YES;
    if(self.lat && self.lng) {
        CLLocationCoordinate2D myLoc = CLLocationCoordinate2DMake(self.lat, self.lng);
        //    [self.mapView setCenterCoordinate:self.currentLocation.coordinate animated:NO];
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(myLoc, 1000, 1000);
        MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
        [self.mapView setRegion:adjustedRegion animated:NO];
        //
        // Place a single pin
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        [annotation setCoordinate:myLoc];
//        [annotation setTitle:@"Title"]; //You can set the subtitle too
        [self.mapView addAnnotation:annotation];
    } else {
        self.locationMgr = [[CLLocationManager alloc] init];
        self.locationMgr.delegate = self;
        self.locationMgr.distanceFilter = kCLDistanceFilterNone;
        self.locationMgr.desiredAccuracy = kCLLocationAccuracyBest;
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            [self.locationMgr requestWhenInUseAuthorization];
        }
        [self.locationMgr startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    [self.locationMgr stopUpdatingLocation];
    CLLocation* loc = [locations lastObject];
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(loc.coordinate, 1000, 1000);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    [self.mapView setRegion:adjustedRegion animated:NO];
    //
    // Place a single pin
    self.point = [[MKPointAnnotation alloc] init];
    [self.point setCoordinate:loc.coordinate];
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView addAnnotation:self.point];
    //
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
    [self.mapView addGestureRecognizer:tap];
}

- (void) onTap:(UITapGestureRecognizer*)recognizer
{
    CGPoint touchPoint = [recognizer locationInView:self.mapView];
    CLLocationCoordinate2D coord = [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
    self.point.coordinate = coord;
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doneAction:(id)sender
{
    if(self.lat && self.lng) {
        [self backAction:sender];
    } else {
        [self.delegate ctrl:self chooseGeoLat:self.point.coordinate.latitude lng:self.point.coordinate.longitude];
        [self backAction:sender];
    }
}

@end
