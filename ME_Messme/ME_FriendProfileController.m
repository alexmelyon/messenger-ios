//
//  ME_FriendProfileController.m
//  ME_Messme
//
//  Created by MessMe on 29/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_FriendProfileController.h"
#import "ME_ImageCache.h"
#import "UIImageView+Utils.h"
#import "ME_Network.h"
#import "ME_ChatDetailController.h"
@import Contacts;
@import ContactsUI;
#import "NSString+Util.h"

@interface ME_FriendProfileController () <CNContactViewControllerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *loginLabel;
@property (weak, nonatomic) IBOutlet UILabel *avatarTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UILabel *sexLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UISwitch *lockSwitch;
@property (weak, nonatomic) IBOutlet UILabel *mediaLabel;
@property (weak, nonatomic) IBOutlet UILabel *sharedGroups;
@property (weak, nonatomic) IBOutlet UIButton *clearDialogsButton;
@property (weak, nonatomic) IBOutlet UIButton *gotoMessagesButton;

- (IBAction)backAction:(id)sender;
- (IBAction)saveAction:(id)sender;
- (IBAction)gotoMessagesAction:(id)sender;
- (IBAction)clearDialogsAction:(id)sender;
- (IBAction)likeAction:(id)sender;

@end

@implementation ME_FriendProfileController

+ (void) showProfileUserid:(long long)userid fromCtrl:(UIViewController*)fromCtrl
{
    [[AppDelegate shared] getUserById:userid andThen:^(ME_Friend *meFriend) {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ME_FriendProfileController* ctrl = [storyboard instantiateViewControllerWithIdentifier:@"friendProfile"];
        ctrl.friend = meFriend;
        UINavigationController* navCtrl = [[UINavigationController alloc] initWithRootViewController:ctrl];
        [fromCtrl presentViewController:navCtrl animated:YES completion:nil];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.scrollView.contentSize = CGSizeMake(320.0, 550.0);
    //
    self.nameLabel.text = [self.friend getPresentName];
    self.loginLabel.text = self.friend.login;
    [ME_ImageCache getAvatarForFriend:self.friend size:self.avatarImageView.frame.size andThen:^(UIImage *image) {
        [self.avatarImageView setRoundedImage:image];
    }];
    [self updateLikeButton];
    self.ageLabel.text = [NSString stringWithFormat:@"%d", self.friend.age];
    self.countryLabel.text = self.friend.countryname;
    self.cityLabel.text = self.friend.cityname;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) updateLikeButton
{
    NSString* likes = [NSString stringWithFormat:@" %d", self.friend.likes];
    [self.likeButton setTitle:likes forState:UIControlStateNormal];
    if(self.friend.isLiked) {
        [self.likeButton setImage:[UIImage imageNamed:@"thumbs_up_blue"] forState:UIControlStateNormal];
    } else {
        [self.likeButton setImage:[UIImage imageNamed:@"thumbs_up_gray"] forState:UIControlStateNormal];
    }
    // TODO change label and image places
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveAction:(id)sender
{
    NSLog(@"saveAction");
    CNMutableContact* contact = [[CNMutableContact alloc] init];
    if([self.friend.name trim].length) {
        contact.givenName = self.friend.name;
    } else {
        contact.givenName = [self.friend getPresentName];
    }
    contact.familyName = self.friend.surname;
    contact.phoneNumbers = @[[CNLabeledValue labeledValueWithLabel:CNLabelPhoneNumberMobile value:[CNPhoneNumber phoneNumberWithStringValue:[NSString stringWithFormat:@"%lld", self.friend.ID]]]];
    CNContactStore *store = [[CNContactStore alloc] init];
    CNContactViewController *controller = [CNContactViewController viewControllerForNewContact:contact];
    controller.contactStore = store;
    controller.delegate = self;
    UINavigationController* navCtrl = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navCtrl animated:YES completion:nil];
}

- (void)contactViewController:(CNContactViewController *)viewController didCompleteWithContact:(CNContact *)contact
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)clearDialogsAction:(id)sender {
    NSLog(@"clearDialogsAction");
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Удалить все диалоги с этим человеком?" delegate:self cancelButtonTitle:@"Нет" otherButtonTitles:@"Да", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(1 == buttonIndex) {
        [ME_Network send:@{@"action":@"message.clearhistory", @"options":@{@"userid":@(self.friend.ID)}} andThen:^(NSDictionary *json) {
            if(1000 == [json[@"status"] intValue]) {
                [[[UIAlertView alloc] initWithTitle:nil message:@"История сообщений с пользователем очищена" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            } else {
                NSLog(@"SOMETHING WRONG WITH MESSAGE.CLEARHISTORY");
            }
        }];
    }
}

- (IBAction)likeAction:(id)sender
{
    int value = self.friend.isLiked ? 0 : 1;
    [ME_Network send:@{@"action":@"user.like", @"options":@{@"id":@(self.friend.ID), @"value":@(value)}} andThen:^(NSDictionary *json) {
        if(1000 == [json[@"status"] intValue]) {
            int result = [json[@"result"] intValue];
            self.friend.isLiked = !self.friend.isLiked;
            self.friend.likes = result;
            [self updateLikeButton];
        } else {
            NSLog(@"SOMETHING WRONG WITH USER.LIKE");
        }
    }];
}

- (IBAction)gotoMessagesAction:(id)sender
{
    [ME_ChatDetailController showChatUserid:self.friend.ID fromCtrl:self];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
