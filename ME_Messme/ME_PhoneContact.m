//
//  ME_PhoneContact.m
//  ME_Messme
//
//  Created by MessMe on 08/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_PhoneContact.h"

@implementation ME_PhoneContact

+ (ME_PhoneContact*) phoneContactWithDictionary:(NSDictionary*)dict
{
    ME_PhoneContact* phoneContact = [[ME_PhoneContact alloc] init];
    phoneContact.phone = [dict objectForKey:@"phone"];
    phoneContact.fullname = [dict objectForKey:@"fullname"];
    phoneContact.name = [dict objectForKey:@"name"];
    phoneContact.surname = [dict objectForKey:@"surname"];
    return phoneContact;
}

@end
