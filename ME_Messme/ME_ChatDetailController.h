//
//  ME_ChatDetailController.h
//  ME_Messme
//
//  Created by MessMe on 18/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ME_Dialog.h"
#import "ME_GroupModel.h"

@interface ME_ChatDetailController : UIViewController

//@property (nonatomic, strong) NSMutableArray* userlist;
//@property (nonatomic, strong) ME_Dialog* dlgModel;
@property (nonatomic, strong) ME_GroupModel* grpModel;

- (void) setUserlist:(NSMutableArray*)userlist;
- (NSMutableArray*) userlist;
+ (void) showChatUserid:(long long)userid fromCtrl:(UIViewController*)fromCtrl;

@end
