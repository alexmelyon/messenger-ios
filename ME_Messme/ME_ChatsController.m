//
//  ME_ChatsController.m
//  ME_Messme
//
//  Created by MessMe on 17/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_ChatsController.h"
#import "ME_Network.h"
#import "ME_UserCell.h"
#import "ME_GroupCell.h"
#import "ME_Friend.h"
#import "AppDelegate.h"
#import "ME_ChatDetailController.h"
#import "ME_Dialog.h"
#import "ME_MessageModel.h"
#import "NSDate+Utils.h"
#import "ME_NewGroupController.h"
#import "ME_FriendProfileController.h"

static NSString* SEGUE_CHAT = @"segueChat";
static NSString* SEGUE_CHAT_NO_ANIM = @"segueChatNoAnim";
static NSString* SEGUE_NEW_GROUP = @"segueNewGroup";

@interface ME_ChatsController () <
UIActionSheetDelegate,
ME_UserCellDelegate,
ME_GroupCellDelegate,
AppDelegateContactsListener,
//ME_ContactsForGroupDelegate
ME_NewGroupControllerDelegate
>

@property (nonatomic, strong) UIView* emptyView;
@property (nonatomic, strong) NSMutableArray* lastlistArr;
- (IBAction)addAction:(id)sender;
- (void) putEmptyView;
- (void) putTableView;
//@property long long useridToSend;
@property (nonatomic, strong) ME_Dialog* dialogToSend;
@property (nonatomic, strong) ME_GroupModel* groupToSend;

@end

@implementation ME_ChatsController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    [[AppDelegate shared] addContactsListener:self];
    //
    CGRect frame = self.tableView.frame;
    self.emptyView = [[UIView alloc] initWithFrame:frame];
    self.emptyView.backgroundColor = [UIColor colorWithRed:245/255.0 green:244/255.0 blue:243/255.0 alpha:1.0];
    //        UIGraphicsBeginImageContext(self.emptyView.frame.size);
    //        [[UIImage imageNamed:@"pattern_messages"] drawInRect:self.emptyView.bounds];
    //        UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    //        UIGraphicsEndImageContext();
    //        self.emptyView.backgroundColor = [UIColor colorWithPatternImage:image];
    //
    UILabel* topLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 76)];
    topLabel.text = @"У вас нет диалогов\nгрупп и рассылок";
    topLabel.numberOfLines = 0;
    topLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    topLabel.textAlignment = NSTextAlignmentCenter;
    [self.emptyView addSubview:topLabel];
    //
    UIImageView* middleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 135-64, 320, 150)];
    middleImageView.contentMode = UIViewContentModeCenter;
    middleImageView.image = [UIImage imageNamed:@"logo-blue"];
    [self.emptyView addSubview:middleImageView];
    //
    UILabel* bottomLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 330-64, 320, 70)];
    bottomLabel.text = @"Создайте диалог, группу или\nрассылку для участников из\nвашего списка Контактов";
    bottomLabel.numberOfLines = 0;
    bottomLabel.textAlignment = NSTextAlignmentCenter;
    bottomLabel.textColor = [UIColor colorWithRed:103/255.0 green:105/255.0 blue:107/255.0 alpha:1.0];
    [self.emptyView addSubview:bottomLabel];
    //
    void (^onSendBlock)(NSDictionary* json) = ^(NSDictionary *json) {
        if([json[@"action"] isEqualToString:@"message.send"]) {
            // user
            long long userid = [json[@"options"][@"userlist"][0] longLongValue];
            ME_MessageModel* trap = [[ME_MessageModel alloc] init];
            trap.message = json[@"options"][@"message"];
            trap.date = [[NSDate date] toUtc];
            [self updateDialogWithUserId:userid msgModel:trap];
        } else if([json[@"action"] isEqualToString:@"groupchat.send"]) {
            // group
            [self updateLastList];
        } else {
            [self updateLastList];
        }
    };
    [ME_Network onActionOutgoing:@"message.send" setCallback:onSendBlock];
    [ME_Network onAction:@"groupchat.send" setCallback:onSendBlock];
    [ME_Network onAction:@"groupchat.create" setCallback:onSendBlock];
    [ME_Network onAction:@"groupchat.update" setCallback:onSendBlock];
    [ME_Network onAction:@"ongroupchatmessage" setCallback:onSendBlock];
    //
    void(^onMessageBlock)(NSDictionary* json) = ^(NSDictionary *json) {
        if(1000 == [json[@"status"] intValue]) {
            NSDictionary* result = [json objectForKey:@"result"];
            ME_MessageModel* msgModel = [[ME_MessageModel alloc] initWithDict:result];
            [self updateDialogWithUserId:msgModel.userid msgModel:msgModel];
        }
    };
    [ME_Network onAction:@"onmessage" setCallback:onMessageBlock];
    //
    [self updateLastList];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void) updateLastList
{
    BOOL clean = [AppDelegate shared].isNeededToClearFriends;
    [ME_Network send:@{@"action":@"dialog.lastlist", @"options":@{@"locale":@"ru"}} doIt:!clean andThen:^(NSDictionary *json) {
        id result = [json objectForKey:@"result"];
        self.lastlistArr = [NSMutableArray array];
        for(NSDictionary* item in result) {
            [self.lastlistArr addObject:[[ME_Dialog alloc] initWithDict:item]];
        }
        [self.tableView reloadData];
    }];
}

- (void) updateDialogWithUserId:(long long)inputUserid msgModel:(ME_MessageModel*)msgModel
{
    // Update rows
    ME_Dialog* dialog = nil;
    for(int i = 0; i < self.lastlistArr.count; i++) {
        ME_Dialog* tempDialog = self.lastlistArr[i];
        long long userid = [tempDialog getSourceUserid];
        if(inputUserid == userid) {
            dialog = self.lastlistArr[i];
            [dialog updateWithMsg:msgModel];
            NSIndexPath* from = [NSIndexPath indexPathForRow:i inSection:0];
            NSIndexPath* to = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView beginUpdates];
            [self.lastlistArr removeObjectAtIndex:i];
            [self.lastlistArr insertObject:dialog atIndex:0];
            [self.tableView moveRowAtIndexPath:from toIndexPath:to];
            [self.tableView endUpdates];
            [self.tableView reloadRowsAtIndexPaths:@[to] withRowAnimation:NO];
            break;
        }
    }
    // Insert new rows
    BOOL isMe = [[AppDelegate shared].phone longLongValue] == inputUserid;
    if(!dialog && !isMe) {
        // message from new user
        dialog = [[ME_Dialog alloc] initWithMsg:msgModel];
        [self.lastlistArr insertObject:dialog atIndex:0];
        NSIndexPath* path = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView insertRowsAtIndexPaths:@[path] withRowAnimation:YES];
        long long userid = msgModel.userid;
        // disconnect :(
        [[AppDelegate shared] getUserById:userid andThen:^(ME_Friend *meFriend) {
            if(meFriend) {
                for(int i = 0; i < self.lastlistArr.count; i++) {
                    ME_Dialog* tempDialog = self.lastlistArr[i];
                    if([tempDialog getSourceUserid] == meFriend.ID) {
                        dialog.owner = 0;
                        dialog.fromAvatar = meFriend.avatar;
                        dialog.fromName = meFriend.name;
                        dialog.fromSurname = meFriend.surname;
                        dialog.message = msgModel.message;
                        dialog.msgUnread = 1;
                        NSIndexPath* path = [NSIndexPath indexPathForRow:i inSection:0];
                        [self.tableView reloadRowsAtIndexPaths:@[path] withRowAnimation:NO];
                        break;
                    }
                }
            } else {
                NSLog(@"SOMETHING WRONG WITH USER");
            }
        }];
    }
}

- (void)onContactsUpdated:(NSArray *)contacts
{
    NSLog(@"ME_ChatsContorller onContactsUpdated");
    [self.tableView reloadData];
}

- (void) putEmptyView
{
//    self.tableView.hidden = YES;
//    self.searchDisplayController.searchBar.hidden = YES;
//    self.view = self.emptyView;
    [self.emptyView removeFromSuperview];
    [self.view addSubview:self.emptyView];
    self.tableView.scrollEnabled = NO;
}

- (void) putTableView
{
//    self.view = self.tempTable;
//    self.tableView = self.tempTable;
    [self.emptyView removeFromSuperview];
    self.tableView.scrollEnabled = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addAction:(id)sender
{
    NSLog(@"addAction");
    UIActionSheet* sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Отмена" destructiveButtonTitle:nil otherButtonTitles:@"Диалог", @"Группу", @"Рассылку", nil];
    [sheet showInView:self.view];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"clickedButtonAtIndex %ld", buttonIndex);
    if(0 == buttonIndex) {
        // go to contacts
        self.tabBarController.selectedIndex = 1;
    } else if(1 == buttonIndex) {
        //
//        id opts = @{@"name":@"test", @"description":@"", @"avatar":@"", @"userlist":@[]};
//        id msg = @{@"action":@"groupchat.create",@"options":opts};
//        [ME_Network send:msg andThen:^(NSDictionary *json) {
//            if(1000 == [json[@"status"] intValue]) {
//                ME_GroupModel* grpModel = [[ME_GroupModel alloc] initWithDict:json[@"result"]];
//                self.groupToSend = grpModel;
//                [self performSegueWithIdentifier:SEGUE_NEW_GROUP sender:nil];
//            }
//        }];
        [self performSegueWithIdentifier:SEGUE_NEW_GROUP sender:nil];
    } else if(2 == buttonIndex) {
        //
    }
}

- (void)iconClickedFromUserCell:(UITableViewCell *)cell
{
    NSLog(@"iconClickedFromUserCell");
}

- (void)iconClickedFromGroupCell:(UITableViewCell *)cell
{
    NSLog(@"iconClickedFromGroupCell");
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ME_Dialog* dialog = self.lastlistArr[indexPath.row];
    if(ME_DIALOG_TYPE_0_USER == dialog.type) {
        self.groupToSend = nil;
        self.dialogToSend = dialog;
        [self performSegueWithIdentifier:SEGUE_CHAT sender:self];
    } else if(ME_DIALOG_TYPE_1_GROUP == dialog.type) {
        id msg = @{@"action":@"groupchat.info", @"options":@{@"id":dialog.entryId}};
        [ME_Network send:msg andThen:^(NSDictionary *json) {
            NSDictionary* result = json[@"result"];
            self.dialogToSend = nil;
            self.groupToSend = [[ME_GroupModel alloc] initWithDict:result];
            [self performSegueWithIdentifier:SEGUE_CHAT sender:nil];
        }];
    } else if(ME_DIALOG_TYPE_2_MAILING == dialog.type) {
        //
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.lastlistArr.count;
}

- (void)userCell:(UITableViewCell *)cell clickedAvatarWithDialog:(ME_Dialog *)dialog
{
    NSLog(@"selectAvatar");
    long long userid = [dialog getSourceUserid];
    [ME_FriendProfileController showProfileUserid:userid fromCtrl:self];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView != self.tableView) {
        NSLog(@"WRONG TABLE VIEW");
        return [self.tableView dequeueReusableCellWithIdentifier:@"userCell" forIndexPath:indexPath];;
    }
    ME_Dialog* dialog = self.lastlistArr[indexPath.row];
    if(ME_DIALOG_TYPE_0_USER == dialog.type) {
        ME_UserCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userCell" forIndexPath:indexPath];
        
        ME_Dialog* dialog = self.lastlistArr[indexPath.row];
        [cell configureWithDialog:dialog];
        cell.delegate = self;
        
        return cell;
        //
    } else if(ME_DIALOG_TYPE_1_GROUP == dialog.type) {
        ME_GroupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupCell" forIndexPath:indexPath];
        
        [cell configureWithDialog:dialog];
        
        return cell;
        //
    } else if(ME_DIALOG_TYPE_2_MAILING == dialog.type) {
        // mailing рассылки
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupCell" forIndexPath:indexPath];
        
        // Configure the cell...
        
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}

//- (void)groupController:(UITableViewController *)grpCtrl createGroup:(ME_GroupModel *)grpModel
- (void)newGroupCtrl:(UIViewController *)ctrl groupUpdated:(ME_GroupModel *)grpModel
{
    NSLog(@"groupUpdated");
    self.dialogToSend = nil;
    self.groupToSend = grpModel;
    [self performSegueWithIdentifier:SEGUE_CHAT_NO_ANIM sender:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([SEGUE_CHAT isEqualToString:segue.identifier]) {
        UINavigationController* navCtrl = segue.destinationViewController;
        ME_ChatDetailController* ctrl = navCtrl.viewControllers[0];
        if(self.dialogToSend) {
            ctrl.userlist = [NSMutableArray arrayWithObject:@([self.dialogToSend getSourceUserid])];
//            ctrl.dlgModel = self.dialogToSend;
        } else if(self.groupToSend) {
            ctrl.grpModel = self.groupToSend;
        }
        //
    } else if([SEGUE_CHAT_NO_ANIM isEqualToString:segue.identifier]) {
        UINavigationController* navCtrl = segue.destinationViewController;
        ME_ChatDetailController* ctrl = navCtrl.viewControllers[0];
        ctrl.grpModel = self.groupToSend;

        //
    }else if([SEGUE_NEW_GROUP isEqualToString:segue.identifier]) {
        UINavigationController* navCtrl = segue.destinationViewController;
//        ME_ContactsForGroupController* ctrl = navCtrl.viewControllers[0];
//        ctrl.delegate = self;
        ME_NewGroupController* ctrl = navCtrl.viewControllers[0];
//        ctrl.grpModel = self.groupToSend;
        ctrl.delegate = self;
    }
}


@end
