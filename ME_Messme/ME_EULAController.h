//
//  EULAController.h
//  ME_Messme
//
//  Created by MessMe on 23/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ME_EULADelegate
- (void) callLoginSegueFrom:(id)sender;
@end

@interface ME_EULAController : UIViewController
@property (nonatomic, weak) id<ME_EULADelegate> delegate;
@end
