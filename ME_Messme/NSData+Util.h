//
//  NSData+Util.h
//  ME_Messme
//
//  Created by MessMe on 03/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Util)

- (void) saveTo:(NSString*)filename;
+ (NSData*) readFrom:(NSString*)filename;
+ (NSString*) docPathWith:(NSString*)filename;

@end
