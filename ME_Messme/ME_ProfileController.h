//
//  ME_ProfileController.h
//  ME_Messme
//
//  Created by MessMe on 18/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ME_ProfileController : UIViewController

@property BOOL isInitial;
@property BOOL isDisableLoginField;

@end
