//
//  AttachmentObj.h
//  ME_Messme
//
//  Created by MessMe on 11/01/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    ME_ATTACHMENT_0_FILE = 0,
    ME_ATTACHMENT_1_IMAGE = 1,
    ME_ATTACHMENT_2_VIDEO = 2,
    ME_ATTACHMENT_3_AUDIO = 3,
    ME_ATTACHMENT_4_VOICE = 4,
    ME_ATTACHMENT_10_CONTACT = 10,
    ME_ATTACHMENT_11_GEO = 11,
} ME_ATTACHMENT_TYPE;

@interface ME_Attachment : NSObject

@property (nonatomic, copy) NSString* ID;
@property ME_ATTACHMENT_TYPE type;
@property (nonatomic, copy) NSString* filename;
@property (nonatomic, copy) NSString* contenttype;
@property (nonatomic, copy) NSString* desc;
@property long long filesize;
@property (nonatomic, copy) NSString* title;
//
- (ME_Attachment*) initWithImage:(UIImage*)obj filename:(NSString*)filename;
- (ME_Attachment*) initWithAudioUrl:(NSURL*)url filename:(NSString*)filename;
- (ME_Attachment*) initWithGeoFilename:(NSString*)filename;
- (ME_Attachment*) initWithVideoUrl:(NSURL*)url filename:(NSString*)filename;
- (ME_Attachment*) initWithVCardFilename:(NSString*)filename;
//
- (NSData*) getNSData;
- (void) updateNSDataDoIt:(BOOL)doIt andThen:(void(^)(ME_Attachment* attach))callback;
- (void) updateNSDataAndThen:(void(^)(ME_Attachment* attach))callback;
- (ME_Attachment*) initWithDict:(NSDictionary*)dict;
- (double) getViewHeight;
- (NSString*) getContentType;
- (double) getGeoLat;
- (double) getGeoLng;

@end
