//
//  ME_AttachContactView.m
//  ME_Messme
//
//  Created by MessMe on 03/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_AttachContactView.h"
#import "ME_MessageModel.h"
#import "AppDelegate.h"
#import "CNContact+Utils.h"
#import "ME_ImageCache.h"
#import "UIImageView+Utils.h"
#import "NSString+Util.h"

@interface ME_AttachContactView ()

@property (nonatomic, strong) UIImageView* avatarImageView;
@property (nonatomic, strong) UILabel* nameLabel;
//@property (nonatomic, strong) UIButton* messageButton;
//@property (nonatomic, strong) UIButton* saveButton;
//@property (nonatomic, strong) UIButton* inviteButton;
@property (nonatomic, strong) UIButton* addButton;

@property (nonatomic, strong) CNContact* contact;
@property (nonatomic, strong) ME_Friend* friend;

@end

@implementation ME_AttachContactView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)layoutSubviews
{
    [super layoutSubviews];
    //
    if(!self.subviews.count) {
        self.avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        [self addSubview:self.avatarImageView];
        //
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, MESSSAGE_WIDTH - 25, 25)];
        [self addSubview:self.nameLabel];
        //
        CGRect f = CGRectMake(0, 35, MESSSAGE_WIDTH, 15);
        self.addButton = [[UIButton alloc] initWithFrame:f];
        [self.addButton setTitle:@"Добавить" forState:UIControlStateNormal];
        [self.addButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.addButton setFrame:f];
        [self.addButton addTarget:self action:@selector(addClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.addButton];
    }
    [self.attach updateNSDataDoIt:![[self.attach getNSData] length] andThen:^(ME_Attachment* attach) {
            //
            NSError* err = nil;
            NSArray* contactsArr = [CNContactVCardSerialization contactsWithData:[self.attach getNSData] error:&err];
            if(contactsArr.count) {
                self.contact = contactsArr[0];
                NSString* first = [self.contact getFirstNumber];
                self.friend = [[AppDelegate shared] getFriendByUserid:[first longLongValue]];
                if(self.friend) {
                    [ME_ImageCache getAvatarForFriend:self.friend andThen:^(UIImage *image) {
                        [self.avatarImageView setRoundedImage:image];
                    }];
                    NSString* name = [self.friend getPresentName];
                    self.nameLabel.text = name;
                } else {
                    self.avatarImageView.image = [UIImage imageNamed:@"tab_contacts"];
                    self.nameLabel.text = [[self.contact.givenName plus:@" "] plus:self.contact.familyName];
                }
            } else {
                NSLog(@"SOMETHING WRONG WITH VCARD");
            }
        }];
}

- (void) addClicked:(id)sender
{
    NSLog(@"ADD CONTACT CLICKED");
    [self.delegate attach:self addClickedContact:self.contact];
}

//- (void) messageClicked:(id)sender
//{
//    NSLog(@"MESSAGE CONTACT CLICKED");
//    [self.delegate attach:self messageClickedFriend:self.friend];
//}
//
//- (void) saveClicked:(id)sender
//{
//    NSLog(@"SAVE CONTACT CLICKED");
//    [self.delegate attach:self saveClickedFriend:self.friend];
//}
//
//- (void) inviteClicked:(id)sender
//{
//    NSLog(@"INVITE CONTACT CLICKED");
//    [self.delegate attach:self inviteClickedContact:self.contact];
//}

@end
