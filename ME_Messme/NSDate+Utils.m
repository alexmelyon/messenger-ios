//
//  NSDate+Utils.m
//  ME_Messme
//
//  Created by MessMe on 10/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "NSDate+Utils.h"
#import "NSString+Util.h"

@implementation NSString (DateUtils)

- (NSDate*) toDateFromUtc
{
    if([self isEqualToString:@"0001-01-01T00:00:00"]) {
        return nil;
    }
    NSString *input = self;// @"2013-05-08T19:03:53+00:00";
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"]; //iso 8601 format
    NSDate *output = [dateFormat dateFromString:input];
    if(!output) {
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'.'SZZZZZ"];
        output = [dateFormat dateFromString:input];
    }
    return output;
}

@end

@implementation NSDate (Util)

- (NSString*) toUtc
{
    NSDateFormatter* fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'.'SZZZZZ"];
    return [fmt stringFromDate:self];
}

- (NSString*) fromNow
{
//    return @"15 минут";
    NSDate* dateA = self;
    NSDate* dateB = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit
                                               fromDate:dateA
                                                 toDate:dateB
                                                options:0];
    NSInteger year = components.year;
    NSInteger month = components.month;
    NSInteger day = components.day;
    if(1 == day) {
        return @"вчера";
    } else if(year || month || day) {
        return [self getyyyyMMdd];
    }
    NSInteger hour = components.hour;
    if(hour) {
        return [NSString russianDeclensionForNum:hour one:@"час" two:@"часа" five:@"часов"];
    }
    NSInteger minute = components.minute;
    if(minute) {
        return [NSString russianDeclensionForNum:minute one:@"минута" two:@"минуты" five:@"минут"];
    } else {
        return @"Только что";
    }
}

- (NSString*) getyyyyMMdd
{
    NSDateFormatter* fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyy.MM.dd"];
    return [fmt stringFromDate:self];
}

- (NSString*) getHHmm
{
    NSDateFormatter* fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"HH:mm"];
    return [fmt stringFromDate:self];
}

@end