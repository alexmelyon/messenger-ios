//
//  ME_AudioPlayer.m
//  ME_Messme
//
//  Created by MessMe on 22/01/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_AudioPlayer.h"
#import <AVFoundation/AVFoundation.h>
#import "NSString+Util.h"

@interface ME_AudioPlayer () <AVAudioRecorderDelegate>

@property (nonatomic, strong) AVAudioRecorder* recorder;
@property (nonatomic, strong) AVAudioSession* audioSession;
@property (nonatomic, strong) AVPlayer *avPlayer;

@end

@implementation ME_AudioPlayer

- (void) playVideoFromUrl:(NSURL*)url
{
    NSLog(@"playVideoFromUrl:%@", url);
}

- (void) initAudio
{
    // Disable Stop/Play button when application launches
    //    [stopButton setEnabled:NO];
    //    [playButton setEnabled:NO];
    
    // Set the audio file
//    self.audioRecName = @"MyAudioMemo.m4a";
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               self.audioRecName,
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    self.recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    self.recorder.delegate = self;
    self.recorder.meteringEnabled = YES;
    [self.recorder prepareToRecord];
}

- (void) playFromCell:(ME_ChatCell*)cell attach:(ME_Attachment*)attach
{
    BOOL doPlay = ![self.audioCellId isEqualToString:cell.msgModel.ID] && ![self.audioAttachId isEqualToString:attach.ID];
    [self stopPlaying];
    if(doPlay) {
        NSLog(@"PLAY AUDIO FROM ATTACH https://files.messme.me:8102/message/%@", attach.ID);
        NSString* urlStr = [NSString stringWithFormat:@"https://files.messme.me:8102/message/%@", attach.ID];
//            NSURL* url = [NSURL URLWithString:@"https://files.messme.me:8102/message/2df3fd75-9577-44a2-bdfb-4e3650d407c3"]; // mp3
//        https://files.messme.me:8102/message/b01605d3-98e2-4dd9-a3bd-df6c889fc216 // m4a
//        urlStr = [[NSBundle mainBundle] pathForResource:@"MyAudioMemo" ofType:@"m4a"];
//        urlStr = [[NSBundle mainBundle] pathForResource:@"sound" ofType:@"mp3"];
        NSURL* url = [NSURL URLWithString:urlStr];
        //
        self.avPlayer = [[AVPlayer alloc] initWithURL:url];
//        self.avPlayer = [AVPlayer playerWithURL:url];
//        AVAsset* asset = [AVURLAsset URLAssetWithURL:url options:nil];
//        AVPlayerItem* item = [AVPlayerItem playerItemWithAsset:asset];
//        self.avPlayer = [AVPlayer playerWithPlayerItem:item];
        AVPlayerItem* item = [self.avPlayer currentItem];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:item];
        [self.avPlayer addObserver:self forKeyPath:@"status" options:0 context:nil];
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateProgress:) userInfo:nil repeats:YES];
        //
        self.audioCell = cell;
        self.audioCellId = cell.msgModel.ID;
        self.audioAttachId = attach.ID;
        [self.audioCell setPlaying:YES attach:attach progress:0];
    }
}

- (void) updateProgress:(id)sender
{
//    NSLog(@"updateProgress");
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (object == self.avPlayer && [keyPath isEqualToString:@"status"]) {
        if (self.avPlayer.status == AVPlayerStatusFailed) {
            NSLog(@"AVPlayer Failed");
        } else if (self.avPlayer.status == AVPlayerStatusReadyToPlay) {
            NSLog(@"AVPlayerStatusReadyToPlay");
            self.audioRecName = [[[NSUUID UUID] UUIDString] plus:@".m4a"];
            [self.avPlayer play];
            self.avPlayer.volume = 1.0;
            //
        } else if (self.avPlayer.status == AVPlayerItemStatusUnknown) {
            NSLog(@"AVPlayer Unknown");
        }
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    NSLog(@"END");
    [self stopPlaying];
    
}

//- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
//{
//    NSLog(@"audioPlayerDidFinishPlaying");
//    [self stopPlaying];
//}


- (void) pausePlaying
{
    [self.avPlayer pause];
    [self stopPlaying];
}

- (void) stopPlaying
{
    [self.avPlayer setRate:0.0];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[self.avPlayer currentItem]];

    @try {
        [self.avPlayer removeObserver:self forKeyPath:@"status"];
    }@catch(NSException* err) {
        NSLog(@"ERROR REGISTERING OBSERVER");
    }
    [self.audioCell setPlaying:NO attach:nil progress:0];
    self.audioCell = nil;
    self.audioCellId = nil;
    self.audioAttachId = nil;
}

- (NSURL*) getRecUrl
{
    return self.recorder.url;
}

- (void) startRecording
{
    NSLog(@"START RECORDING");
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    //
    self.audioSession = [AVAudioSession sharedInstance];
    NSError* err = nil;
    [self.audioSession setActive:YES error:&err];
    if(err) {
        NSLog(@"SOMETHING WRONG %@", err.description);
    }
    [self.recorder record];
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    NSLog(@"audioRecorderDidFinishRecording");
}

- (void) stopRecording
{
    NSLog(@"STOP RECORDING");
    [self.recorder stop];
    NSError* err;
    [self.audioSession setActive:NO error:&err];
    if(err) {
        NSLog(@"SOMETHING WRONG %@", err.description);
    }
}

- (void)updateCell
{
    NSLog(@"updateCell");
    // TODO
}

@end
