//
//  ME_ProfileController.m
//  ME_Messme
//
//  Created by MessMe on 18/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_ProfileController.h"
#import "ME_Network.h"
#import "AppDelegate.h"
#import "ME_CountryController.h"
#import "ME_CityController.h"
#import "NSString+Util.h"
#import "UIImage+Utils.h"

static NSString* SEGUE_COUNTRY = @"segueCountry";
static NSString* SEGUE_CITY = @"segueCity";

@interface ME_ProfileController () <UITextFieldDelegate,
                                    UITableViewDataSource,
                                    UITableViewDelegate,
                                    UIActionSheetDelegate,
                                    UIImagePickerControllerDelegate,
                                    UINavigationControllerDelegate,
                                    ME_CountryDelegate,
                                    ME_CityDelegate,
                                    UIGestureRecognizerDelegate>

- (IBAction)nextAction:(id)sender;
@property UITextField* lastField;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UITextField* usernameField;
@property (nonatomic, strong) UITextField* surnameField;
@property (nonatomic, strong) UITextField* loginField;
@property (nonatomic, strong) UIImageView* checkLoginImageView;
//@property (nonatomic, copy) NSString* username;
//@property (nonatomic, copy) NSString* surname;
- (IBAction)onLoginChanged:(id)sender;
//@property (nonatomic, copy) NSString* login;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
//@property (nonatomic, strong) UIImage* avatarImage;
@property (nonatomic, strong) UIImageView* avatarImageView;
@property (nonatomic, strong) UIActionSheet* avatarActionSheet;
@property (nonatomic, strong) UIActionSheet* sexActionSheet;
@property (strong, nonatomic)  UIDatePicker *datePicker;
- (void) datePickerAction:(id)sender;
@property BOOL hasDatePicker;
- (void) removeDateAction:(id)sender;

@end

@implementation ME_ProfileController
{
    UIToolbar* _numberToolbar;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //
//    if(self.isInitial) {
//        self.tabBarController.tabBar.hidden = YES;
//    } else {
//        self.tabBarController.tabBar.hidden = NO;
//    }
    //
    _numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    _numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    _numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Отмена"
                                                            style:UIBarButtonItemStylePlain
                                                           target:self
                                                           action:@selector(cancelNumberPad:)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"ОК" style:UIBarButtonItemStyleDone
                                                           target:self
                                                           action:@selector(doneWithNumberPad:)]];
    [_numberToolbar sizeToFit];
    //
//    self.login = @"alexmelyon";
    [self checkLoginAction:nil];
    //
    self.datePicker = [[UIDatePicker alloc] init];
    //
    self.navigationItem.hidesBackButton = self.isInitial;
}
-(void)cancelNumberPad:(id)sender
{
    [self.lastField resignFirstResponder];
}

-(void)doneWithNumberPad:(id)sender
{
    [self checkLoginAction:nil];
    if(self.lastField == self.usernameField) {
        [AppDelegate shared].userProfile.username = self.lastField.text;
    } else if(self.lastField == self.surnameField) {
        [AppDelegate shared].userProfile.surname = self.lastField.text;
    } else if(self.lastField == self.loginField) {
        [AppDelegate shared].userProfile.login = self.lastField.text;
    }
    [self.lastField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)nextAction:(id)sender
{
    if(![AppDelegate shared].userProfile.username) {
        [AppDelegate shared].userProfile.username = @"";
    }
    if(![AppDelegate shared].userProfile.surname) {
        [AppDelegate shared].userProfile.surname = @"";
    }
    NSString* birthdate = [[AppDelegate shared].userProfile getDateString];
    if(![AppDelegate shared].userProfile.birthdate) {
        birthdate = @"";
    }
    if(![AppDelegate shared].userProfile.sex) {
        [AppDelegate shared].userProfile.sex = @"";
    }
    NSString* avatarBase64 = [[AppDelegate shared].userProfile.avatarImage toBase64String];
    if(!avatarBase64) {
        avatarBase64 = @"";
    }
    NSDictionary* options = @{@"login":[AppDelegate shared].userProfile.login,
                              @"name":[AppDelegate shared].userProfile.username,
                              @"surname":[AppDelegate shared].userProfile.surname,
                              @"birthdate":birthdate,
                              @"sex":[AppDelegate shared].userProfile.sex,
                              @"country":@([[[AppDelegate shared].userProfile.countryObj objectForKey:@"countryid"] longLongValue]),
                              @"city":@([[[AppDelegate shared].userProfile.cityObj objectForKey:@"cityid"] longLongValue]),
                              @"avatar":avatarBase64};
    if(self.isInitial) {
        [ME_Network send:@{@"action":@"user.create", @"options":options} andThen:^(NSDictionary *json) {
            if(1000 == [[json objectForKey:@"status"] intValue]) {
                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                id ctrl = [storyboard instantiateViewControllerWithIdentifier:@"tabsController"];
                [self presentViewController:ctrl animated:YES completion:nil];
            } else if(1014 == [[json objectForKey:@"status"] intValue]) {
                [[[UIAlertView alloc] initWithTitle:@"" message:@"Пользователь с таким логином уже существует" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            } else {
                NSLog(@"ERROR");
            }
        }];
    } else {
        NSLog(@"nextAction");
        // TODO
    }
}

- (IBAction)onLoginChanged:(UITextField*)sender {
    [AppDelegate shared].userProfile.login = sender.text;
    [self checkLoginAction:sender];
}

/** only login now */
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.lastField = textField;
    if(self.loginField == textField) {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"%@", string);
    NSString* result = [textField.text stringByReplacingCharactersInRange:range withString:string];
    result = [result stringByReplacingOccurrencesOfString:@" " withString:@""];
    if(textField == self.loginField) {
        [AppDelegate shared].userProfile.login = result;
        NSError* error;
        if(error) {
            NSLog(@"ERROR %@", error);
        }
        if([string isEqualToString:@" "]) {
            return NO;
        }
        if([[string trim] isEqualToString:@""]) {
            return YES;
        }
        NSRegularExpression* regexp = [NSRegularExpression regularExpressionWithPattern:@"[a-z0-9_.-]" options:0 error:&error];
        return [regexp firstMatchInString:string options:0 range:NSMakeRange(0, string.length)];
    } else if(textField == self.usernameField) {
        [AppDelegate shared].userProfile.username = result;
    } else if(textField == self.surnameField) {
        [AppDelegate shared].userProfile.surname = result;
    }
    return result.length <= 255;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    //
    if(textField == self.usernameField) {
        [AppDelegate shared].userProfile.username = textField.text;
    } else if(textField == self.surnameField) {
        [AppDelegate shared].userProfile.surname = textField.text;
    } else if(textField == self.loginField) {
        [AppDelegate shared].userProfile.login = textField.text;
    }
    [self checkLoginAction:textField];
    //
    return NO;
}

- (void)checkLoginAction:(id)sender
{
    NSLog(@"checkLoginAction");
    if([AppDelegate shared].userProfile.login != nil && [AppDelegate shared].userProfile.login.length >= 5) {
        self.nextButton.enabled = YES;
        self.doneButton.enabled = YES;
        self.nextButton.alpha = 1.0;
        [ME_Network send:@{@"action":@"user.checklogin", @"options":@{@"login":[AppDelegate shared].userProfile.login}} andThen:^(NSDictionary *json) {
            if(self.isDisableLoginField || 1000 == [[json objectForKey:@"status"] intValue]) {
                self.checkLoginImageView.image = [UIImage imageNamed:@"tick_blue"];
                self.nextButton.enabled = YES;
                self.doneButton.enabled = YES;
                self.nextButton.alpha = 1.0;
            } else {
                self.checkLoginImageView.image = [UIImage imageNamed:@"no-sm"];
                self.nextButton.enabled = NO;
                self.doneButton.enabled = NO;
                self.nextButton.alpha = 0.5;
            }
        }];
    } else {
        self.nextButton.enabled = NO;
        self.doneButton.enabled = NO;
        self.nextButton.alpha = 0.5;
        self.checkLoginImageView.image = [UIImage imageNamed:@"tick_red"];
    }
}

- (void)chooseAvatarAction:(id)sender
{
    NSLog(@"chooseAvatarAction");
    [self.lastField resignFirstResponder];
    if([AppDelegate shared].userProfile.avatarImage) {
        self.avatarActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Отмена"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Фото на iPhone", @"Сделать снимок", @"Удалить", nil];
        self.avatarActionSheet.destructiveButtonIndex = 2;
    } else {
        self.avatarActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Отмена"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Фото на iPhone", @"Сделать снимок", nil];
    }
    [self.avatarActionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(self.avatarActionSheet == actionSheet) {
        switch(buttonIndex) {
            case 0: {
                NSLog(@"choose from iPhone");
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
                imagePickerController.delegate = self;
                //    imagePickerController.allowsEditing = YES;
                imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentModalViewController:imagePickerController animated:YES];
            } break;
            case 1: {
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                    NSLog(@"choose from camera");
                    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
                    imagePickerController.delegate = self;
                    //    imagePickerController.allowsEditing = YES;
                    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                    [self presentModalViewController:imagePickerController animated:YES];
                } else {
                    NSLog(@"CAMERA IS NOT AVAILABLE");
                }
            } break;
            case 2: {
                [AppDelegate shared].userProfile.avatarImage = nil;
//                self.avat
                [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:NO];
            }
            default: {
                NSLog(@"actionSheet default");
            }
        }
    } else if(self.sexActionSheet == actionSheet) {
        switch (buttonIndex) {
            case 0: {
                [AppDelegate shared].userProfile.sex = @"m";
            } break;
            case 1: {
                [AppDelegate shared].userProfile.sex = @"f";
            } break;
            case 2: {
                [AppDelegate shared].userProfile.sex = @"";
            } break;
            default:
                break;
        }
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:NO];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage* img = [info objectForKey:UIImagePickerControllerOriginalImage];
    if(img) {
        UIImage* newImage = [img cropCenterToSize:CGSizeMake(77, 77)];
        [AppDelegate shared].userProfile.avatarImage = newImage;
        self.avatarImageView.image = newImage;
        self.avatarImageView.layer.cornerRadius = 77.0/2;
        self.avatarImageView.clipsToBounds = YES;
        [self.tableView reloadData];
    }
}

- (IBAction)datePickerAction:(UIDatePicker*)sender {
    [AppDelegate shared].userProfile.birthdate = sender.date;
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:1]] withRowAnimation:NO];
}

- (void) removeDateAction:(id)sender
{
    [AppDelegate shared].userProfile.birthdate = nil;
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:1]] withRowAnimation:NO];
}

#pragma mark UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}
#pragma mark UITableViewDataSource

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    if(1 == indexPath.section && 1 == indexPath.row) {
        [self removeDateAction:nil];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(1 == indexPath.section && 0 == indexPath.row) {
        self.sexActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"Отмена"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"Мужской", @"Женский", @"Не выбран", nil];
        [self.sexActionSheet showInView:self.view];
    } else if(1 == indexPath.section && 1 == indexPath.row) {
        [tableView beginUpdates];
        if(self.hasDatePicker) {
            [tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
        } else {
            if(![AppDelegate shared].userProfile.birthdate) {
                [AppDelegate shared].userProfile.birthdate = [NSDate date];
            }
            self.datePicker.date = [AppDelegate shared].userProfile.birthdate;
            [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:1]] withRowAnimation:NO];
            [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
        }
        self.hasDatePicker = !self.hasDatePicker;
        [tableView endUpdates];
        if(self.hasDatePicker) {
            [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(0 == section) {
        return 1;
    } else if(1 == section) {
        if(self.hasDatePicker) {
            return 3;
        }
        return 2;
    } else if(2 == section) {
        return 2;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell;
    if(0 == indexPath.section && 0 == indexPath.row) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"firstCell"];
        self.usernameField = (UITextField*)[cell viewWithTag:101];
        self.surnameField = (UITextField*)[cell viewWithTag:102];
        self.loginField = (UITextField*)[cell viewWithTag:103];
        self.avatarImageView = (UIImageView*)[cell viewWithTag:104];
        self.checkLoginImageView = (UIImageView*)[cell viewWithTag:106];
        //
        self.usernameField.inputAccessoryView = _numberToolbar;
        self.surnameField.inputAccessoryView = _numberToolbar;
        self.loginField.inputAccessoryView = _numberToolbar;
        self.loginField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.avatarImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseAvatarAction:)];
        self.avatarImageView.gestureRecognizers = nil;
        [self.avatarImageView addGestureRecognizer:tap];
        //
        self.usernameField.text = [AppDelegate shared].userProfile.username;
        self.surnameField.text = [AppDelegate shared].userProfile.surname;
        self.loginField.text = [AppDelegate shared].userProfile.login;
        if(self.isDisableLoginField) {
            self.loginField.enabled = NO;
            self.checkLoginImageView.hidden = YES;
            //
        } else {
//            self.avatarImageView.image = [UIImage imageNamed:@"emptyPhoto"];
        }
        self.avatarImageView.image = [[AppDelegate shared].userProfile.avatarImage resizeTo:CGSizeMake(77, 77)];
        self.avatarImageView.layer.cornerRadius = 77.0/2;
        self.avatarImageView.clipsToBounds = YES;
        if(![AppDelegate shared].userProfile.avatarImage) {
            self.avatarImageView.image = [UIImage imageNamed:@"emptyPhoto"];
        }
        self.avatarImageView.layer.cornerRadius = 77.0/2;
        self.avatarImageView.clipsToBounds = YES;
        //
    } else if(1 == indexPath.section && 0 == indexPath.row) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"sexCell"];
        cell.textLabel.text = @"Пол";
        cell.detailTextLabel.text = [[AppDelegate shared].userProfile getSexString];
        //
    } else if(1 == indexPath.section && 1 == indexPath.row) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"dateCell"];
        cell.textLabel.text = @"Дата рождения";
        cell.detailTextLabel.text = [[AppDelegate shared].userProfile getDateString];
        //
        if([AppDelegate shared].userProfile.birthdate) {
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cross_blue_circle"]];
            [cell.accessoryView setFrame:CGRectMake(0, 0, 8, 24)];
            cell.accessoryView.contentMode = UIViewContentModeCenter;
            UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeDateAction:)];
            [tap setCancelsTouchesInView:YES];
            [tap setNumberOfTouchesRequired:1];
            tap.delegate = self;
            cell.accessoryView.userInteractionEnabled = YES;
            [cell.accessoryView addGestureRecognizer:tap];
        } else {
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_right_gray"]];
            [cell.accessoryView setFrame:CGRectMake(0, 0, 8, 24)];
            cell.accessoryView.contentMode = UIViewContentModeCenter;
            cell.accessoryView.gestureRecognizers = nil;
        }
        //
    } else if(1 == indexPath.section && 2 == indexPath.row) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"pickerCell"];
        [(UIDatePicker*)[cell viewWithTag:101] removeFromSuperview]; // because view bugs
        self.datePicker.frame = CGRectMake(0, 0, 320, 216);
        self.datePicker.maximumDate = [NSDate date];
        [cell.contentView addSubview:self.datePicker];
        //
        NSDate* date = [AppDelegate shared].userProfile.birthdate;
        if(!date) {
            date = [NSDate date];
        }
        self.datePicker.date = date;
        self.datePicker.datePickerMode = UIDatePickerModeDate;
        self.datePicker.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
        [self.datePicker addTarget:self action:@selector(datePickerAction:) forControlEvents:UIControlEventValueChanged];
        //
//    } else if(2 == indexPath.section && 0 == indexPath.row) {
//        cell = [tableView dequeueReusableCellWithIdentifier:@"getPositionCell"];
//        //
    } else if(2 == indexPath.section && 0 == indexPath.row) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"countryCell"];
        NSString* imgName = [[[AppDelegate shared].userProfile.countryObj objectForKey:@"iso"] stringValue];
        cell.imageView.image = [[UIImage imageNamed:imgName] resizeTo:CGSizeMake(34, 34)];
        cell.imageView.layer.cornerRadius = 17;
        cell.textLabel.text = [[AppDelegate shared].userProfile.countryObj objectForKey:@"countryname"];
        // jcd
    } else if(2 == indexPath.section && 1 == indexPath.row) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"cityCell"];
        NSString* cityname = [[AppDelegate shared].userProfile.cityObj objectForKey:@"cityname"];
        if(cityname) {
            cell.textLabel.text = cityname;
        } else {
            cell.textLabel.text = @"Выберите город";
        }
        
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(0 == indexPath.section) {
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"firstCell"];
        return  cell.contentView.frame.size.height;
    } else if(1 == indexPath.section && 2 == indexPath.row) {
        return 216;
    }
    return 44;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(2 == section) {
        return @"РАСПОЛОЖЕНИЕ";
    }
    return @" ";
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(0 == section) {
        return 0;
    }
    return UITableViewAutomaticDimension;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"SEGUE %@", segue.identifier);
    
    if([SEGUE_COUNTRY isEqualToString:segue.identifier]) {
        ME_CountryController* ctrl = segue.destinationViewController;
        ctrl.delegate = self;
    } else if([SEGUE_CITY isEqualToString:segue.identifier]) {
        ME_CityController* ctrl = segue.destinationViewController;
        ctrl.countryid = [[[AppDelegate shared].userProfile.countryObj objectForKey:@"countryid"] intValue];
        ctrl.delegate = self;
    }
}

- (void) selectCountry:(NSDictionary*)country from:(id)sender
{
    NSLog(@"selectCountry");
    [AppDelegate shared].userProfile.countryObj = country;
    [AppDelegate shared].userProfile.cityObj = nil;
    //
    [self.navigationController popViewControllerAnimated:YES];
    NSArray* toUpdate = @[[NSIndexPath indexPathForRow:0 inSection:2], [NSIndexPath indexPathForRow:1 inSection:2]];
    [self.tableView reloadRowsAtIndexPaths:toUpdate withRowAnimation:NO];
}

- (void)selectCity:(NSDictionary *)city from:(id)sender
{
    NSLog(@"selectCity");
    [AppDelegate shared].userProfile.cityObj = city;
    //
    [self.navigationController popViewControllerAnimated:YES];
    NSArray* toUpdate = @[[NSIndexPath indexPathForRow:1 inSection:2]];
    [self.tableView reloadRowsAtIndexPaths:toUpdate withRowAnimation:NO];
}
@end
