//
//  ME_GroupModel.m
//  ME_Messme
//
//  Created by MessMe on 19/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_GroupModel.h"

@implementation ME_GroupModel

- (ME_GroupModel*) initWithDict:(NSDictionary*)dict
{
    self = [super init];
    [self updateWithDict:dict];
    return self;
}

- (void) updateWithDict:(NSDictionary*)dict
{
    self.adminid = [dict[@"adminid"] longLongValue];
    self.avatar = dict[@"avatar"];
    self.dateStr = dict[@"date"];
    self.desc = dict[@"description"];
    self.ID = dict[@"id"];
    self.isSecret = [dict[@"issecret"] intValue];
    self.name = dict[@"name"];
    self.password = dict[@"password"];
    self.userid = [dict[@"userid"] longLongValue];
    //    self.userlogin = dict[@"userlogin"];
    //    self.adminlogin = dict[@"adminlogin"];
    self.userlist = [NSMutableArray arrayWithArray:dict[@"userlist"]];
}

@end
