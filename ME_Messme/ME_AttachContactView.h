//
//  ME_AttachContactView.h
//  ME_Messme
//
//  Created by MessMe on 03/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ME_Attachment.h"
#import "ME_Friend.h"
@import Contacts;

@class ME_AttachContactView;

@protocol ME_AttachContactViewDelegate

//- (void) attach:(ME_AttachContactView*)attach messageClickedFriend:(ME_Friend*)buddy;
//- (void) attach:(ME_AttachContactView*)attach saveClickedFriend:(ME_Friend*)buddy;
//- (void) attach:(ME_AttachContactView*)attach inviteClickedContact:(CNContact*)contact;
- (void) attach:(ME_AttachContactView*)attach addClickedContact:(CNContact*)contact;

@end

@interface ME_AttachContactView : UIView

@property (nonatomic, strong) ME_Attachment* attach;
@property (nonatomic, weak) id<ME_AttachContactViewDelegate> delegate;

@end
