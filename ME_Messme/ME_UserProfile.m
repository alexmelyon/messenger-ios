//
//  ME_UserProfile.m
//  ME_Messme
//
//  Created by MessMe on 02/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_UserProfile.h"
#import "NSDate+Utils.h"

@implementation ME_UserProfile

- (ME_UserProfile*) init
{
    self = [super init];
    self.countryObj = @{
                        @"continentid":@(4),
                        @"continentname":@"Европа",
                        @"countryid":@(2017370),
                        @"countryname":@"Россия",
                        @"iso":@(643),
                        @"phonecode":@(7)
                        };
    return self;
}

- (NSString*) getSexString
{
    if([self.sex isEqualToString:@"m"]) {
        return @"Мужской";
    } else if([self.sex isEqualToString:@"f"]) {
        return  @"Женский";
    }
    return @"Не указан";
}

- (NSString*) getDateString
{
    if(self.birthdate) {
        NSDateFormatter* fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"dd.MM.yyyy";
        return [fmt stringFromDate:self.birthdate];
    }
    return @"Не указан";
}

- (ME_UserProfile*) initWithDictionary:(NSDictionary*)dict andImage:(UIImage*)image
{
    self = [[ME_UserProfile alloc] init];
    self.username = [dict objectForKey:@"name"];
    self.surname = [dict objectForKey:@"surname"];
    self.login = [dict objectForKey:@"login"];
    self.sex = [dict objectForKey:@"sex"];
    self.birthdate = [[dict objectForKey:@"birthdate"] toDateFromUtc];
    //
    if(![[dict objectForKey:@"avatar"] isEqualToString:@""]) {
        self.avatarImage = image;
    }
    return self;
}

@end
