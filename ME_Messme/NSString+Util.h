//
//  NSStrimg+Trim.h
//  ME_Messme
//
//  Created by MessMe on 04/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Util)

- (NSString*) trim;
- (NSString*) plus:(NSString*)second;
- (NSString*) plus:(NSString*)second :(NSString*)third;
- (NSString*) plusLongLong:(long long)second;
- (NSString*) plusParams:(id)second, ...;
- (double) getHeightForWidth:(double)width;
- (double) getHeightForWidth:(double)width andFontSize:(double)fontSize;
+ (NSString*) russianDeclensionForNum:(NSInteger)num one:(NSString*)one two:(NSString*)two five:(NSString*)five;

@end
