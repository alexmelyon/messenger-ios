//
//  StartController.m
//  ME_Messme
//
//  Created by MessMe on 19/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_StartController.h"
#import "AppDelegate.h"
#import "ME_EULAController.h"
#import "ME_RecoveryController.h"

static NSString* SEGUE_NO_ANIMATION = @"segueNoAnimation";
static NSString* SEGUE_LOGIN = @"segueLogin";
static NSString* SEGUE_EULA = @"segueEula";
static NSString* SEGUE_RECOVERY = @"segueRecovery";

@interface ME_StartController () <ME_EULADelegate>
@end

@implementation ME_StartController
{
    BOOL _gotoLogin;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //
    if([AppDelegate shared].token && ![AppDelegate shared].isNewDeviceNeedRelogin) {
        self.view.hidden = YES;
    } else {
        self.view.hidden = NO;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //
    if([AppDelegate shared].token && ![AppDelegate shared].isNewDeviceNeedRelogin) {
        if([AppDelegate shared].isGoingToTabs) {
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            id ctrl = [storyboard instantiateViewControllerWithIdentifier:@"tabsController"];
            [self presentViewController:ctrl animated:YES completion:nil];
            
        } else {
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ME_RecoveryController* ctrl = [storyboard instantiateViewControllerWithIdentifier:@"recoveryController"];
            ctrl.isInitial = YES;
            UINavigationController* navCtrl = [[UINavigationController alloc] initWithRootViewController:ctrl];
            [self presentViewController:navCtrl animated:YES completion:nil];
        }
    }
    
    if(_gotoLogin) {
        [self performSegueWithIdentifier:SEGUE_LOGIN sender:self];
        _gotoLogin = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([SEGUE_EULA isEqualToString: segue.identifier]) {
        ME_EULAController* ctrl = segue.destinationViewController;
        ctrl.delegate = self;
    }
}

#pragma mark delegates

- (void)callLoginSegueFrom:(id)sender
{
//    [self performSegueWithIdentifier:SEGUE_LOGIN sender:sender];
    _gotoLogin = YES;
}

@end
