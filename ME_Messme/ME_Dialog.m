//
//  ME_DIalog.m
//  ME_Messme
//
//  Created by MessMe on 18/01/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_Dialog.h"

@implementation ME_Dialog

- (ME_Dialog*) initWithMsg:(ME_MessageModel*)msgModel
{
    self = [super init];
    //
    //    self.fromAvatar =
    //    self.fromLogin
//    self.fromName
//    self.fromSurname
    self.fromUserid = msgModel.userid;
    self.toUserid = msgModel.userid;
    self.fromStatus = msgModel.status;
    self.toStatus = msgModel.status;
//    self.fromUserid = msgModel.userid;
    self.msgUnread = 1;
    //
    return self;
}

- (ME_Dialog*) initWithDict:(NSDictionary*)dict
{
    self = [super init];
    self.cntOnline = [dict[@"cntonline"] intValue]; // online users for group chats
    self.cntTotal = [dict[@"cnttotal"] intValue];// total users for group chats
    self.dateStr = dict[@"date"];
    self.entryId = dict[@"entryid"];
    self.fromAvatar = dict[@"fromavatar"];
    self.fromLogin = dict[@"fromlogin"];
    self.fromName = dict[@"fromname"];
    self.fromSurname = dict[@"fromsurname"];
    self.fromStatus = [dict[@"fromstatus"] intValue];
    self.fromUserid = [dict[@"fromuserid"] longLongValue];
    self.message = dict[@"message"];
    self.msgTotal = [dict[@"msgtotal"] intValue];
    self.msgType = [dict[@"msgtype"] intValue];
    self.msgUnread = [dict[@"msgunread"] intValue];
    self.owner = [dict[@"owner"] intValue];
    self.status = [dict[@"status"] intValue];
    self.toAvatar = dict[@"toavatar"];
    self.toLogin = dict[@"tologin"];
    self.toName = dict[@"toname"];
    self.toStatus = [dict[@"tostatus"] intValue];
    self.toSurname = dict[@"tosurname"];
    self.toUserid = [dict[@"touserid"] longLongValue];
    self.type = [dict[@"type"] intValue];
    return self;
}

- (void) updateWithMsg:(ME_MessageModel*)msgModel
{
    self.dateStr = msgModel.date;
    self.message = msgModel.message;
    self.msgUnread += 1;

}

- (long long) getSourceUserid
{
    return self.owner ? self.toUserid : self.fromUserid;
}
- (NSString*) getSourceName
{
    return self.owner ? self.toName : self.fromName;
}
- (NSString*) getSourceSurname
{
    return self.owner ? self.toSurname : self.fromSurname;
}
- (NSString*) getSourceLogin
{
    return self.owner ? self.toLogin : self.fromLogin;
}
- (NSString*) getSourceAvatar
{
    return self.owner ? self.toAvatar : self.fromAvatar;
}
- (int) getSourceStatus
{
    return self.owner ? self.toStatus : self.fromStatus;
}

@end
