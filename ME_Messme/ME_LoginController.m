//
//  ME_LoginController.m
//  ME_Messme
//
//  Created by MessMe on 16/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_LoginController.h"
#import "ME_Network.h"
#import "AppDelegate.h"
#import "ME_CountryController.h"
#import <UIKit/UIKit.h>

static NSString* SEGUE_CODE = @"segueCode";
static NSString* SEGUE_COUNTRY = @"segueCountry";
static NSString* SEGUE_PROMO = @"seguePromo";

@interface ME_LoginController () <UITextFieldDelegate, ME_CountryDelegate, UIAlertViewDelegate>

- (IBAction) nextAction:(id)sender;
- (IBAction)editingDidBegin:(id)sender;
@property UITextField* lastField;
@property NSString* lastText;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property UITextField* phoneField;
@property UITextField* promoField;
@property UILabel* prefixLabel;
@property NSString* phone;
@property NSString* promo;

@end

@implementation ME_LoginController
{
    UIToolbar* _numberToolbar;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    _numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    _numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Отмена"
                                                            style:UIBarButtonItemStylePlain
                                                           target:self
                                                           action:@selector(cancelNumberPad:)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:nil
                                                                         action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"ОК" style:UIBarButtonItemStyleDone
                                                           target:self
                                                           action:@selector(doneWithNumberPad:)]];
    [_numberToolbar sizeToFit];
    //
    [AppDelegate shared].country = @{
                     @"continentid":@(4),
                     @"continentname":@"Европа",
                     @"countryid":@(2017370),
                     @"countryname":@"Россия",
                     @"iso":@(643),
                     @"phonecode":@(7)
                     };
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void) keyboardWillHide:(NSNotification*)sender
{
    [self resetTableViewPosition];
}

-(void)cancelNumberPad:(id)sender{
    self.lastField.text = self.lastText;
    [self.lastField resignFirstResponder];
//    [self resetTableViewPosition];
}

-(void)doneWithNumberPad:(id)sender{
    if(self.lastField == self.phoneField) {
        self.phone = self.lastField.text;
    } else if(self.lastField == self.promoField) {
        self.promo = self.lastField.text;
    }
    [self.lastField resignFirstResponder];
//    [self resetTableViewPosition];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if(textField == self.phoneField) {
        self.phone = textField.text;
    } else if(textField == self.promoField) {
        self.promo = textField.text;
    }
    return NO;
}

- (IBAction)editingDidBegin:(UITextField*)sender {
    [sender becomeFirstResponder];
    self.lastField = sender;
    self.lastText = self.lastField.text;
    if(self.promoField == sender) {
        [UIView animateWithDuration:0.5 animations:^{
            CGRect frame = self.tableView.frame;
            frame.origin.y = -50;
            self.tableView.frame = frame;
        }];
    }
}
- (void) resetTableViewPosition
{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.tableView.frame;
        frame.origin.y = 64;
        self.tableView.frame = frame;
    }];
}
//
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(0 == section) {
        return 2;
    } else if(1 == section) {
        return 1;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(0 == indexPath.section && 0 == indexPath.row) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"countryCell" forIndexPath:indexPath];
        cell.imageView.contentMode = UIViewContentModeCenter;
        NSString* iso = [[[AppDelegate shared].country objectForKey:@"iso"] stringValue];
        cell.imageView.image = [UIImage imageNamed:iso];
        //
        CGSize itemSize = CGSizeMake(34, 34);
        UIGraphicsBeginImageContextWithOptions(itemSize, NO, UIScreen.mainScreen.scale);
        CGRect imageRect = CGRectMake(0, 0, itemSize.width, itemSize.height);
        [cell.imageView.image drawInRect:imageRect];
        cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        cell.imageView.layer.cornerRadius = 17;
        //
        cell.textLabel.text = [[AppDelegate shared].country objectForKey:@"countryname"];
        return cell;
    } else if(0 == indexPath.section && 1 == indexPath.row) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"phoneCell" forIndexPath:indexPath];
        self.prefixLabel = (UILabel*)[cell viewWithTag:101];
        NSString* phonecode = [[[AppDelegate shared].country objectForKey:@"phonecode"] stringValue];
        phonecode = [NSString stringWithFormat:@"+%@", phonecode];
        self.prefixLabel.text = phonecode;
        self.phoneField = (UITextField*)[cell viewWithTag:102];
        self.phoneField.inputAccessoryView = _numberToolbar;
//        self.phoneField.text = @"9824772501";
        self.phoneField.text = self.phone;
        return cell;
    } else if(1 == indexPath.section && 0 == indexPath.row) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"promoCell" forIndexPath:indexPath];
        self.promoField = (UITextField*)[cell viewWithTag:101];
        self.promoField.inputAccessoryView = _numberToolbar;
        self.promoField.text = self.promo;
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(0 == section) {
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = @"Подтвердите код страны и\nваш номер телефона";
        label.font = [UIFont fontWithName:@"Helvetica" size:14];
        label.numberOfLines = 0;
        label.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0];
        return label;
    } else if(1 == section) {
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = @"Код доступа будет выслан вам\nв SMS сообщении";
        label.font = [UIFont fontWithName:@"Helvetica" size:14];
        label.numberOfLines = 0;
        label.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0];
        return label;
    }
    return nil;
}

- (void) tableView:(UITableView*)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath*)indexPath
{
    if(1 == indexPath.section && 0 == indexPath.row) {
        [self performSegueWithIdentifier:SEGUE_PROMO sender:self];
    }
}

- (IBAction) nextAction:(id)sender
{
    if(![ME_Network isAvailable]) {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Пожалуйста, проверьте интернет соединение" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    NSString* phonecode = [[AppDelegate shared].country objectForKey:@"phonecode"];
    NSString* full = [NSString stringWithFormat:@"%@%@", phonecode, self.phone];
    if(full.length != 11) {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Пожалуйста, проверьте введенный номер мобильного телефона" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    [AppDelegate shared].phonecode = phonecode;
    [AppDelegate shared].phone = full;
    if(!self.promo) {
        self.promo = @"";
    }
    NSString* message = [NSString stringWithFormat:@"Это ваш правильный номер?\n+%@\nSMS с вашим кодом доступа будет отправлено на этот номер", full];
    [[[UIAlertView alloc] initWithTitle:@"Проверка номера телефона" message:message delegate:self cancelButtonTitle:@"Изменить" otherButtonTitles:@"OK", nil] show];
    //
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"clickedButtonAtIndex %ld", buttonIndex);
    if(1 == buttonIndex) {
        
        NSString* path = [NSString stringWithFormat: @"/User?id=%@", [AppDelegate shared].phone];
            [ME_Network connectTo:path andThen:^(SRWebSocket* webSocket) {
                [ME_Network send:@{@"action":@"user.checkuser",@"options":@{@"userid":[AppDelegate shared].phone}} andThen:^(NSDictionary *json) {
                    if(1016 == [[json objectForKey:@"status"] intValue]) {
                        [AppDelegate shared].isUserAlreadyExists = YES;
                    }
                    [ME_Network send:@{@"action":@"user.register", @"options":@{@"invite":self.promo}} andThen:^(NSDictionary *json) {
                        [self performSegueWithIdentifier:SEGUE_CODE sender:nil];
                    }];
                }];
            }];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([SEGUE_COUNTRY isEqualToString:segue.identifier]) {
        ME_CountryController *ctrl = segue.destinationViewController;
        ctrl.delegate = self;
    }
}

- (void)selectCountry:(NSDictionary *)country from:(id)sender
{
    [AppDelegate shared].country = country;
    //
    [self.navigationController popViewControllerAnimated:YES];
    NSArray* toUpdate = @[[NSIndexPath indexPathForRow:0 inSection:0],
                          [NSIndexPath indexPathForRow:1 inSection:0]];
    [self.tableView reloadRowsAtIndexPaths:toUpdate withRowAnimation:NO];
}

@end
