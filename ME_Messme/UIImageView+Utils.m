//
//  UIImageView+Utils.m
//  ME_Messme
//
//  Created by MessMe on 11/01/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "UIImageView+Utils.h"

@implementation UIImageView (Utils)

- (void) setRoundedImage:(UIImage*)img
{
    self.image = img;
    self.layer.cornerRadius = self.frame.size.width / 2;
    self.clipsToBounds = YES;
}

@end
