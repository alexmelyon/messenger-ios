//
//  ME_City.m
//  ME_Messme
//
//  Created by MessMe on 02/03/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_City.h"

@implementation ME_City

- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    self.cityId = [dict[@"cityid"] intValue];
    self.cityName = dict[@"cityname"];
    self.continentId = [dict[@"continentid"] intValue];
    self.continentName = dict[@"continentname"];
    self.countryId = [dict[@"countryid"] intValue];
    self.countryName = dict[@"countryname"];
    self.regionId = [dict[@"regionid"] intValue];
    self.regionName = dict[@"regionname"];
    self.iso = [dict[@"iso"] intValue];
    self.lat = [dict[@"lat"] doubleValue];
    self.lng = [dict[@"lng"] doubleValue];
    return self;
}

@end
