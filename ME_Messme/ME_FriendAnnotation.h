//
//  ME_FriendAnnotation.h
//  ME_Messme
//
//  Created by MessMe on 01/03/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ME_Friend.h"
#import <MapKit/MapKit.h>

@interface ME_FriendAnnotation : ME_Friend <MKAnnotation>

@end
