//
//  ME_ImageCache.h
//  ME_Messme
//
//  Created by MessMe on 11/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ME_Friend.h"

@interface ME_ImageCache : NSObject

+ (void) getImage:(NSString*)key subPath:(NSString*)subPath andThen:(void(^)(UIImage*image))callback;
+ (void) getAvatar:(NSString*)key login:(NSString*)login name:(NSString*)name andThen:(void(^)(UIImage* image))callback;
+ (void) getAvatarForFriend:(ME_Friend*)friend andThen:(void(^)(UIImage* image))callback;
+ (void) getAvatarForFriend:(ME_Friend*)friend size:(CGSize)size andThen:(void(^)(UIImage* image))callback;

@end
