//
//  ME_Contact.m
//  ME_Messme
//
//  Created by MessMe on 30/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_Friend.h"
#import "NSString+Util.h"
#import "AppDelegate.h"
#import "ME_ImageCache.h"

@interface ME_Friend ()
@end

@implementation ME_Friend

- (ME_Friend*) initWithDict:(NSDictionary*)dict
{
    self = [super init];
    ME_Friend* friend = self;
    friend.ID = [[dict objectForKey:@"id"] longLongValue];
    friend.login = [dict objectForKey:@"login"];
    friend.name = [dict objectForKey:@"name"];
    friend.surname = [dict objectForKey:@"surname"];
    friend.sex = [dict objectForKey:@"sex"];
    friend.age = [[dict objectForKey:@"age"] intValue];
    friend.iso = [[dict objectForKey:@"iso"] intValue];
    friend.country = [[dict objectForKey:@"country"] intValue];
    friend.city = [[dict objectForKey:@"city"] longValue];
    friend.countryname = [dict objectForKey:@"countryname"];
    friend.cityname = [dict objectForKey:@"cityname"];
    friend.status = [[dict objectForKey:@"status"] intValue];
    friend.invite = [dict objectForKey:@"invite"];
    friend.lat = [[dict objectForKey:@"lat"] doubleValue];
    friend.lng = [[dict objectForKey:@"lng"] doubleValue];
    friend.distance = [[dict objectForKey:@"distance"] doubleValue];
    friend.geostatus = [[dict objectForKey:@"geostatus"] intValue];
    friend.isfriend = [[dict objectForKey:@"isfriend"] boolValue];
    friend.avatar = [dict objectForKey:@"avatar"];
    friend.likes = [[dict objectForKey:@"likes"] intValue];
    friend.isLiked = [[dict objectForKey:@"isliked"] boolValue];
    friend.statusDate = [dict objectForKey:@"statusdate"];
    return friend;
}

- (NSString*) getPresentName
{
    if([self.phoneName trim].length) {
        return self.phoneName;
    }
    if([[[self.name plus:@" " :self.surname] trim] length]) {
        return [self.name plus:@" " :self.surname];
    }
    return self.login;
}

@end
