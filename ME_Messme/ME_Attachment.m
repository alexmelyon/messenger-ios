//
//  AttachmentObj.m
//  ME_Messme
//
//  Created by MessMe on 11/01/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_Attachment.h"
#import "JSON.h"
#import "ME_Network.h"
#import "NSString+Util.h"

@interface ME_Attachment ()

@property (nonatomic, strong) NSData* data;

@end

@implementation ME_Attachment

- (ME_Attachment*) initWithVCardFilename:(NSString*)filename
{
    self = [super init];
    self.ID = [[NSUUID UUID] UUIDString];
    self.type = ME_ATTACHMENT_10_CONTACT;
    self.data = [NSData dataWithContentsOfFile:filename];
    self.filename = filename;
    return self;
}

- (ME_Attachment*) initWithVideoUrl:(NSURL*)url filename:(NSString*)filename
{
    self = [super init];
    self.ID = [[NSUUID UUID] UUIDString];
    self.type = ME_ATTACHMENT_2_VIDEO;
    self.data = [NSData dataWithContentsOfURL:url];
    self.filename = filename;
    return self;
}

- (ME_Attachment*) initWithGeoFilename:(NSString*)filename
{
    self = [super init];
    self.ID = [[NSUUID UUID] UUIDString];
    self.type = ME_ATTACHMENT_11_GEO;
    self.data = [NSData dataWithContentsOfFile:filename];
    self.filename = filename;
    return self;
}

- (ME_Attachment*) initWithImage:(UIImage*)img filename:(NSString*)filename
{
    if(self = [super init]) {
        self.ID = [[NSUUID UUID] UUIDString];
        self.type = ME_ATTACHMENT_1_IMAGE;
        self.data = UIImageJPEGRepresentation(img, 1.0);
        self.filename = filename;
    }
    return self;
}

- (ME_Attachment*) initWithAudioUrl:(NSURL*)url filename:(NSString*)filename
{
    if(self = [super init]) {
        self.ID = [[NSUUID UUID] UUIDString];
        self.type = ME_ATTACHMENT_4_VOICE;
        self.data = [NSData dataWithContentsOfURL:url];
        self.filename = filename;
    }
    return self;
}

- (NSData*) getNSData
{
    return self.data;
}

- (ME_Attachment*) initWithDict:(NSDictionary*)dict
{
    if(![[dict class] isSubclassOfClass:[NSDictionary class]]) {
        NSAssert(false, @"DICT IS NOT DICT");
    }
    self = [super init];
    self.contenttype = dict[@"contenttype"];
    self.desc = dict[@"description"];
    self.filename = dict[@"filename"];
    self.filesize = [dict[@"filesize"] longLongValue];
    self.ID = dict[@"id"];
    self.title = dict[@"title"];
    self.type = [dict[@"type"] intValue];
    return self;
}

- (double) getViewHeight
{
    if(ME_ATTACHMENT_1_IMAGE == self.type) {
        return 200.0;
    } else if(ME_ATTACHMENT_2_VIDEO == self.type) {
        return 200.0;
    } else if (ME_ATTACHMENT_4_VOICE == self.type || ME_ATTACHMENT_3_AUDIO == self.type) {
        return 40.0;
    } else if(ME_ATTACHMENT_10_CONTACT == self.type) {
        return 40.0;
    } else if(ME_ATTACHMENT_11_GEO == self.type) {
        return 200.0;
    }
    return 0;
}

- (NSString*) getContentType
{
    if(ME_ATTACHMENT_1_IMAGE == self.type) {
        return @"image/jpg";
    } else if(ME_ATTACHMENT_2_VIDEO == self.type) {
        return @"video/mp4";
    } else if(ME_ATTACHMENT_4_VOICE == self.type || ME_ATTACHMENT_3_AUDIO == self.type) {
        return @"audio/m4a";
    } else if(ME_ATTACHMENT_11_GEO == self.type) {
        return @"text/plain";
    }
    return @"";
}

- (void) updateNSDataDoIt:(BOOL)doIt andThen:(void(^)(ME_Attachment* attach))callback
{
    if(doIt) {
        [self updateNSDataAndThen:callback];
    } else {
        callback(self);
    }
}

- (void) updateNSDataAndThen:(void(^)(ME_Attachment* attach))callback
{
    NSString* path = [@"/message/" plus:self.ID];
    [ME_Network downloadFileFromPath:path andThen:^(NSData *data) {
        self.data = data;
        callback(self);
    }];
}

- (double) getGeoLat
{
    NSData* data = [self getNSData];
    NSString* str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if(str.length) {
        NSDictionary* json = [str JSONValue];
        double lat = [json[@"lat"] doubleValue];
        return lat;
    }
    NSLog(@"NO GEO");
    return 0;
}

- (double) getGeoLng
{
    NSData* data = [self getNSData];
    NSString* str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if(str.length) {
        NSDictionary* json = [str JSONValue];
        double lng = [json[@"lng"] doubleValue];
        return lng;
    }
    NSLog(@"NO GEO");
    return 0;
}

@end
