//
//  AppDelegate.m
//  ME_Messme
//
//  Created by MessMe on 16/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "AppDelegate.h"
#import "ME_Network.h"
//#import <AddressBook/AddressBook.h>
#import <Contacts/Contacts.h>
#import "ME_PhoneContact.h"
#import <UIKit/UIKit.h>
#import "NSArray+Utils.h"
#import "NSString+Util.h"
#import "ME_Friend.h"

@interface AppDelegate ()
- (void) loadUserDefaults;
@property (nonatomic, strong) NSMutableArray* contactsListeners;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //
    NSLog(@"didFinishLaunchingWithOptions"); // 1
    //
    [self loadUserDefaults];
    self.userProfile = [[ME_UserProfile alloc] init];
    //
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    //
    return YES;
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken  {
    NSLog(@"My token is: %@", deviceToken);
    // <4ed632c2 6a8cb31f b000a2d1 a992319d 8870fe2f 088ffd5a f18486e5 acde3f41>
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failed to get push-token, error: %@", error);
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    NSLog(@"applicationWillResignActive");
    [self saveUserDefaults];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //
    NSLog(@"applicationDidEnterBackground");
    [self saveUserDefaults];
    //
    [ME_Network send:@{@"action":@"user.setstatus", @"options":@{@"status": @(0)}} andThen:^(NSDictionary *json) {
        if(1000 == [json[@"status"] intValue]) {
            NSLog(@"I'M OFFLINE");
        }
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    //
    NSLog(@"applicationWillEnterForeground");
    //
    [self loadUserDefaults];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSLog(@"applicationDidBecomeActive"); // 3
//    [self loadUserDefaults];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"applicationWillTerminate");
//    [self saveUserDefaults];
}

+ (AppDelegate*) shared
{
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}

- (void) saveUserDefaults
{
    [[NSUserDefaults standardUserDefaults] setObject:self.phone forKey:@"phone"];
    [[NSUserDefaults standardUserDefaults] setObject:self.token forKey:@"token"];
    [[NSUserDefaults standardUserDefaults] setObject:self.phonecode forKey:@"phonecode"];
    [[NSUserDefaults standardUserDefaults] setBool:self.isGoingToTabs forKey:@"isGoingToTabs"];
}

- (void) loadUserDefaults
{
    self.phone = [[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];
    self.token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    self.phonecode = [[NSUserDefaults standardUserDefaults] objectForKey:@"phonecode"];
    self.isGoingToTabs = [[NSUserDefaults standardUserDefaults] boolForKey:@"isGoingToTabs"];
}

- (ME_Friend*) getFriendByUserid:(long long)userid
{
    for(int i = 0; i < self.friends.count; i++) {
        ME_Friend* friend = self.friends[i];
        if(userid == friend.ID) {
            return friend;
        }
    }
    return nil;
}

- (NSString*) getPresentnameForUserid:(long long)userid
{
    if(userid == [self.phone longLongValue]) {
        return @"Вы";
    } else {
        return [[self getFriendByUserid:userid] getPresentName];
    }
}

- (void) updateSections
{
    // TODO filter existing
    NSLog(@"updateSections");
//    if(!self.meContactsArr) {
//        self.meContactsArr = [NSMutableArray array];
//    }
    self.meContactsArr = [NSMutableArray array];
    for(int i = 0; i < self.friends.count; i++) {
        ME_Friend* item = [self.friends objectAtIndex:i];
        [self.meContactsArr addObject:item];
    }
    NSMutableArray* phonesArr = [self.phoneContactsArr mutableCopy];
    // remove phones contained in friends
    for(long i = 0; i < self.meContactsArr.count; i++) {
        ME_Friend* friendItem = [self.meContactsArr objectAtIndex:i];
//        BOOL exists = NO;
        for(int j = 0; j < phonesArr.count; j++) {
            ME_PhoneContact* phoneItem = [phonesArr objectAtIndex:j];
            long long friendId = friendItem.ID;
            long long phoneValue = [phoneItem.phone longLongValue];
            if(friendId == phoneValue) {
//                exists = YES;
                [phonesArr removeObjectAtIndex:j];
//                NSString* phoneName = [[friendItem.name plus:@" "] plus:friendItem.surname];
//                NSString* phoneName = [[[phoneItem.name plus:@" "] plus:phoneItem.surname] trim];
                NSString* phoneName = phoneItem.fullname;
                if(phoneName.length) {
                    friendItem.phoneName = phoneName;
                } else {
                    NSLog(@"PHONE CONTACT HAS NO LABEL");
                }
                break;
            }
        }
//        if(exists) {
//            [self.meContactsArr removeObjectAtIndex:i];
//        }
    }
    //
    NSMutableArray* sectionsArr = [NSMutableArray array];
    for(int i = 0; i < phonesArr.count; i++) {
        ME_PhoneContact* phoneItem = [phonesArr objectAtIndex:i];
        //
        NSString* username = phoneItem.fullname;
        NSString* firstLetter = [[username substringToIndex:1] uppercaseString];
        NSDictionary* section = nil;
        int j = 0;
        for(j = 0; j < sectionsArr.count; j++) {
            NSDictionary* sect = [sectionsArr objectAtIndex:j];
            NSString* title = [sect objectForKey:@"title"];
            if([title isEqualToString:firstLetter]) {
                section = sect;
                break;
            }
        }
        if(!section) {
            NSString* phone = phoneItem.phone;
            section = @{@"rows":@(1), @"title":firstLetter, @"arr":[NSMutableArray arrayWithObject:@{@"username":username, @"phone":phone}]};
            [sectionsArr addObject:section];
        } else {
            int rows = [[section objectForKey:@"rows"] intValue] + 1;
            NSMutableArray* arr = [section objectForKey:@"arr"];
            NSString* phone = phoneItem.phone;
            [arr addObject:@{@"username":username, @"phone":phone}];
            section = @{@"rows":@(rows), @"title":firstLetter, @"arr": arr};
            [sectionsArr replaceObjectAtIndex:j withObject:section];
        }
    }
    NSError* error = nil;
    NSRegularExpression* rusRegexp = [NSRegularExpression regularExpressionWithPattern:@"[А-Яа-я]" options:0 error:&error];
    if(error) {
        NSLog(@"ERROR %@", error);
    }
    NSRegularExpression* engRegexp = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z]" options:0 error:&error];
    if(error) {
        NSLog(@"ERROR %@", error);
    }
    self.meContactsSections = [sectionsArr sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        NSString* left = [obj1 objectForKey:@"title"];
        NSString* right = [obj2 objectForKey:@"title"];
        NSTextCheckingResult* leftRus = [rusRegexp firstMatchInString:left options:0 range:NSMakeRange(0, 1)];
        NSTextCheckingResult* rightEng = [engRegexp firstMatchInString:right options:0 range:NSMakeRange(0, 1)];
        if(leftRus && rightEng) {
            return -1;
        } else {
            return [left compare:right];
        }
    }];
    for(id<AppDelegateContactsListener> item in self.contactsListeners) {
        [item onContactsUpdated:self.meContactsArr];
    }
}

- (void) updateFriends:(void(^)(NSMutableArray*friends))onUpdate
{
    // then receive server contacts
    id msg = @{@"action":@"user.list",
               @"options":@{@"userlist":@[],
                            @"locale":@"ru",
                            @"status":@(-1),
                            @"isfriend":@(1),
                            @"country":@(0),
                            @"city":@(0),
                            @"sex":@"",
                            @"minage":@(0),
                            @"maxage":@(0),
                            @"mask":@"",
                            @"lat1":@(0),
                            @"lng1":@(0),
                            @"lat2":@(0),
                            @"lng2":@(0)}};
    [ME_Network send:msg andThen:^(NSDictionary *json) {
        if(1000 == [json[@"status"] intValue]) {
            [AppDelegate shared].friends = [NSMutableArray array];
            for(NSDictionary* item in [json objectForKey:@"result"]) {
                [[AppDelegate shared].friends addObject:[[ME_Friend alloc] initWithDict:item]];
            }
            onUpdate([AppDelegate shared].friends);
        } else {
            NSLog(@"ERROR USER LIST");
        }
    }];
}

- (void) fetchContactsAndThen:(void(^)(NSMutableArray* arr))onFetched
{
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if (status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusDenied) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Пожалуйста, откройте приложению доступ к контактам" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//        [self presentViewController:alert animated:TRUE completion:nil];
        return;
    }
    
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (!granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"ERROR: APP NEED PERMISSIONS");
            });
            return;
        }
        NSMutableArray<ME_PhoneContact*>* phoneUsers = [NSMutableArray array];
        
        NSError *fetchError;
        NSArray* keys = @[CNContactPhoneNumbersKey, CNContactGivenNameKey, CNContactFamilyNameKey, CNContactIdentifierKey, [CNContactFormatter descriptorForRequiredKeysForStyle:CNContactFormatterStyleFullName]];
        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
        
        CNContactFormatter *formatter = [[CNContactFormatter alloc] init];
        BOOL success = [store enumerateContactsWithFetchRequest:request error:&fetchError usingBlock:^(CNContact *contact, BOOL *stop) {
            
            NSString *fullname = [formatter stringFromContact:contact];
            NSArray <CNLabeledValue<CNPhoneNumber*>*> *phones = contact.phoneNumbers;
            if(fullname && phones.count) {
                CNLabeledValue<CNPhoneNumber*> *first = [phones firstObject];
                CNPhoneNumber* number = first.value;
                NSString* digits = number.stringValue;
                NSDictionary* user = @{@"phone":digits, @"fullname":fullname, @"name":contact.givenName, @"surname":contact.familyName};
                ME_PhoneContact* phoneContact = [ME_PhoneContact phoneContactWithDictionary:user];
                [phoneUsers addObject:phoneContact];
            }
            //
        }];
        if (!success) {
            NSLog(@"error = %@", fetchError);
        }
        // get digits only
        for(int i = 0; i < phoneUsers.count; i++) {
            ME_PhoneContact* contact = [phoneUsers objectAtIndex:i];
            NSString* phone = contact.phone;
            contact.phone = [[phone componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        }
        // replace first 8
        for(int i = 0; i < phoneUsers.count; i++) {
            ME_PhoneContact* contact = [phoneUsers objectAtIndex:i];
            if([[contact.phone substringToIndex:1] isEqualToString:@"8"]) {
                contact.phone = [NSString stringWithFormat:@"%@%@", [AppDelegate shared].phonecode, [contact.phone substringFromIndex:1]];
            }
        }
        // remove duplicates
        NSMutableIndexSet* toRemove = [NSMutableIndexSet new];
        for(int i = 0; i < phoneUsers.count; i++) {
            for(int j = 0; j < i; j++) {
                if([phoneUsers[i].phone isEqualToString:phoneUsers[j].phone]) {
                    [toRemove addIndex:j];
                }
            }
        }
        [phoneUsers removeObjectsAtIndexes:toRemove];
        NSLog(@"REMOVED DUPLICATES %tu", toRemove.count);
        //
        self.phoneContactsArr = phoneUsers;
        dispatch_async(dispatch_get_main_queue(), ^{
            onFetched(phoneUsers);
        });
    }];
}

- (NSArray*) getPhoneNumbersArr
{
    NSMutableArray* arr = [NSMutableArray array];
    for(ME_PhoneContact* item in self.phoneContactsArr) {
        NSString* phone = item.phone;
        // replace first 8
        if([[phone substringToIndex:1] isEqualToString:@"8"]) {
            phone = [phone substringFromIndex:1];
            NSString* prefix = self.phonecode;
            phone = [NSString stringWithFormat:@"%@%@", prefix, phone];
        }
        //
        [arr addObject:@([phone longLongValue])];
    }
    return arr;
}

- (NSInteger) getNumberOfSectionsForContacts
{
    return self.meContactsSections.count + 1;
}

- (NSInteger) getNumberOfRowsForContactsSection:(NSInteger)section
{
    if(0L == section) {
        return self.meContactsArr.count;
    } else {
        NSInteger row = section - 1L;
        NSDictionary* section = [self.meContactsSections objectAtIndex:row];
        return [[section objectForKey:@"rows"] intValue];
    }
}

- (NSString*) getTitleForHeaderInContactsSection:(NSInteger)section
{
    if(0 == section) {
        return nil;
    } else {
        return [[self.meContactsSections objectAtIndex:section-1] objectForKey:@"title"];
    }
}

- (void) addContactsListener:(id)sender
{
    if(!self.contactsListeners) {
        self.contactsListeners = [NSMutableArray array];
    }
    [self.contactsListeners addObject:sender];
}

- (void) removeContactsListener:(id)sender
{
    [self.contactsListeners removeObject:sender];
}
- (void) exitProfile
{
//    self.userProfile = nil;
    self.userProfile = [ME_UserProfile new];
    self.friends = [NSMutableArray new];
    self.phoneContactsArr = [NSMutableArray new];
    self.phone = nil;
    self.token = nil;
    self.isGoingToTabs = NO;
    [self saveUserDefaults];
}

- (void) getUserById:(long long)userid andThen:(void(^)(ME_Friend* meFriend))callback
{
    ME_Friend* friend = [self getFriendByUserid:userid];
    if(friend) {
        callback(friend);
        return;
    }
    friend = self.meEnemiesDict[@(userid)];
    if(friend) {
        callback(friend);
        return;
    }
    if(![self getFriendByUserid:userid]) {
        [ME_Network send:@{@"action":@"user.info", @"options":@{@"userid":@(userid), @"locale":@"ru"}} andThen:^(NSDictionary *json) {
            NSDictionary* result = json[@"result"];
            ME_Friend* friend = [[ME_Friend alloc] initWithDict:result];
//            if(!self.meEnemiesArr) {
//                self.meEnemiesArr = [NSMutableArray array];
//            }
            if(!self.meEnemiesDict) {
                self.meEnemiesDict = [NSMutableDictionary dictionary];
            }
//            BOOL isContains = NO;
//            for(int i = 0)
//            [self.meEnemiesArr addObject:friend];
            [self.meEnemiesDict setObject:friend forKey:@(friend.ID)];
            //
            if(callback) {
                callback(friend);
            } else {
                NSLog(@"SOMETHING WRONG");
            }
        }];
    }
}

@end
