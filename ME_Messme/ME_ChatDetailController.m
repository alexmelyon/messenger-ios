//
//  ME_ChatDetailController.m
//  ME_Messme
//
//  Created by MessMe on 18/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_ChatDetailController.h"
#import "ME_Network.h"
#import "AppDelegate.h"
#import "NSString+Util.h"
#import "ME_ChatCell.h"
#import "ME_Attachment.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ME_MessageModel.h"
#import "ME_AudioPlayer.h"
#import <CoreLocation/CoreLocation.h>
#import <MobileCoreServices/UTCoreTypes.h>
@import Contacts;
@import ContactsUI;
#import "ME_GeoViewController.h"
#import <AudioToolbox/AudioServices.h>
#import "NSData+Util.h"
#import "NSString+Util.h"
#import "ME_ImageCache.h"
#import "UIImageView+Utils.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "UIImage+Utils.h"
#import "ME_FriendProfileController.h"

static NSString* SEGUE_GEO = @"segueGeo";

@interface ME_ChatDetailController () <UITableViewDataSource,
UITableViewDelegate,
UITextFieldDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
UIActionSheetDelegate,
ME_ChatCellDelegate,
UIGestureRecognizerDelegate,
CLLocationManagerDelegate,
CNContactViewControllerDelegate,
ME_GeoViewControllerDelegate,
CNContactPickerDelegate,
UITextViewDelegate>

- (IBAction)backAction:(id)sender;
- (IBAction)lockAction:(id)sender;
//@property (nonatomic, strong) NSMutableArray* modelMessagesArr;

@property (nonatomic, strong) UIView* footerView;
//@property (weak, nonatomic) IBOutlet UITextField *sendField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)sendAction:(id)sender;
@property (nonatomic, strong) UIToolbar* keyboardToolbar;
@property (weak, nonatomic) IBOutlet UIView *toolbarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toolbarBottomConstraint;
- (IBAction) selectCamera:(id)sender;
- (IBAction) selectClip:(id)sender;
- (IBAction) selectTimer:(id)sender;
- (IBAction) selectGeo:(id)sender;
- (IBAction) selectTranslate:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollHeightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *attachmentsScrollView;
- (IBAction)sendTouchDown:(id)sender;
@property (nonatomic, strong) ME_AudioPlayer* player;
@property (nonatomic, strong) UIView* audioTip;
- (IBAction)sendLongtapAction:(UILongPressGestureRecognizer*)sender;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property BOOL isRecordStarted;
- (IBAction)sendPanAction:(UIPanGestureRecognizer*)recognizer;
@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *sendButtonPanGesture;
@property (strong, nonatomic) IBOutlet UILongPressGestureRecognizer *sendButtonLongpressGesture;
//@property BOOL isAudioUploaded;
@property BOOL isSnapActive;
@property (weak, nonatomic) IBOutlet UIButton *timerButton;
@property (nonatomic, strong) CLLocationManager* locationMgr;
//@property (nonatomic, strong) ME_Attachment* tempAttachment;
@property double tempGeoGotoLat;
@property double tempGeoGotoLng;
@property double tempMessageGeoLat;
@property double tempMessageGeoLng;
@property BOOL isLocationTracking;
@property (weak, nonatomic) IBOutlet UIButton *geoButton;
@property (weak, nonatomic) IBOutlet UITextView *sendTextView;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sendTextViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sendPanelHeightConstraint;
@property (nonatomic, strong) UIImageView* titleAvatarImageView;
@property (nonatomic, strong) UILabel* titleLabel;
@property (nonatomic, strong) UILabel* onlineLabel;
@property (nonatomic, strong) MPMoviePlayerViewController* movieController;
@property (nonatomic, strong) MPMoviePlayerController* moviePlayer;
@property (nonatomic, strong) NSMutableArray* dlgUserlist;

@end

@implementation ME_ChatDetailController

+ (void) showChatUserid:(long long)userid fromCtrl:(UIViewController*)fromCtrl
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ME_ChatDetailController* ctrl = [storyboard instantiateViewControllerWithIdentifier:@"chatDetail"];
    ctrl.userlist = [NSMutableArray arrayWithObject: @(userid)];
    ctrl.grpModel = nil;
    UINavigationController* navCtrl = [[UINavigationController alloc] initWithRootViewController:ctrl];
    [fromCtrl presentViewController:navCtrl animated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    NSLog(@"============================= USERID %lld", [self.userlist[0] longLongValue]);
    //
    UIEdgeInsets ins = self.sendTextView.contentInset;
    [self.sendTextView setTextContainerInset:UIEdgeInsetsMake(ins.top, ins.left, ins.bottom, 30)];
    //
    double w = 320 - 66 * 2;
    UIView* navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, w, 44)];
    self.titleAvatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 36, 36)];
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(titleAvatarClicked:)];
    self.titleAvatarImageView.userInteractionEnabled = YES;
    [self.titleAvatarImageView addGestureRecognizer:tap];
    [navView addSubview:self.titleAvatarImageView];
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, w, 36)];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [navView addSubview:self.titleLabel];
    self.onlineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, w, 10)];
    self.onlineLabel.textColor = [UIColor grayColor];
    self.onlineLabel.textAlignment = NSTextAlignmentCenter;
    self.onlineLabel.font = [UIFont systemFontOfSize:10];
    [navView addSubview:self.onlineLabel];
    self.navigationItem.titleView = navView;
    //
//    long long userid = [self.userlist[0] longLongValue];
//    ME_Friend* friend = [[AppDelegate shared] getFriendByUserid:userid];
//    self.titleLabel.text = [friend getPresentName];
    //
    self.sendTextView.layer.borderWidth = 1.0f;
    self.sendTextView.layer.borderColor = [[UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1.0] CGColor];
    self.sendTextView.layer.cornerRadius = 5.0f;
    //
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pattern_messages"]];
    //
    self.modelMessagesArr = [[NSMutableArray alloc] init];
    //
    void(^receive)(NSDictionary* json) = ^(NSDictionary* json){
        if(1000 == [json[@"status"] intValue]) {
            NSDictionary* result = [json objectForKey:@"result"];
            ME_MessageModel* msgModel = [[ME_MessageModel alloc] initWithDict:result];
            long long activeUser = [self.userlist[0] longLongValue];
            if(msgModel.userid == activeUser) {
                msgModel.status = MSG_STATUS_2_READED;
                msgModel.isIncoming = YES;
                [self addOneMessage:msgModel];
            }
        }
    };
    [ME_Network onAction:@"ongroupchatmessage" setCallback:receive];
    [ME_Network onAction:@"onmessage" setCallback:receive];
    //
    self.sendTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    //
    //
    self.player = [[ME_AudioPlayer alloc] init];
    [self.player initAudio];
    //
    long long userid = [self.userlist[0] longLongValue];
    // update header
    if(self.grpModel) {
        self.titleLabel.text = self.grpModel.name;
        self.onlineLabel.hidden = YES;
        NSString* name = self.grpModel.name;
        [ME_ImageCache getAvatar:self.grpModel.avatar login:name name:name andThen:^(UIImage *image) {
            [self.titleAvatarImageView setRoundedImage:image];
        }];
    } else {
        [[AppDelegate shared] getUserById:userid andThen:^(ME_Friend *friend) {
            NSString* presentName = [friend getPresentName];
            self.titleLabel.text = presentName;
            self.onlineLabel.text = friend.status ? @"В сети" : @"Не в сети";
            self.onlineLabel.hidden = NO;
            [ME_ImageCache getAvatar:friend.avatar login:friend.login name:presentName andThen:^(UIImage *image) {
                [self.titleAvatarImageView setRoundedImage:image];
            }];
        }];
    }
    //
    void(^getListBlock)(NSDictionary* json) = ^(NSDictionary *json) {
        NSArray* result = [json objectForKey:@"result"];
        NSMutableArray* list = [NSMutableArray array];
        for(int i = 0; i < result.count; i++) {
            NSDictionary* dict = result[i];
            ME_MessageModel* msgModel = [[ME_MessageModel alloc] initWithDict:dict];
            msgModel.isIncoming = YES;
            [list addObject:msgModel];
        }
        [self addMessageList:list];
    };
    [ME_Network send:@{@"action":@"message.list", @"options":@{@"userid":@(userid), @"page":@(1), @"size":@(20)}} andThen:getListBlock];
    if(self.grpModel) {
        [ME_Network send:@{@"action":@"groupchat.messagelist", @"options":@{@"id":self.grpModel.ID, @"page":@(1), @"size":@(20)}} andThen:getListBlock];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //
}

- (NSMutableArray*) modelMessagesArr
{
    long long userid = [self.userlist[0] longLongValue];
    return [[[AppDelegate shared] dialogs] objectForKey:@(userid)];
}

- (void) setUserlist:(NSMutableArray*)userlist
{
    // dog-nail
    if(self.grpModel) {
        self.grpModel.userlist = userlist;
    } else {
        self.dlgUserlist = userlist;
    }
}
- (NSMutableArray*) userlist
{
    // dog-nail
    if(self.grpModel) {
        return self.grpModel.userlist;
    } else {
        return self.dlgUserlist;
    }
}
- (void) setModelMessagesArr:(NSMutableArray*)arr
{
    if(![[AppDelegate shared] dialogs]) {
        [AppDelegate shared].dialogs = [[NSMutableDictionary alloc] init];
    }
    long long userid = [self.userlist[0] longLongValue];
    [[AppDelegate shared].dialogs setObject:arr forKey:@(userid)];
}

- (void) keyboardWillShow:(id)sender
{
    [self.view bringSubviewToFront:self.toolbarView];
    //
    NSDictionary* info = [sender userInfo];
    double duration = [info[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rawFrame = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect kbFrame = [self.view convertRect:rawFrame fromView:nil];
    self.toolbarBottomConstraint.constant = kbFrame.size.height;
//    self.bottomConstraint.constant = 216;
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void) keyboardWillHide:(id)sender
{
    NSDictionary* info = [sender userInfo];
    double duration = [info[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//    CGRect rawFrame = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    //    CGRect kbFrame = [self.view convertRect:rawFrame fromView:nil];
//    self.toolbarBottomConstraint.constant = self.tabBarController.tabBar.frame.size.height;
    self.toolbarBottomConstraint.constant = 0;
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) titleAvatarClicked:(UITapGestureRecognizer*)sender
{
    NSLog(@"titleAvatarClicked");
    if(!self.grpModel && self.userlist.count) {
        UIImageView* imageView = (UIImageView*)sender.view;
        long long userid = [self.userlist[0] longLongValue];
        [ME_FriendProfileController showProfileUserid:userid fromCtrl:self];
    }
}

- (void) selectCamera:(id)sender
{
    NSLog(@"selectCamera");
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    //    imagePickerController.allowsEditing = YES;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeImage];
        [self.navigationController presentViewController:imagePickerController animated:YES completion:nil];
    } else {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Camera is not available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}
- (void) selectClip:(id)sender
{
    NSLog(@"selectClip");
//    [self.sendField resignFirstResponder];
    [self.sendTextView resignFirstResponder];
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Отмена"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Снять фото или видео", @"Фото / Видео", @"Выбрать местоположение", @"Контакт", nil];
    [actionSheet showInView:self.view];
}

- (void) selectGallery:(id)sender
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    //    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeImage];
    [self.navigationController presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(0 == buttonIndex) {
        NSLog(@"SELECT CAMERA");
        [self selectCamera:nil];
    } else if(1 == buttonIndex) {
        NSLog(@"SELECT GALLERY");
        [self selectGallery:nil];
    } else if(2 == buttonIndex) {
        NSLog(@"SELECT MY GEO");
        self.tempGeoGotoLat = 0;
        self.tempGeoGotoLng = 0;
        [self performSegueWithIdentifier:SEGUE_GEO sender:nil];
    } else if(3 == buttonIndex) {
        [self selectContact:nil];
    }
}

- (void) selectContact:(id)sender
{
    NSLog(@"SELECT CONTACT");
    CNContactPickerViewController* picker = [[CNContactPickerViewController alloc] init];
    picker.delegate = self;
    [self.navigationController presentViewController:picker animated:YES completion:nil];
}

- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact
{
    NSLog(@"didSelectContact");
    NSError* err;
    NSData* data = [CNContactVCardSerialization dataWithContacts:@[contact] error:&err];
    if(err) {
        NSLog(@"CANNOT WRITE FILE");
    }
    NSString* fullPath = [NSData docPathWith:@"contact.vcf"];
    [data writeToFile:fullPath atomically:YES];
    ME_Attachment* attach = [[ME_Attachment alloc] initWithVCardFilename:fullPath];
    [self addAttachment:attach];
}

- (void) selectTimer:(id)sender
{
    NSLog(@"selectTimer");
    self.isSnapActive = !self.isSnapActive;
    [self setSnap:self.isSnapActive];
}

- (void) setSnap:(BOOL)isOn
{
    if(self.isSnapActive) {
        [self.timerButton setImage:[UIImage imageNamed:@"timer_active"] forState:UIControlStateNormal];
    } else {
        [self.timerButton setImage:[UIImage imageNamed:@"timer"] forState:UIControlStateNormal];
    }
}

- (void) selectGeo:(id)sender
{
    NSLog(@"selectGeo");
    self.isLocationTracking = !self.isLocationTracking;
    if(self.isLocationTracking) {
        [self.geoButton setImage:[UIImage imageNamed:@"geo_active"] forState:UIControlStateNormal];
        //
        self.locationMgr = [[CLLocationManager alloc] init];
        self.locationMgr.delegate = self;
        self.locationMgr.distanceFilter = kCLDistanceFilterNone;
        self.locationMgr.desiredAccuracy = kCLLocationAccuracyBest;
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            [self.locationMgr requestWhenInUseAuthorization];
        }
        [self.locationMgr startUpdatingLocation];
    } else {
        self.tempMessageGeoLat = 0;
        self.tempMessageGeoLng = 0;
        [self.geoButton setImage:[UIImage imageNamed:@"geo"] forState:UIControlStateNormal];
    }
    self.geoButton.contentMode = UIViewContentModeCenter;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    if(!self.isLocationTracking) {
        [self.locationMgr stopUpdatingLocation];
    }
    CLLocation* loc = [locations lastObject];
    self.tempMessageGeoLat = loc.coordinate.latitude;
    self.tempMessageGeoLng = loc.coordinate.longitude;
}


- (void) selectTranslate:(id)sender
{
    NSLog(@"selectTranslate");
}

#pragma mark UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSLog(@"didFinishPickingMediaWithInfo");
    [picker dismissViewControllerAnimated:YES completion:nil];
//    if(info[@"UIImagePickerControllerMediaType"])
    UIImage* img = [info objectForKey:UIImagePickerControllerOriginalImage];
    //
    if([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:(NSString*)kUTTypeImage]) {
        if(img) {
//            img = [UIImage imageWithCGImage:[img CGImage] scale:1.0 orientation:UIImageOrientationUp];
            img = [img fixRotation];
            NSURL* refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
            ALAssetsLibraryAssetForURLResultBlock resultBlock = ^(ALAsset* imageAsset) {
                ALAssetRepresentation* imageRep = [imageAsset defaultRepresentation];
                NSString* filename = [NSString stringWithFormat:@"%@.jpg", [[NSUUID UUID] UUIDString]];
//                NSString* filename = [NSString stringWithFormat:@"%@", [[NSUUID UUID] UUIDString]];
                ME_Attachment* attach = [[ME_Attachment alloc] initWithImage:img filename:filename];
                [self addAttachment:attach];
                
            };
            ALAssetsLibrary* assetsLibrary = [[ALAssetsLibrary alloc] init];
            [assetsLibrary assetForURL:refURL resultBlock:resultBlock failureBlock:nil];
        }
    } else if([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.movie"]) {
        NSURL* url = info[UIImagePickerControllerMediaURL];
        NSString* filename = [[[NSUUID UUID] UUIDString] plus:@".mp4"];
        ME_Attachment* attach = [[ME_Attachment alloc] initWithVideoUrl:url filename:filename];
        [self addAttachment:attach];
    } else {
        NSLog(@"WRONG MEDIA TYPE");
    }
}

- (void) addAttachment:(ME_Attachment*)attach
{
    [self sendMessage:@"" attachArr:@[attach]];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)lockAction:(id)sender
{
    NSLog(@"lockAction");
}

- (IBAction)sendAction:(id)sender
{
    NSLog(@"sendAction");
    [self hideAudioTip];
//    if([self.sendField.text length]) {
    if([self.sendTextView.text length]) {
//        [self sendMessage:self.sendField.text attachArr:nil];
        [self sendMessage:self.sendTextView.text attachArr:nil];
//        [self.sendField resignFirstResponder];
        [self.sendTextView resignFirstResponder];
    }
    if(self.isRecordStarted) {
        [self hideAudioTip];
        [self.player stopRecording];
        self.isRecordStarted = NO;
        self.sendButtonLongpressGesture.enabled = NO;
    }
}

- (IBAction)sendTouchDown:(id)sender
{
    NSLog(@"sendTouchDown");

//    self.sendButtonLongpressGesture.enabled = YES;
//    NSLog(@"RECORD NO 3");
//    self.isRecordStarted = NO;
}

- (IBAction)sendLongtapAction:(UILongPressGestureRecognizer*)sender
{
    NSLog(@"sendLongtapAction");
    if(sender.state == UIGestureRecognizerStateBegan) {
        NSLog(@"ENABLED NO 2");
//        self.sendButtonLongpressGesture.enabled = NO;
        if(!self.isRecordStarted) {
            self.isRecordStarted = YES;
            [self.player startRecording];
            [self showAudioTip];
        }
    } else if(sender.state == UIGestureRecognizerStateEnded) {
        [self.player stopRecording];
        self.isRecordStarted = NO;
        [self hideAudioTip];
        //
        ME_Attachment* attach = [[ME_Attachment alloc] initWithAudioUrl:[self.player getRecUrl] filename:self.player.audioRecName];
        [self addAttachment:attach];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (IBAction) sendPanAction:(UIPanGestureRecognizer*)recognizer
{
    CGPoint translation = [recognizer translationInView:self.sendButton.superview];
    translation.y = 0;
    if(!self.isRecordStarted) {
        translation.x = 0;
    }
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x, recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.sendButton.superview];
    //
    if(recognizer.view.center.x < 320 / 2) {
        NSLog(@"RECORD NO 1");
        self.isRecordStarted = NO;
        [self hideAudioTip];
        [self.player stopRecording];
    }
    if(recognizer.state == UIGestureRecognizerStateEnded && self.isRecordStarted) {
        NSLog(@"DRAG ENDED");
        [self.player stopRecording];
        [self hideAudioTip];
        if(self.isRecordStarted) {
            NSLog(@"RECORD NO 2");
            self.isRecordStarted = NO;
            NSLog(@"ENABLED NO 1");
//            self.sendButtonLongpressGesture.enabled = NO;
            //
            ME_Attachment* attach = [[ME_Attachment alloc] initWithAudioUrl:[self.player getRecUrl] filename:self.player.audioRecName];
            [self addAttachment:attach];
        }

    }
}

- (void) showAudioTip
{
    if(!self.audioTip) {
//        UIView* superview = self.sendField.superview;
        UIView* superview = self.sendTextView.superview;
        self.audioTip = [[UIView alloc] initWithFrame:CGRectMake(0, 0, superview.frame.size.width, 44)];
        CGFloat height = 44;
        //
        UIImageView* trashImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, height, height)];
        trashImageView.contentMode = UIViewContentModeCenter;
        trashImageView.image = [UIImage imageNamed:@"no-sm"];
        [self.audioTip addSubview:trashImageView];
        //
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(height + 5, 0, 320-height*2, height)];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = @"Влево - отмена";
        [self.audioTip addSubview:label];
        [superview addSubview:self.audioTip];
    }
    self.sendTextView.hidden = YES;
    self.audioTip.hidden = NO;
}

- (void) hideAudioTip
{
    self.audioTip.hidden = YES;
    self.sendTextView.hidden = NO;
    self.sendButton.frame = CGRectMake(280, 4, 32, 32);
    [self.sendButton setImage:[UIImage imageNamed:@"tab_microphone_active"] forState:UIControlStateNormal];
}

- (void) sendMessage:(NSString*)text attachArr:(NSArray*)attachArr
{
    NSLog(@"SEND MESSAGE");
    long long userid = [self.userlist[0] longLongValue];
//    long long userid = [self.dialog getUserId];
    if(!attachArr) {
        attachArr = @[];
    }
    int type = self.isSnapActive ? 1 : 0;
    ME_Attachment* attach = nil;
    if(attachArr.count) {
        attach = attachArr[0];
    }
    NSArray* attachArrForJson = nil;
    if(attach) {
        attachArrForJson = @[attach.ID];
    } else {
        attachArrForJson = @[];
    }
    double lat = self.isLocationTracking ? self.tempMessageGeoLat : 0;
    double lng = self.isLocationTracking ? self.tempMessageGeoLng : 0;
    int dialogtype = self.grpModel ? 1 : 0;
    id opts = nil;
    int timer = type == MSG_TYPE_1_SNAP ? 15 : 0;
    if(self.grpModel) {
        opts = @{@"id": self.grpModel.ID, @"message": text, @"type":@(type), @"dialogtype":@(dialogtype), @"lat":@(lat), @"lng":@(lng), @"timer":@(timer), @"attach":attachArrForJson};
    } else {
        opts = @{@"userlist": @[@(userid)], @"message":text, @"type":@(type), @"dialogtype":@(dialogtype), @"attach":attachArrForJson, @"lat":@(lat), @"lng":@(lng), @"timer":@(timer)};
    }
    
    NSString* action = self.grpModel ? @"groupchat.send" : @"message.send";
    id msg = @{@"action":action, @"options": opts};
    ME_MessageModel* msgModel = [[ME_MessageModel alloc] initWithDict:msg andAttachArr:attachArr];
    [self addOneMessage:msgModel];
    //
    BOOL doUpload = msgModel.attachArr.count > 0;
    NSArray* userdataArr = nil;
    if(attach) {
        userdataArr = @[@{@"id":attach.ID, @"type":@(attach.type), @"title":attach.filename, @"description":@""}];
    }
    [ME_Network uploadDoIt:doUpload data:[attach getNSData] filename:attach.filename userdata:userdataArr contentType:[attach getContentType] andThen:^{
        NSLog(@"UPLOADED");
        [ME_Network send:msg andThen:^(NSDictionary *json) {
            self.sendTextView.text = @"";
            NSDictionary* result = [json objectForKey:@"result"];
            if(1000 == [json[@"status"] intValue]) {
                [msgModel updateWithDict:result];
                msgModel.isIncoming = YES;
                msgModel.status = MSG_STATUS_1_DELIVERED;
                [self setStatusForMsg:msgModel];
//                [self updateMessageModel:msgModel];
            }
        }];
    }];
}

- (void) updateMessageModel:(ME_MessageModel*)inputMsg
{
    for(int i = 0; i < self.modelMessagesArr.count; i++) {
        ME_MessageModel* msgModel = self.modelMessagesArr[i];
        if([inputMsg.ID isEqualToString:msgModel.ID]) {
            [self.modelMessagesArr replaceObjectAtIndex:i withObject:inputMsg];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:NO];
            break;
        }
    }
}

- (void) addMessageList:(NSMutableArray*)list
{
    self.modelMessagesArr = list;
    [self.tableView reloadData];
    [self scrollToNewestMessageAnimated:NO];
    for (int i = 0; i < self.modelMessagesArr.count; i++) {
        ME_MessageModel* msgModel = self.modelMessagesArr[i];
        if(MSG_TYPE_1_SNAP == msgModel.type) {
            [self performSelector:@selector(checkSnapMessages:) withObject:msgModel afterDelay:15.0];
        }
        [self setStatusForMsg:msgModel];
    }
}

- (void) addOneMessage:(ME_MessageModel*)msgModel
{
//    msgModel = [self changeMessageName:msgModel];
    //
    [self.modelMessagesArr addObject:msgModel];
    //
    int index = (int)self.modelMessagesArr.count - 1;
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    [self scrollToNewestMessageAnimated:YES];
    //
    [self setStatusForMsg:msgModel];
}

- (void) setStatusForMsg:(ME_MessageModel*)msgModel
{
    if(msgModel.isIncoming) {
        if(MSG_TYPE_1_SNAP == msgModel.type) {
            [self performSelector:@selector(checkSnapMessages:) withObject:msgModel afterDelay:15.0];
        }
        BOOL isMine = msgModel.userid == [[[AppDelegate shared] phone] longLongValue];
        if(msgModel.status != MSG_STATUS_2_READED && !isMine) {
            NSString* msgId = msgModel.ID;
            [ME_Network send:@{@"action":@"message.setstatus", @"options":@{@"list":@[msgId], @"status":@(MSG_STATUS_2_READED)}} andThen:^(NSDictionary *json) {
                msgModel.status = MSG_STATUS_2_READED;
            }];
        }
    }
    [self updateMessageModel:msgModel];
}

- (void) checkSnapMessages:(ME_MessageModel*)inputMsg
{
    NSLog(@"CHECK SNAP MESSAGES");
    if(!inputMsg.ID) {
        NSLog(@"CHECK SNAP MESSAGES ERROR");
    }
    for(int i = 0; i < self.modelMessagesArr.count; i++) {
        ME_MessageModel* msgModel = self.modelMessagesArr[i];
//        if([msgId isEqualToString: msgModel.ID]) {
        if([inputMsg.ID isEqualToString: msgModel.ID]) {
            [self.modelMessagesArr removeObjectAtIndex:i];
            NSArray* toDelete = @[[NSIndexPath indexPathForRow:i inSection:0]];
            [self.tableView deleteRowsAtIndexPaths:toDelete withRowAnimation:YES];
            //
        }
    }
}

- (ME_MessageModel*) changeMessageName:(ME_MessageModel*)msgModel
{
    if(msgModel.owner) {
        msgModel.userid = [[AppDelegate shared].phone longLongValue];
    } else {
        msgModel.userid = [self.userlist[0] longLongValue];
//        msgModel.userid = [self.dialog getUserId];
    }
    return msgModel;
}

- (void)scrollToNewestMessageAnimated:(BOOL)animated
{
    if(self.modelMessagesArr.count) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:(self.modelMessagesArr.count - 1) inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    NSString* result = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if(result.length) {
        [self.sendButton setImage:[UIImage imageNamed:@"plane"] forState:UIControlStateNormal];
        [self adjustSendPanelHeightWithText:result];
    } else {
        [self.sendButton setImage:[UIImage imageNamed:@"tab_microphone_active"] forState:UIControlStateNormal];
    }
    return YES;
}

- (void) adjustSendPanelHeightWithText:(NSString*)result
{
    CGRect f = self.sendTextView.frame;
    double h = [result getHeightForWidth:f.size.width - 10];
    double defaultSendH = 30;
    if(h < defaultSendH) {
        h = defaultSendH;
    } else {
        h += 5;
    }
    double maxSendH = 75;
    if(h > maxSendH) {
        h = maxSendH;
    }
    double defaultPanelH = 88;
    self.sendPanelHeightConstraint.constant = defaultPanelH - defaultSendH + h;
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    NSString* result = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    if(result.length) {
//        [self.sendButton setImage:[UIImage imageNamed:@"plane"] forState:UIControlStateNormal];
//        // change textfield height
////        CGRect f = self.sendField.frame;
//        CGRect f = self.sendTextView.frame;
//        double h = [result getHeightForWidth:f.size.width];
//        if(h < 30) {
//            h = 30;
//        }
//        NSLog(@"h = %f", h);
////        [self.sendField setFrame:CGRectMake(f.origin.x, f.origin.y, f.size.width, h)];
//        [self.sendTextView setFrame:CGRectMake(f.origin.x, f.origin.y, f.size.width, h)];
//    } else {
//        [self.sendButton setImage:[UIImage imageNamed:@"tab_microphone_active"] forState:UIControlStateNormal];
//    }
//    return YES;
//}

#pragma mark - ME_GeoViewControllerDelegate

- (void)ctrl:(UIViewController *)ctrl chooseGeoLat:(double)lat lng:(double)lng
{
    id fileObj = @{@"lat":@(lat), @"lng":@(lng), @"description":@""};
    NSString* fileBody = [fileObj JSONRepresentation];
    NSLog(@"ATTACH GEO %@", fileBody);
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString* geoFilename = [NSString stringWithFormat:@"%@/location.txt", documentsDirectory];
    NSString* fullname = [NSData docPathWith:@"location.txt"];
    //
    NSError* err;
    [fileBody writeToFile:fullname atomically:NO encoding:NSUTF8StringEncoding error:&err];
    
    if(err) {
        NSLog(@"SOMETHING BAD WITH LOCATION");
    } else {
        ME_Attachment* attach = [[ME_Attachment alloc] initWithGeoFilename:fullname];
        [self addAttachment:attach];
    }
}

#pragma mark CNContactViewControllerDelegate

- (void)contactViewController:(CNContactViewController *)viewController didCompleteWithContact:(CNContact *)contact
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - ME_ChatCellDelegate

- (void)chatCell:(ME_ChatCell *)cell imageClicked:(ME_Attachment *)attach
{
    NSLog(@"imageClicked");
}

- (void)chatCell:(ME_ChatCell *)cell videoClicked:(ME_Attachment *)attach
{
    NSString* urlStr = [NSString stringWithFormat:@"https://files.messme.me:8102/message/%@", attach.ID];
//    urlStr = @"https://s3.amazonaws.com/adplayer/colgate.mp4";
//    urlStr = @"http://www.ebookfrenzy.com/ios_book/movie/movie.mov";
    NSLog(@"videoClicked %@", urlStr);
//    NSURL* url = [NSURL URLWithString:urlStr];
//    self.movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:url];
//    [self.movieController.moviePlayer prepareToPlay];
//    [self.movieController.moviePlayer play];
//    [self presentMoviePlayerViewControllerAnimated:self.movieController];
    //
    [ME_Network downloadFileFrom:urlStr andThen:^(NSData *data) {
        NSString* filename = [NSString stringWithFormat:@"%@.mp4", attach.ID];
        NSString* fullpath = [NSData docPathWith:filename];
//        [data saveTo:filename];
        [data writeToFile:fullpath atomically:YES];
        NSURL* fileUrl = [NSURL fileURLWithPath:fullpath];
        MPMoviePlayerViewController* videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:fileUrl];
        [self presentMoviePlayerViewControllerAnimated:videoPlayerView];
        [videoPlayerView.moviePlayer play];
//        MPMoviePlayerController* moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:fileUrl];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
//        moviePlayer.controlStyle = MPMovieControlStyleDefault;
//        moviePlayer.shouldAutoplay = YES;
//        [self.view addSubview:moviePlayer.view];
//        [moviePlayer setFullscreen:YES animated:YES];
    }];
    //
//    MPMoviePlayerController* moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
//    moviePlayer.controlStyle = MPMovieControlStyleDefault;
//    moviePlayer.shouldAutoplay = YES;
//    [self.view addSubview:moviePlayer.view];
//    [moviePlayer setFullscreen:YES animated:YES];
    //
//    AVPlayer* player = [AVPlayer playerWithURL:url];
//    AVPlayerViewController* playerViewController = [[AVPlayerViewController alloc] init];
//    playerViewController.player = player;
//    [self presentViewController:playerViewController animated:YES completion:^{
//        [playerViewController.player play];
//    }];
    //
//    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.moviePlayer];
//    self.moviePlayer.controlStyle = MPMovieControlStyleDefault;
//    self.moviePlayer.shouldAutoplay = YES;
//    [self.view addSubview:self.moviePlayer.view];
//    [self.moviePlayer setFullscreen:YES animated:YES];
//    [self.moviePlayer play];
//    [self.view bringSubviewToFront:self.moviePlayer.view];
    //
//    AVAsset* asset = [AVAsset assetWithURL:url];
//    AVPlayerItem* playerItem = [[AVPlayerItem alloc] initWithAsset:asset];
//    AVPlayer* avPlayer = [AVPlayer playerWithPlayerItem:playerItem];
//    AVPlayerLayer* avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:avPlayer];
//    avPlayerLayer.frame = self.view.frame;
//    [self.view addSubview:avPlayerLayer];
//    [avPlayer play];
}

- (void) moviePlaybackDidFinish:(NSNotification*)notification
{
    NSLog(@"moviePlaybackDidFinish");
    
}

- (void)chatCell:(UITableViewCell *)cell addContactClicked:(CNContact *)contact
{
    CNContactStore *store = [[CNContactStore alloc] init];
    CNContactViewController *controller = [CNContactViewController viewControllerForNewContact:contact];
    controller.contactStore = store;
    controller.delegate = self;
    UINavigationController* navCtrl = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navCtrl animated:YES completion:nil];
}

- (void)chatCell:(ME_ChatCell *)cell audioClicked:(ME_Attachment*)attach
{
    NSLog(@"audioClicked play=");
    [self.player playFromCell:cell attach:attach];
}

- (void)chatCell:(UITableViewCell *)cell geoClicked:(ME_Attachment *)attach
{
    NSLog(@"GEO CLICKED");
    self.tempGeoGotoLat = [attach getGeoLat];
    self.tempGeoGotoLng = [attach getGeoLng];
    [self performSegueWithIdentifier:SEGUE_GEO sender:nil];
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // hide keyboard
//    [self.sendField resignFirstResponder];
    [self.sendTextView resignFirstResponder];
    //
    ME_MessageModel* msgModel = self.modelMessagesArr[indexPath.row];
    if([msgModel hasGeo]) {
        self.tempGeoGotoLat = msgModel.lat;
        self.tempGeoGotoLng = msgModel.lng;
        [self performSegueWithIdentifier:SEGUE_GEO sender:nil];
    } else {
        for(ME_Attachment* attach in msgModel.attachArr) {
            if(ME_ATTACHMENT_11_GEO == attach.type) {
                self.tempGeoGotoLat = [attach getGeoLat];
                self.tempGeoGotoLng = [attach getGeoLng];
                [self performSegueWithIdentifier:SEGUE_GEO sender:nil];
                break;
            }
        }
    }
}

//- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"123");
//    return indexPath;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger res = self.modelMessagesArr.count;
    return res;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //
    ME_ChatCell* cell = [tableView dequeueReusableCellWithIdentifier:@"chatCell" forIndexPath:indexPath];
    ME_MessageModel* msgModel = self.modelMessagesArr[indexPath.row];
    [cell setModel:msgModel];
    cell.delegate = self;
    //
    
    if([msgModel.ID isEqualToString:self.player.audioCellId]) {
        self.player.audioCell = cell;
        [self.player updateCell];
    }
    //
    return cell;
}

//- (void) cellTapped:(id)sender
//{
//    [self.sendField resignFirstResponder];
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ME_MessageModel* msgModel = self.modelMessagesArr[indexPath.row];
    return [msgModel getHeightForCell];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([SEGUE_GEO isEqualToString:segue.identifier]) {
        if(self.tempGeoGotoLat && self.tempGeoGotoLng) {
            // show
            ME_GeoViewController* ctrl = segue.destinationViewController;
            ctrl.lat = self.tempGeoGotoLat;
            ctrl.lng = self.tempGeoGotoLng;
        } else {
            // choose
            ME_GeoViewController* ctrl = segue.destinationViewController;
            ctrl.lat = 0;
            ctrl.lng = 0;
            ctrl.delegate = self;
        }
    }
}

@end
