//
//  NSArray+Utils.m
//  ME_Messme
//
//  Created by MessMe on 08/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "NSArray+Utils.h"

@implementation NSArray (Utils)

- (BOOL) containsByPredicate:(NSPredicate*)predicate
{
    for(int i = 0; i < self.count; i++) {
        if([predicate evaluateWithObject:[self objectAtIndex:i]]) {
            return YES;
        }
    }
    return NO;
}

- (NSArray*) mapWithBlock:(id(^)(id obj))block
{
    NSMutableArray *arr = [NSMutableArray array];
    for(int i = 0; i < self.count; i++) {
        id obj = block(self[i]);
        [arr addObject:obj];
    }
    return arr;
}

@end
