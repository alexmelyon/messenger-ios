//
//  ME_RecoveryController.m
//  ME_Messme
//
//  Created by MessMe on 27/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_RecoveryController.h"
#import "ME_Network.h"
#import "AppDelegate.h"
#import "UIImage+Utils.h"
#import "ME_ProfileController.h"
#import "NSString+Util.h"

static NSString* SEGUE_TABS = @"segueTabs";

@interface ME_RecoveryController ()
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *loginLabel;
- (IBAction)doNotRecoveryAction:(id)sender;
- (IBAction)recoveryAction:(id)sender;
- (IBAction)backAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *recoveryButton;
@property (weak, nonatomic) IBOutlet UIButton *doNotRecoveryButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property NSDictionary* userProfile;

@end

@implementation ME_RecoveryController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.doNotRecoveryButton.layer.borderWidth = 1.0f;
    self.doNotRecoveryButton.layer.borderColor = [UIColor colorWithRed:127/255.0 green:197/255.0 blue:239/255.0 alpha:1.0].CGColor;
    //
    self.backButton.hidden = self.isInitial;
    //
    NSString* path = [NSString stringWithFormat:@"/User?id=%@", [AppDelegate shared].phone];
    [ME_Network connectTo:path doIt:[ME_Network isConnected] andThen:^(SRWebSocket *webSocket) {
        [self retrieveUserProfile];
    }];
}

- (void) retrieveUserProfile
{
    // hide screen
    self.navigationController.navigationBarHidden = YES;
    UIView* whiteView = [[UIView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:whiteView];
    //
    NSString* path = [NSString stringWithFormat: @"/User?id=%@", [AppDelegate shared].phone];
    [ME_Network connectTo:path doIt:![ME_Network isConnected] andThen:^(SRWebSocket *webSocket) {
        //
        [ME_Network send:@{@"action":@"user.info", @"options":@{@"userid":[AppDelegate shared].phone, @"locale":@"ru"}} andThen:^(NSDictionary *json) {
            NSDictionary* result = [json objectForKey:@"result"];
            self.userProfile = result;
            //
            // show screen
            self.navigationController.navigationBarHidden = NO;
            [whiteView removeFromSuperview];
            //
            NSString* login = [result objectForKey:@"login"];
            if(!login.length) {
                [self doRecovery];
            } else {
                //
                NSString* name = [result objectForKey:@"name"];
                NSString* surname = [result objectForKey:@"surname"];
                NSString* fullname = [name plus:@" " :surname];
                if([fullname trim].length) {
                    self.usernameLabel.text = fullname;
                } else {
                    self.usernameLabel.text = login;
                }
                
                self.loginLabel.text = login;
                self.phoneLabel.text = [[result objectForKey:@"id"] stringValue];
                NSString* avatar = [result objectForKey:@"avatar"];
                if([@"" isEqualToString:avatar]) {
                    self.avatarImageView.image = [UIImage imageNamed:@"tab_contacts"];
                    //
                    [self enableButtons];
                    //
                } else {
                    //
                    NSString* path = [NSString stringWithFormat:@"/avatar/%@", avatar];
                    [ME_Network downloadFileFromPath:path andThen:^(NSData *data) {
                        NSLog(@"DOWNLOADED %tu", data.length);
                        self.avatarImageView.image = [[data toImage] resizeTo:CGSizeMake(50, 50)];
                        //
                        [self enableButtons];
                    }];
                }
            }
        }];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) enableButtons
{
    
    self.recoveryButton.enabled = YES;
    self.doNotRecoveryButton.enabled = YES;
    self.recoveryButton.alpha = 1.0;
    self.doNotRecoveryButton.alpha = 1.0;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([SEGUE_TABS isEqualToString:segue.identifier]) {
        [AppDelegate shared].isGoingToTabs = YES;
    }
}


- (IBAction)doNotRecoveryAction:(id)sender {
    NSLog(@"doNotRecoveryAction");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ME_ProfileController* ctrl = [storyboard instantiateViewControllerWithIdentifier:@"profileController"];
    ctrl.isInitial = YES;
    [AppDelegate shared].isNeededToClearFriends = YES;
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (IBAction)recoveryAction:(id)sender {
    NSLog(@"recoveryAction");
    [AppDelegate shared].userProfile = [[ME_UserProfile alloc] initWithDictionary:self.userProfile andImage:self.avatarImageView.image];
    int countryid = [[self.userProfile objectForKey:@"country"] intValue];
    id opts = @{@"continent":@[], @"locale":@"ru", @"mask":@"", @"filter":@[@(countryid)]};
    id msg = @{@"action":@"geo.country.list", @"options":opts};
    [ME_Network send:msg andThen:^(NSDictionary *json) {
        //
        NSArray* result = [json objectForKey:@"result"];
        if(result.count > 0) {
            [AppDelegate shared].userProfile.countryObj = [result objectAtIndex:0];
        }
        //
        long long cityid = [[self.userProfile objectForKey:@"city"] longLongValue];
        if(cityid) {
            id opts = @{@"continent":@[], @"country":@[], @"region":@[], @"locale":@"ru", @"mask":@"", @"filter":@[@(cityid)]};
            id msg = @{@"action":@"geo.city.list", @"options":opts};
            [ME_Network send:msg andThen:^(NSDictionary *json) {
                //
                NSArray* result = json[@"result"];
                if(result.count) {
                    [AppDelegate shared].userProfile.cityObj = result[0];
                } else {
                    NSAssert(result.count, @"ERROR: CITY LIST IS EMPTY");
                }
                //
                [self doRecovery];
            }];
        } else {
            [self doRecovery];
        }
    }];
}

- (void) doRecovery
{
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ME_ProfileController* ctrl = [storyboard instantiateViewControllerWithIdentifier:@"profileController"];
    ctrl.isInitial = YES;
    NSString* login = self.userProfile[@"login"];
    if(login.length) {
        ctrl.isDisableLoginField = YES;
    }
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end

