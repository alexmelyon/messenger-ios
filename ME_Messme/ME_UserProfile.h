//
//  ME_UserProfile.h
//  ME_Messme
//
//  Created by MessMe on 02/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ME_UserProfile : NSObject

@property NSString* username;
@property NSString* surname;
@property NSString* login;
@property NSString* sex;
@property NSDate* birthdate;
@property UIImage* avatarImage;
@property NSDictionary* countryObj;
@property NSDictionary* cityObj;

- (ME_UserProfile*) initWithDictionary:(NSDictionary*)dict andImage:(UIImage*)image;
- (NSString*) getSexString;
- (NSString*) getDateString;

@end
