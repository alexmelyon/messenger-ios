//
//  ME_MessageModel.h
//  ME_Messme
//
//  Created by MessMe on 11/01/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>

static int BUBBLE_WIDTH = 240;
static int MESSSAGE_WIDTH = 220;

typedef enum {
    MSG_STATUS_UNREAD = -1,
    MSG_STATUS_0_SENT = 0,
    MSG_STATUS_1_DELIVERED = 1,
    MSG_STATUS_2_READED = 2
} MSG_STATUS;

typedef enum {
    MSG_TYPE_0_NON_SNAP = 0,
    MSG_TYPE_1_SNAP
}MSG_TYPE;

@interface ME_MessageModel : NSObject

@property int owner;
@property (nonatomic, copy) NSString* message;
@property (nonatomic, copy) NSString* date;
@property long long userid;
@property (nonatomic, strong) NSMutableArray* attachArr;
@property MSG_TYPE type;
@property (nonatomic, copy) NSString* ID;
@property MSG_STATUS status;
@property double lat;
@property double lng;
@property int timer;
@property BOOL isIncoming;

- (ME_MessageModel*) initWithDict:(NSDictionary*)dict;
- (ME_MessageModel*) initWithDict:(NSDictionary*)dict andAttachArr:(NSArray*)attachArr;
- (ME_MessageModel*) updateWithDict:(NSDictionary*)dict;
- (double) getHeightForCell;
- (BOOL) hasGeo;

@end
