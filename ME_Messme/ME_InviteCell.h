//
//  InviteCell.h
//  ME_Messme
//
//  Created by MessMe on 08/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ME_InviteCellDelegate <NSObject>
- (void) inviteFromCell:(UITableViewCell*)cell withPhone:(NSString*)phone;
@end

@interface ME_InviteCell : UITableViewCell
@property (nonatomic, weak) id<ME_InviteCellDelegate> delegate;
@property (nonatomic, copy) NSString* phone;
- (void) inviteAction:(UIButton*)sender;
@end
