//
//  ME_ContactsController.m
//  ME_Messme
//
//  Created by MessMe on 20/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import "ME_ContactsController.h"
#import "AppDelegate.h"
#import <Contacts/Contacts.h>
#import "ME_Friend.h"
@import Contacts;
@import ContactsUI;
#import "ME_Network.h"
#import "ME_InviteCell.h"
#import <MessageUI/MessageUI.h>
#import "NSArray+Utils.h"
#import "ME_ImageCache.h"
#import "UIImage+Utils.h"
#import "CNContact+Utils.h"
#import "NSString+Util.h"
#import "ME_ChatDetailController.h"
#import "ME_Dialog.h"
#import "UIImageView+Utils.h"
#import "ME_SearchItem.h"
#import "ME_FriendProfileController.h"

static NSString* SEGUE_CHAT = @"segueChat";

@interface ME_ContactsController () <
AppDelegateContactsListener,
MFMessageComposeViewControllerDelegate,
CNContactViewControllerDelegate,
UISearchDisplayDelegate,
ME_InviteCellDelegate
>
- (IBAction)addContactAction:(id)sender;
@property (nonatomic, strong) NSArray* searchResult;
@property (nonatomic, strong) NSArray* searchToInviteResult;
@property (nonatomic, copy) NSString* searchQuery;
@property NSTimer* searchTimer;
@property long long useridToSend;
@end

@implementation ME_ContactsController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[AppDelegate shared] addContactsListener:self];
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void) appWillEnterForeground:(id)sender
{
    [[AppDelegate shared] fetchContactsAndThen:^(NSMutableArray *arr) {
        //
        [[AppDelegate shared] updateSections];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
//    [[AppDelegate shared] removeContactsListener:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onContactsUpdated:(NSArray *)contacts
{
    NSLog(@"ME_ContactsContorller onContactsUpdated");
    [self.tableView reloadData];
}

- (IBAction)addContactAction:(id)sender
{
    NSLog(@"addContactAction");
    CNContactStore *store = [[CNContactStore alloc] init];
    CNContactViewController *controller = [CNContactViewController viewControllerForNewContact:[CNContact new]];
    controller.contactStore = store;
    controller.delegate = self;
    UINavigationController* navCtrl = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navCtrl animated:YES completion:nil];
}

- (void)contactViewController:(CNContactViewController *)viewController didCompleteWithContact:(CNContact *)contact
{
    NSLog(@"didCompleteWithContact %@", contact);
    //
    [viewController dismissViewControllerAnimated:YES completion:nil];
    if(contact) {
        [[AppDelegate shared] fetchContactsAndThen:^(NSMutableArray *arr) {
//            long long phone = [[contact getFirstNumber] longLongValue];
        id opts = @{@"userlist":[[AppDelegate shared] getPhoneNumbersArr]}; // jcd
        id msg = @{@"action":@"user.setfriend", @"options":opts};
        [ME_Network send:msg andThen:^(NSDictionary *json) {
                //
                [[AppDelegate shared] updateFriends:^(NSMutableArray *friends) {
                    //
                    [[AppDelegate shared] updateSections];
                }];
            }];
        }];
    }
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    NSString* trimmed = [searchString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if(searchString == nil || [trimmed isEqualToString:@""]) {
        //        self.filteredArr = self.citiesArr;
    } else {
        self.searchQuery = [trimmed lowercaseString];
        [self.searchTimer invalidate];
        self.searchTimer = [NSTimer scheduledTimerWithTimeInterval:0.0 target:self selector:@selector(doSearch:) userInfo:nil repeats:NO];
    }
    return YES;
}

- (void) doSearch:(id)sender
{
    NSString* mask = [self.searchQuery lowercaseString];
    if(!mask) {
        mask = @"";
    }
    // TODO search locally
    NSPredicate* onlinePred = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        ME_Friend* friend = evaluatedObject;
        if([[friend.name lowercaseString] containsString:mask]) {
            return YES;
        }
        if([[friend.surname lowercaseString] containsString:mask]) {
            return YES;
        }
        if([[friend.login lowercaseString] containsString:mask]) {
            return YES;
        }
        return NO;
    }];
    NSArray* meFriends = [[AppDelegate shared].meContactsArr filteredArrayUsingPredicate:onlinePred];
    NSArray* localFriends = [meFriends mapWithBlock:^id(id obj) {
        ME_Friend* friend = obj;
//        return @{@"name": friend.name, @"surname":friend.surname, @"id":@(friend.ID), @"login":friend.login, @"avatar": friend.avatar};
        return [[ME_SearchItem alloc] initWithName:friend.name surname:friend.surname ID:friend.ID login:friend.login avatar:friend.avatar];
    }];
    //
    NSPredicate* toInvitePred = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        ME_PhoneContact* contact = evaluatedObject;
        if([[contact.fullname lowercaseString] containsString:mask]) {
            return YES;
        }
        if([[contact.name lowercaseString] containsString:mask]) {
            return YES;
        }
        if([[contact.surname lowercaseString] containsString:mask]) {
            return YES;
        }
        return NO;
    }];
    self.searchToInviteResult = [[AppDelegate shared].phoneContactsArr filteredArrayUsingPredicate:toInvitePred];
    //
    if(mask.length >= 5) {
        id msg = @{@"action":@"user.list",
                   @"options":@{@"userlist":@[],
                                @"locale":@"ru",
                                @"status":@(-1),
                                @"isfriend":@(-1),
                                @"country":@(0),
                                @"city":@(0),
                                @"sex":@"",
                                @"minage":@(0),
                                @"maxage":@(0),
                                @"mask":mask,
                                @"lat1":@(0),
                                @"lng1":@(0),
                                @"lat2":@(0),
                                @"lng2":@(0)}};
        [ME_Network send:msg andThen:^(NSDictionary *json) {
            NSMutableArray* netArr = [NSMutableArray array];
            for(NSDictionary* item in json[@"result"]) {
                BOOL isContains = NO;
                for(ME_SearchItem* itemJ in localFriends) {
                    if(itemJ.ID == [item[@"id"] longLongValue]) {
                        isContains = YES;
                        break;
                    }
                }
                if(!isContains) {
                    long long ID = [item[@"id"] longLongValue];
                    [netArr addObject:[[ME_SearchItem alloc] initWithName:item[@"name"]
                                                                        surname:item[@"surname"]
                                                                             ID:ID
                                                                          login:item[@"login"] avatar:item[@"avatar"]]];
                }
            }
//            self.searchResult = [localFriends arrayByAddingObjectsFromArray:[json objectForKey:@"result"]];
            self.searchResult = [localFriends arrayByAddingObjectsFromArray:netArr];
            [self.searchDisplayController.searchResultsTableView reloadData];
        }];
    } else {
        self.searchResult = [localFriends copy];
        [self.searchDisplayController.searchResultsTableView reloadData];
    }
}

- (void)inviteFromCell:(UITableViewCell *)cell withPhone:(NSString *)phone
{
    if([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController* ctrl = [[MFMessageComposeViewController alloc] init];
        // TODO write invice code
        [ctrl setBody:@"Привет, я теперь использую MessMe мессенджер ^_^!"];
        ctrl.recipients = @[phone];
        [ctrl setMessageComposeDelegate:self];
        [self presentViewController:ctrl animated:YES completion:nil];
    } else {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Ваше устройство не поддерживает эту функцию" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

#pragma mark MFMessageComposeViewControllerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    NSLog(@"didFinishWithResult");
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tableView) {
        if(0 == indexPath.section) {
            ME_Friend* meContact = [[AppDelegate shared].meContactsArr objectAtIndex:indexPath.row];
            self.useridToSend = meContact.ID;
        }
    } else {
        // search result
        if(0 == indexPath.section) {
            ME_SearchItem* obj = self.searchResult[indexPath.row];
            self.useridToSend = obj.ID;
        }
    }
    [self performSegueWithIdentifier:SEGUE_CHAT sender:self];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(tableView == self.tableView) {
        return [[AppDelegate shared] getNumberOfSectionsForContacts];
    } else {
        return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == self.tableView) {
        return [[AppDelegate shared] getNumberOfRowsForContactsSection:section];
    } else {
        if(0 == section) {
            return self.searchResult.count;
        } else if(1 == section) {
            return self.searchToInviteResult.count;
        }
        return 0;
    }
}

- (void) selectAvatar:(UITapGestureRecognizer*)sender
{
    NSLog(@"selectAvatar");
    UIImageView* imageView = (UIImageView*)sender.view;
    NSInteger tag = imageView.tag;
    ME_Friend* friend = [AppDelegate shared].meContactsArr[tag];
    [ME_FriendProfileController showProfileUserid:friend.ID fromCtrl:self];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //
    if(tableView == self.tableView) {
        if(0 == indexPath.section) {
            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            ME_Friend* meContact = [[AppDelegate shared].meContactsArr objectAtIndex:indexPath.row];
            NSString* name = [meContact getPresentName];
            cell.textLabel.text = name;
            cell.detailTextLabel.text = [[meContact.login plus:@": "] plusLongLong:meContact.ID];
            cell.imageView.image = [UIImage blankImageWithSize:CGSizeMake(34, 34) color:[UIColor whiteColor]];
            [ME_ImageCache getAvatar:meContact.avatar login:meContact.login name:name andThen:^(UIImage *image) {
                [cell.imageView setRoundedImage:image];
            }];
            UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectAvatar:)];
            cell.imageView.userInteractionEnabled = YES;
            cell.imageView.tag = indexPath.row;
            [cell.imageView addGestureRecognizer:tap];
            return cell;
        } else {
            ME_InviteCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"inviteCell" forIndexPath:indexPath];
            NSDictionary* section = [[AppDelegate shared].meContactsSections objectAtIndex:indexPath.section - 1];
            NSArray* arr = [section mutableArrayValueForKey:@"arr"];
            NSString* fullname = [[arr objectAtIndex:indexPath.row] objectForKey:@"username"];
            UITextField* nameField = (UITextField*)[cell viewWithTag:101];
            nameField.text = fullname;
            UIButton* inviteButton = (UIButton*)[cell viewWithTag:102];
            inviteButton.layer.cornerRadius = 5;
            inviteButton.layer.borderWidth = 1.0f;
            inviteButton.layer.borderColor = [UIColor colorWithRed:127/255.0 green:197/255.0 blue:239/255.0 alpha:1.0].CGColor;
            [inviteButton addTarget:cell action:@selector(inviteAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView bringSubviewToFront:inviteButton];
            //
            NSString* phone = [[arr objectAtIndex:indexPath.row] objectForKey:@"phone"];
            cell.delegate = self;
            cell.phone = phone;
//            [self.contactsToInvite setObject:phone forKey:[NSString stringWithFormat:@"%ld-%ld", indexPath.section, indexPath.row]];
            return cell;
        }
    }
    // searchResult
    if(0 == indexPath.section) {
        UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
        ME_SearchItem* obj = [self.searchResult objectAtIndex:indexPath.row];
        NSString* name = obj.name;
        NSString* surname = obj.surname;
        NSString* fullname = [NSString stringWithFormat:@"%@ %@", name, surname];
        if(![fullname trim].length) {
            fullname = obj.login;
        }
        cell.textLabel.text = fullname;
//        cell.detailTextLabel.text = obj.login;
        cell.detailTextLabel.text = [[obj.login plus:@": "] plusLongLong:obj.ID];
        NSString* avatar = obj.avatar;
        [ME_ImageCache getAvatar:avatar login:fullname name:fullname andThen:^(UIImage *image) {
            cell.imageView.image = [image resizeTo:CGSizeMake(34, 34)];
            cell.imageView.layer.cornerRadius = 17;
            cell.imageView.clipsToBounds = YES;
//            [cell.imageView setRoundedImage:[image resizeTo:CGSizeMake(34, 34)]];
        }];
        // TODO id
        return cell;
    } else if(1 == indexPath.section) {
        ME_InviteCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"inviteCell"];

        ME_PhoneContact* contact = [self.searchToInviteResult objectAtIndex:indexPath.row];
//        cell.textLabel.text = contact.fullname;
        UILabel* name = [cell viewWithTag:101];
        name.text = contact.fullname;
        UIButton* inviteButton = (UIButton*)[cell viewWithTag:102];
        inviteButton.layer.cornerRadius = 5;
        inviteButton.layer.borderWidth = 1.0f;
        inviteButton.layer.borderColor = [UIColor colorWithRed:127/255.0 green:197/255.0 blue:239/255.0 alpha:1.0].CGColor;
        [inviteButton addTarget:cell action:@selector(inviteAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView bringSubviewToFront:inviteButton];
        //
        NSString* phone = contact.phone;
        cell.delegate = self;
        cell.phone = phone;
//        [self.contactsToInvite setObject:phone forKey:[NSString stringWithFormat:@"%ld-%ld", indexPath.section, indexPath.row]];
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(0 == section) {
        return 0;
    }
    return UITableViewAutomaticDimension;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(tableView == self.tableView) {
        return [[AppDelegate shared] getTitleForHeaderInContactsSection:section];
    } else {
        return nil;
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([SEGUE_CHAT isEqualToString:segue.identifier]) {
        UINavigationController* navCtrl = segue.destinationViewController;
        ME_ChatDetailController* ctrl = navCtrl.viewControllers[0];
        ctrl.userlist = [NSMutableArray arrayWithObject:@(self.useridToSend)];
    }
}


@end
