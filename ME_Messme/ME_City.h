//
//  ME_City.h
//  ME_Messme
//
//  Created by MessMe on 02/03/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ME_City : NSObject

@property int cityId;
@property (nonatomic, copy) NSString* cityName;
@property int continentId;
@property (nonatomic, copy) NSString* continentName;
@property int countryId;
@property (nonatomic, copy) NSString* countryName;
@property int regionId;
@property (nonatomic, copy) NSString* regionName;
@property int iso;
@property double lat;
@property double lng;

- (instancetype) initWithDict:(NSDictionary*)dict;

@end
