//
//  ME_ForGroupCell.m
//  ME_Messme
//
//  Created by MessMe on 19/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "ME_ForGroupCell.h"
#import "ME_ImageCache.h"
#import "UIImageView+Utils.h"
#import "NSString+Util.h"
#import "NSDate+Utils.h"

@interface ME_ForGroupCell ()

@property (weak, nonatomic) IBOutlet UIImageView *checkImageView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *onlineLabel;

@end

@implementation ME_ForGroupCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setChecked:(BOOL)checked
{
    self.checkImageView.image = checked ? [UIImage imageNamed:@"tab_settings_active"] : [UIImage imageNamed:@"tab_settings"];
}

- (void) configureWithFriend:(ME_Friend*)friend
{
    [ME_ImageCache getAvatar:friend.avatar login:friend.login name:[friend getPresentName] andThen:^(UIImage *image) {
        [self.avatarImageView setRoundedImage:image];
    }];
    self.nameLabel.text = [friend getPresentName];
    self.phoneLabel.text = [@"" plusLongLong:friend.ID];
    self.onlineLabel.text = friend.status ? @"В сети" : [[friend.statusDate toDateFromUtc] fromNow];
}

@end
