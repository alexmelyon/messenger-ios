//
//  UIImage+Utils.h
//  ME_Messme
//
//  Created by MessMe on 03/12/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utils)

- (UIImage*) resizeTo:(CGSize)size;
- (NSString*) toBase64String;
- (UIImage*) cropCenter;
- (UIImage*) cropCenterToSize:(CGSize)size;
- (UIImage*) drawText:(NSString*)text atPoint:(CGPoint)point;
//+ (UIImage*) blankImageWithSize:(CGSize)size;
+ (UIImage*) blankImageWithSize:(CGSize)size color:(UIColor*)color;
- (UIImage *)fixRotation;
- (UIImage*) getRounded;

@end

//@interface UIColor (ImageUtils)
//+ (UIColor*) colorWithHex:(NSString*)hex;
//@end

@interface NSData (ImageUtils)
- (UIImage*) toImage;
@end;
