//
//  ME_Contact.h
//  ME_Messme
//
//  Created by MessMe on 30/11/15.
//  Copyright © 2015 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ME_Friend : NSObject

@property long long ID;
@property (nonatomic, copy) NSString* login;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* surname;
@property (nonatomic, copy) NSString* sex;
@property int age;
@property int iso;
@property int country;
@property long city;
@property (nonatomic, copy) NSString* countryname;
@property (nonatomic, copy) NSString* cityname;
@property int status;
@property (nonatomic, copy) NSString* invite;
@property double lat;
@property double lng;
@property double distance;
@property int geostatus;
@property BOOL isfriend;
@property (nonatomic, copy) NSString* avatar;
@property int likes;
@property (nonatomic, copy) NSString* phoneName; // name from address book
@property (nonatomic, copy) NSString* statusDate;
@property (nonatomic, strong) UIImage* image;
@property BOOL isLiked;

- (ME_Friend*) initWithDict:(NSDictionary*)dict;
- (NSString*) getPresentName;

@end
