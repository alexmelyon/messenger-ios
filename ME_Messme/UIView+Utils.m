//
//  UIView+Utils.m
//  ME_Messme
//
//  Created by MessMe on 26/02/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import "UIView+Utils.h"

@implementation UIView (Utils)

- (UIView*) getSuperviewClass:(Class*)clazz
{
    UIView* v = [self superview];
    while(![v isKindOfClass:*clazz]) {
        v = [v superview];
    }
    return nil;
}

@end
