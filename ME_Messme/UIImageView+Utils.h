//
//  UIImageView+Utils.h
//  ME_Messme
//
//  Created by MessMe on 11/01/16.
//  Copyright © 2016 MessMe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImageView (Utils)

- (void) setRoundedImage:(UIImage*)img;

@end
